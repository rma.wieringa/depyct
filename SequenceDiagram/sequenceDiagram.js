/*
 * Copyright (C) 2020, 2021, 2022, 2023, 2024 Rob Wieringa <rma.wieringa@gmail.com>
 * Copyright (C) 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
 * Copyright (C) 2022 Paul Hoogendijk <paul@dezyne.org>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

let globals = {
  spacing: 100, // minimal spacing between lifelines
  hpadding: 5, // horizontal padding for HeaderGroup
  hsep: 15, // minimal horzontal separation between HeaderGroup elements
  vpadding: 5, // vertical padding for HeaderGroup
  barwidth: 10,
  verticalPerTime: 25,
  verticalStart: 25,
  vTime: (t) => (globals.verticalStart + t * globals.verticalPerTime),
  eligibleVertical: 30,
};

class Header extends HasViz {
  constructor(data, states, lifeline) {
    super(data); // {instance, role}
    this.instance = data.instance;
    this.role = data.role;
    this.states = states;
    this.stateEntry = null;
    this.lifeline = lifeline;
    this.initViz();
  }

  initViz() {
    let color =
        (this.role == 'component' || this.role == 'interface') ? '#AEE8A0' : '#FFFC80';
    let text = this.instance.replace(/.*\./,'');
    let btext = new Text(text);
    btext.bold = true;
    btext.update(); // always needed after font changes!
    let rows = [[btext]]; // one row, one column
    if (this.states && this.states.length > 0) {
      let index = this.lifeline.index;
      let w = this.calcDimensions(index);
      let stateEntry = this.states[0].find(entry => entry.index == index);
      if (stateEntry && w.nrLines > 0) {
        // init table with empty text, but take nrLines into account
        let text = this.emptyLines(w.nrLines);
        let names = new Text(text);
        let values = new Text(text);
        names.setWidth(w.namesWidth);
        values.setWidth(w.valuesWidth);
        this.stateTable = new Table([[names, values]]); // one row, two columns
        this.stateTable.hpad = 0;
        this.stateTable.vpad = 0;
        this.stateTable.update();
        rows.push([this.stateTable]); // two rows, one column
      }
      else {
        this.stateTable = new Table([]);
      }
    }
    this.table = new Table(rows);
    this.table.centered = true;
    this.table.update();
    let box = new RoundedBoundingBox(this.table);
    box.padding = 0;
    box.round = 10;
    box.color = color;
    // take care: eliblibles can be wider than Header!
    // add an 'invisible' (transparent) box as wide as the eligibles.
    let etext = this.lifeline.eligibles.map(eligible => eligible.displayText).join('\n');
    let eText = new Text(etext);
    let ewidth = eText.bounds.width;
    let ebox = new Box();
    ebox.bounds.width = ewidth;
    ebox.bounds.height = 1;
    ebox.color = '#FFFFFF00';
    ebox.strokeWeight = 0;
    ebox.update();
    let frame = new Frame([ebox, box]);
    // center the table in the frame
    box.hCenter(ewidth/2);
    this.setViz(frame);
  }

  calcDimensions(index) {
    let result = {namesWidth: 0, valuesWidth: 0, nrLines: 0};
    let allNames = '';
    let allValues = '';
    this.states.forEach(state => {
      let stateEntry = state.find(entry => entry.index == index);
      if (stateEntry) {
        result.nrLines = Math.max(result.nrLines, stateEntry.state.length);
        allNames += '\n'+stateEntry.state.map(entry => entry.name).join('\n');
        allValues += '\n'+stateEntry.state.map(entry => entry.value).join('\n');
      }
    });
    let text = new Text(allNames);
    result.namesWidth = text.bounds.width;
    text = new Text(allValues);
    result.valuesWidth = text.bounds.width;
    return result;
  }

  emptyLines(nr) {
    let text = '';
    for (let i = 2; i <= nr; i++) {
      text = text + '\n';
    }
    return text;
  }

  update() {
    if (this.upToDate) return;
    if (this.stateEntry && this.stateEntry.length) {
      // cope with changes in stateEntry
      let names = this.stateEntry.map(entry => entry.name).join('\n');
      let values = this.stateEntry.map((entry, i) => {
        let v = entry.value;
        if (this.oldStateEntry && i < this.oldStateEntry.length) {
          let oldEntry = this.oldStateEntry.find(old => old.name == this.stateEntry[i].name);
          if (oldEntry && oldEntry.value != v)
            v = '$' + v;
        }
        return v;
      }).join('\n');
      let content = this.stateTable.content[0];
      content[0].text = names;
      content[1].text = values;
    }
    this.table.update();
    this.viz.update();
    this.upToDate = true;
  }

  get midXOffset() {
    return this.bounds.width/2;
  }

  set state(stateEntry) {
    if (stateEntry && this.stateEntry != stateEntry) {
      this.oldStateEntry = this.stateEntry;
      this.stateEntry = stateEntry;
      this.isModified();
    }
  }

  highlight(on) {
    this.viz.content[1].highlight(on);
  }
}

class HeaderGroup extends HasViz {
  constructor(name, elements, foreign, lifelineGroup, states, light) {
    super(null);
    this.name = name;
    this.elements = this.lifelinesToHeaders(elements);
    this.lifelineGroup = lifelineGroup;
    this.body = lifelineGroup.body;
    this.states = states;
    this.foreign = foreign;
    this.isOpen = true;
    this.isTop = name == '';
    this.hasButton = !(this.isTop || this.foreign);
    this.light = light;
    this.initViz();
  }

  lifelinesToHeaders(elements) {
    return elements.map(element => element instanceof Lifeline ? element.header : element.header);
  }

  get firstHeader() {
    if (this.isOpen) {
      let frst = this.elements[0];
      return frst instanceof Header ? frst : frst.firstHeader;
    } else {
      return this;
    }
  }

  get lastHeader() {
    if (this.isOpen) {
      let lst = this.elements[this.elements.length-1];
      return lst instanceof Header ? lst : lst.lastHeader;
    } else {
      return this;
    }
  }

  get firstDepth() {
    let frst = this.elements[0];
    return frst instanceof Header ? 1 : frst.firstDepth + 1;
  }

  get maxDepth() {
    let max = 0;
    this.elements.forEach(element => {
      let d = element instanceof HeaderGroup ? element.maxDepth : 0;
      max = Math.max(max, d);
    });
    return max + 1;
  }

  initViz() {
    this.openText = '-';
    this.closedText = '+';
    this.button = new Button(this.openText, this.buttonOpenClose, this);

    let text = this.name.replace(/.*\./,'');
    this.vizName = new Text(text);
    this.vizName.centered = true;
    this.eframe = new Frame(this.elements);
    this.bboxFrame = new Frame([]);
    this.bboxFrame.content = [this.vizName]
      .concat(this.isOpen ? [this.eframe] : [])
      .concat(this.hasButton ? [this.button] : []);

    this.bbox = new BoundingBox(this.bboxFrame)
    this.bbox.padding = {left: globals.hpadding, top: globals.vpadding,
                    right: globals.hpadding, bottom: globals.vpadding};

    this.bbox.openColor = this.isTop ? '#FFFFFF'
      : this.foreign ? '#E5FFE5'
      : this.light ? '#C9FFC9'
      : '#C0F7BC';
    this.bbox.closedColor = '#A8E094';
    this.bbox.color = this.bbox.openColor;
    this.bbox.shadowed = false;
    this.setViz(this.bbox);
    this.update();
  }

  buttonOpenClose(ctrl) {
    // take care: this instanceof Button
    console.log('PRESSED with CTRL ' + ctrl);
    this.manager.openClose(ctrl)
  }

  openClose(ctrl) {
    this.setOpenClose(!this.isOpen, ctrl);
    this.body.filterIsUptodate = false;
    this.body.update();
  }

  setOpenClose(open, ctrl) {
    // TODO:  this.openCloseCounter = this.openCloseSteps;
    this.isOpen = open;
    if (ctrl) {
      this.elements.forEach(elt => {
        if (elt instanceof HeaderGroup) {
          elt.setOpenClose(open, ctrl);
        }
      });
    }
    this.lifelineGroup.setOpenClose(open);
    this.isModified();
    this.update();
    this.lifelineGroup.update();
    // take a step:
    // TODO this.animationStep();
  }

  update() {
    if (this.upToDate) return;
    let padding = 6;
    if (this.hasButton) {
      this.button.move(0, 0);
      this.button.setText(this.isOpen ? this.openText : this.closedText);
      this.button.update();
      let buttonbnd = this.button.bounds;
      this.vizName.move(buttonbnd.width + padding, 0);
    } else {
      this.vizName.move(0, 0);
    }
    if (this.isOpen) {
      this.bboxFrame.content = [this.vizName, this.eframe].concat(this.hasButton ? [this.button] : []);

      this.elements.forEach((element, ei) => {
        if (ei == 0) {
          element.move(0, 0);
        } else {
          let prevElement = this.elements[ei-1];
          let prevHeader = prevElement instanceof Header ? prevElement : prevElement.lastHeader;
          let nextHeader = element instanceof Header ? element : element.firstHeader;
          let prevmid = prevHeader.relativeBounds(this.eframe).x + prevHeader.bounds.width/2;
          let relnextx = element instanceof Header ? 0 : nextHeader.relativeBounds(element).x;
          let relnextmid = relnextx + nextHeader.bounds.width/2;
          // 1) distance prevElement and element must be at least globals.hsep
          // 2) mids distance of prevHeader and nextHeader must be at least globals.spacing
          let px1 = prevElement.bounds.x + prevElement.bounds.width + globals.hsep;
          let px2 = prevmid + globals.spacing - relnextmid;
          // 3) move down for nested groups:
          let py = (element instanceof Header) ? 0 :
              - element.maxDepth*(globals.vpadding*2 + this.vizName.bounds.height);
          element.move(Math.max(px1, px2), py);
        }
        element.update();
      });
      this.eframe.update();
      // center name and eframe:
      let m = Math.max(this.vizName.bounds.width, this.eframe.bounds.width)/2;
      if (!this.hasButton) this.vizName.hCenter(m);
      this.eframe.hCenter(m);
      this.eframe.vMove(this.vizName.bounds.height + globals.vpadding); // actually a vsep
    } else {
      this.bboxFrame.content = [this.button, this.vizName];
    }
    this.bboxFrame.update();
    this.bboxFrame.hMove(0);
    this.bbox.update();
    if (this.isOpen) {
      this.bbox.color = this.bbox.openColor;
      this.bbox.strokeWeight = this.isTop ? 0 : .1;
      this.openHeight = this.bbox.bounds.height;
    } else {
      this.bbox.color = this.bbox.closedColor;
      this.bbox.strokeWeight = 1;
      this.bbox.boundsHeight = this.openHeight;
    }
    this.upToDate = true;
  }

  get midXOffset() {
    if (this.isOpen) {
      let frst = this.elements[0];
      return frst.midXOffset + globals.hpadding;
    } else {
      return this.bounds.width/2;
    }
  }

  reorder(header) {
    let local = this.elements.find(element => element == header);
    if (local) {
      let overlaps = (elt1, elt2) => {
        return !(elt1.bounds.x + elt1.bounds.width < elt2.bounds.x
                 || elt2.bounds.x + elt2.bounds.width < elt1.bounds.x);
      }
      let swap = (elt1, elt2) => {
        let swapped = [];
        this.elements.forEach(element => {
          if (element == elt1) swapped.push(elt2);
          else if (element == elt2) swapped.push(elt1);
          else swapped.push(element);
        });
        return swapped;
      };
      let reordered;
      let hi = this.elements.findIndex(element => element == header);
      let hmid = header.bounds.x + header.bounds.width/2;
      this.elements.forEach((element, ei) => {
        if (element != header && element.role == header.role) {
          if (overlaps(element, header)) {
            let emid = element.bounds.x + element.bounds.width/2;
            if ((hi < ei && hmid > emid) || (hi > ei && hmid < emid)) {
              reordered = swap(element, header);
            }
          }
        }
      });
      if (reordered) {
        this.elements = reordered;
        let hx = header.bounds.x;
        this.forceUpdate();
        header.bounds.x = hx;
        this.elements.forEach(element => {
          if (element.role == 'provides' || element.role == 'requires') {
            element.lifeline.align();
          }
        });
      }
    } else {
      this.elements.forEach(element => {
        if (element instanceof HeaderGroup) element.reorder(header);
      });
    }
  }

  get lifelineOrder() {
    return {isOpen: this.isOpen,
            elements: this.elements.map(element =>
              (element instanceof Header ? element.lifeline.index : element.lifelineOrder))
           };
  }

  restoreLifelineOrder(lifelineOrder) {
    this.elements = this.elements.map((element, ei) => {
      let elt;
      if (element instanceof Header) {
        let lli = lifelineOrder.elements[ei];
        elt = this.elements.find(element => (element instanceof Header && element.lifeline.index == lli));
      } else {
        element.restoreLifelineOrder(lifelineOrder.elements[ei]);
        elt = element;
      }
      return elt;
    });
    if (!lifelineOrder.isOpen) {
      this.setOpenClose(false, false);
    }
  }
}

class Activity { // no Viz!
  constructor(data, lifeline) {
    this.data = data;
    this.key = data.key;
    this.time = data.time; // only used in Event.deltaTime
    this.location = data.location;
    this.lifeline = lifeline;
  }

  copy(lifeline) {
    let activity = new Activity(this.data, lifeline);
    return activity;
  }
}

// abstract class
class AnyBar extends HasViz {
  constructor(data, lifeline) {
    super(data, lifeline); // data: {startTime, endTime}
    this.startTime = data.startTime;
    this.endTime = data.endTime;
    this.lifeline = lifeline;
    this.barExtend = 4;
  }

  initViz() {
    this.bar = new Box();
    this.bar.bounds.width = globals.barwidth;
    this.bar.bounds.y = globals.vTime(this.startTime) - this.barExtend;
    this.bar.bounds.height = globals.vTime(this.endTime) - globals.vTime(this.startTime) + 2*this.barExtend;
    this.bar.color = '#000000';
    this.setViz(this.bar);
  }

  update() {
    this.bar.boundsHeight = globals.vTime(this.endTime) - globals.vTime(this.startTime) + 2*this.barExtend;
  }
}

class BlockedBar extends AnyBar {
  constructor(data, lifeline) {
    super(data, lifeline); // data: {startTime, endTime}
    this.initViz();
  }

  initViz() {
    super.initViz();
    this.bar.color = '#DDDDDD';
    this.update();
  }
}

class ActivityBar extends AnyBar {
  constructor(data, lifeline) {
    super(data, lifeline); // data: {startTime, endTime, blocked}
    this.blocked = data.blocked.map(bl => new BlockedBar(bl, lifeline));
    this.initViz();
  }

  initViz() {
    super.initViz();
    this.bar.color = '#FE938C';
    this.blocked.forEach(bl => bl.initViz());
    this.frame = new Frame([]);
    this.update();
    this.setViz(this.frame);
  }

  update() {
    super.update();
    this.blocked.forEach(bl => {
      bl.update();
      bl.move(0, globals.vTime(bl.startTime) - globals.vTime(this.startTime));
    });
    this.frame.content = [this.bar].concat(this.blocked);
    this.frame.updateBounds();
  }
}

class Eligible extends HasViz {
  constructor(data) {
    super(data); // {text, type, illegal}
    this.text = data.text;
    this.displayText = this.text.replace(/.*\./,'');
    this.type = data.type;
    this.illegal = data.illegal;
    this.initViz();
  }

  initViz() {
    this.button = new Button(this.displayText, this.buttonHandleEligible, this);
    this.button.color = this.illegal ? '#EEEEEE'
      : this.displayText == '<back>' ? '#5BC0EB' // '#6FFFE9'
      : '#B9E5F7';
    this.button.strokeWeight = 1;
    this.button.strokeColor = this.illegal ? '#CCCCCC' : '#888888';
    this.button.padding = 4;
    this.setViz(this.button);
  }

  draw(p) {
    this.viz.draw(p);
  }

  buttonHandleEligible() {
    // take care: this instanceof Button
    this.manager.handleEligible()
  }

  handleEligible() {
    console.log('PRESSED ' + this.text);
  }

}

class Message extends HasViz {
  constructor(data) {
    super(data); // {text, location}
    this.text = data.text;
    this.location = data.location;
    this.type = this.text.startsWith('error') ? 'error' :
      this.text.startsWith('warning') ? 'warning' :
      this.text.startsWith('info') ? 'info' :
      'unknown';
    this.initViz();
  }

  initViz() {
    this.button = new Button(this.text, this.messageHandle, this);
    this.button.color =
      this.type == 'error' ? '#FD9A99' :
      this.type == 'warning' ? '#FFDDA2' :
      this.type == 'info' ? '#D6E8A0' :
      '#FFFFFF';
    this.button.strokeWeight = 1;
    this.button.strokeColor = '#888888';
    this.button.padding = 4;

    this.setViz(this.button);
  }

  draw(p) {
    this.viz.draw(p);
  }

  messageHandle() {
    // take care: this instanceof Button
    return this.manager.handleMessage()
  }

  handleMessage() {
    console.log('PRESSED ' + this.text);
    return this.location;
  }
}

class Lifeline extends HasViz {
  constructor(data, states, group) {
    super(data); // {index, header, activities, labels}
    this.index = data.index;
    this.activities = this.data.activities.map(activity => new Activity(activity, this));
    this.bars = [];
    this.group = group; // surrounding LifelineGroup
    this.eligibles =
      this.data.labels.filter(label => !label.illegal)
      .concat(this.data.labels.filter(label => label.illegal))
      .map(eligible => new Eligible(eligible));
    // postpone header creation because eligible width influences header width
    this.header = data.header ? new Header(data.header, states, this) : null;
    this.events = [];
    this._length = 1;
    this.initViz();
  }

  initViz() {
    this.line = new VLine(0);
    this.line.color = '#888888';
    // header is done separately!
    this.frame = new Frame([this.line]);
    this.setViz(this.frame);
    this.update();
  }

  get length() {
    return this._length;
  }

  set length(length) {
    this._length = length;
    this.isModified();
  }

  update() {
    if (this.upToDate) return;
    this.frame.content = [this.line].concat(this.bars, this.eligibles);
    this.viz.update(); // updates all items
    let len = globals.vTime(this.length);
    this.line.boundsHeight = len;
    let lx = this.line.bounds.x + this.line.bounds.width/2;
    this.bars.forEach(bar => {
      bar.hCenter(lx);
    });
    this.eligibles.forEach((eligible, i) => {
      eligible.vMove(len + i*globals.eligibleVertical);
      eligible.hCenter(lx);
    });
    this.viz.update();
    this.upToDate = true;
  }

  align() {
    this.update();
    // mid aligns with header; use absolute coordinates
    let parentbnd = this.group ? this.group.absoluteBounds : { x: 0 };
    let headermid = this.header.absoluteBounds.x + this.header.midXOffset;
    this.move(headermid - (parentbnd.x + this.midXOffset), 0);
    this.viz.update();
  }

  get isVisible() {
    return this.group.isOpen && this.group.isVisible;
  }

  get midXOffset() {
    return this.bounds.width/2;
  }

  get headerXOffset() {
    return this.header.midXOffset - this.midXOffset;
  }

  findActivity(key) {
    return this.activities.find(activity => activity.key == key);
  }

  set state(stateEntry) {
    if (stateEntry)
      this.header.state = stateEntry;
  }

  highlight(on) {
    this.viz.content[0].highlight(on);
  }
}

class ClosedLifeline extends Lifeline {
  constructor(data, group, closedHeaderGroup) {
    super(data, [], group);
    this.header = closedHeaderGroup;
    let lls = closedHeaderGroup.lifelineGroup.allLifelines;
  }

  align() {
    this.update();
    // mid aligns with header; use absolute coordinates
    let parentbnd = this.group.absoluteBounds;
    let headermid = this.header.absoluteBounds.x + this.header.midXOffset;
    this.move(headermid - (parentbnd.x + this.midXOffset), 0);
    this.viz.update();
  }
}

class LifelineGroup extends HasViz {
  constructor(data, states, group, body) {
    super(data); // {name, lifelines}
    this.name = data.name;
    this.states = states;
    this.group = group; // surrounding LifelineGroup
    this.body = body;
    this.elements = data.lifelines.map(lldata => {
      if (lldata.lifelines) {
        return new LifelineGroup({name: lldata.name, lifelines: lldata.lifelines}, this.states, this, this.body);
      } else {
        return new Lifeline(lldata, this.states, this);
      }
    });
    this.foreign = this.isForeign(this.elements);
    if (this.foreign) {
      this.elements = this.elements.filter(element =>
        !(element.header instanceof Header && element.header.role == 'foreign'));
    }
    this.light = this.group ? !this.group.light : true;
    this.header = new HeaderGroup(this.name, this.elements, this.foreign, this, this.states, this.light);
    this.isOpen = this.header.isOpen;
    let dd = {index: this.firstLifeline.index+'c',
              length: this.firstLifeline.length,
              header: null,
              activities: [],
              labels: [],
              bars: []};
    this.closedLifeline = new ClosedLifeline(dd, this.group, this.header);
    this.initViz();
  }

  setOpenClose(open) {
    this.isOpen = open;
    this.isModified();
  }

  get isVisible() {
    return this.group.isOpen && this.group.isVisible;
  }

  isForeign(elements) {
    return elements.find(element =>
        element.header instanceof Header && element.header.role == 'foreign') != null;
  }

  initViz() {
    this.frame = new Frame([]);
    this.setViz(this.frame);
    this.update();
  }

  update() {
    if (this.isOpen)  {
      this.frame.content = this.elements;
    } else{
      this.frame.content = [this.closedLifeline];
    }
    this.viz.update();
    this.align();
  }

  align() {
    if (this.isOpen)  {
      this.elements.forEach(element => element.align());
      this.viz.update();
    } else {
      this.closedLifeline.align();
      this.frame.hMove(0);
    }
  }

  setEvents(events) {
    this.internalEvents = [];
    this.externalFromEvents = [];
    this.externalToEvents = [];
    let all = this.allLifelines;
    events.forEach(event => {
      let fromll = all.find(ll => ll == event.from.lifeline);
      let toll = event.to ? all.find(ll => ll == event.to.lifeline) : null;
      if (fromll && (!event.to || toll)) this.internalEvents.push(event);
      else if (fromll) this.externalFromEvents.push(event);
      else if (toll) this.externalToEvents.push(event);
    });
    this.elements.forEach(elt => {
      if (elt instanceof LifelineGroup) elt.setEvents(events);
    });
  }

  filterVisibleEvents(events) {
    let result = events;
    if (this.isOpen) {
      this.elements.forEach(elt => {
        if (elt instanceof LifelineGroup) result = elt.filterVisibleEvents(result);
      });
    } else {
      result = events
        .filter(evt => !this.internalEvents.find(e => e == evt))
        .map(evt => {
          if (this.externalFromEvents.find(e => e == evt)) {
            let from = evt.from.copy(this.closedLifeline);
            this.closedLifeline.activities.push(from);
            return Event.copyEvent(evt, from, evt.to);
          } else if (this.externalToEvents.find(e => e == evt)) {
            let to = evt.to.copy(this.closedLifeline);
            this.closedLifeline.activities.push(to);
            return Event.copyEvent(evt, evt.from, to);
          } else {
            return evt;
          }
        });
      this.closedLifeline.update();
    }
    return result;
  }

  set length(length) {
    this.elements.forEach(elt => {
      elt.length = length;
      elt.update();
    });
    this.closedLifeline.length = length;
    this.closedLifeline.update();
  }

  get midXOffset() {
    return this.isOpen ? this.elements[0].midXOffset : this.bounds.width/2;
  }

  get headerXOffset() {
    return this.header.midXOffset - this.midXOffset;
  }

  get firstLifeline() {
    let frst = this.elements[0];
    return frst instanceof Lifeline ? frst : frst.firstLifeline;
  }

  get allLifelines() {
    let result = [];
    this.elements.forEach(elt => {
      if (elt instanceof Lifeline) {
        result.push(elt);
      } else {
        if (elt.isOpen) {
          result = result.concat(elt.allLifelines);
        } else {
          result.push(elt.closedLifeline);
        }
      }
    });
    return result;
  }

  findActivity(key) {
    return this.elements
      .map(element => element.findActivity(key))
      .find(act => act != null);
  }

  findLifeline(index) {
    let result = null;
    let lifeline = this.elements.forEach(element => {
      if (element instanceof Lifeline) {
        if (element.index == index) result = element;
      } else {
        result = result || element.findLifeline(index);
      }
    });
    if (this.closedLifeline.index == index)
      result = this.closedLifeline;
    return result;
  }

  getComponentLifelines() {
    let result = [];
    this.elements.forEach(element => {
        if (element instanceof Lifeline) {
          if (element.header.role == "component") result.push(element);
        } else {
          result = result.concat(element.getComponentLifelines());
        }
    });
    return result;
  }

  set state(state) {
    if (state) {
      this.elements.forEach(element => {
        if (element instanceof Lifeline) {
          let entry = state.find(st => st.index == element.index);
          if (entry)
            element.state = entry.state;
        } else {
          element.state = state;
        }
      });
      this.isModified();
    }
  }

  allActivities() {
    let result = [];
    this.elements.forEach(elt => {
      if (elt instanceof LifelineGroup) {
        result = result.concat(elt.allActivities());
      } else {
        result = result.concat(elt.activities);
      }
    });
    return result;
  }

  contains(lifeline) {
    let result = false;
    if (lifeline == this.closedLifeline) result = true;
    this.elements.forEach(element => {
      if (element instanceof Lifeline) {
        if (element == lifeline) result = true;
      } else {
        if (element.contains(lifeline)) result = true;
      }
    });
    return result;
  }
}

class Event extends HasViz {
  constructor(data, body) {
    super(data); // {text, from, to, type, index}
    this.body = body;
    this.from = body.findActivity(this.data.from);
    this.to = body.findActivity(this.data.to);
    this.type = data.type;
    this.index = data.index;
    if (this.type == 'error') {
      this.messages = this.data.messages.map(msg => new Message(msg));
    }
    this.initViz();
  }

  initViz() {
    let content = [];
    if (this.type != 'error') {
      let arrow = new HArrow(0, 0, 0);
      if (this.type == 'return') arrow.color = '#888888';
      let text = new Text(this.data.text);
      content = content.concat([arrow, text]);
    } else {
      let img = new Alert();
      content = content.concat([img]);
      content = content.concat(this.messages);
    }
    let frame = new Frame(content); // (0, 0) located
    this.setViz(frame);
    this.update();
  }

  update() {
    if (this.upToDate) return;
    this.viz.update();
    let frame = this.viz;
    // store frame vertical position for later:
    let framey = frame.bounds.y;
    let frombnd = this.from.lifeline.relativeBounds(this.body.viz);
    let fromx = frombnd.x + frombnd.width/2;
    let tobnd = (this.to ? this.to.lifeline : this.from.lifeline).relativeBounds(this.body.viz);
    let tox = tobnd.x + tobnd.width/2;

    // compensate for activity bar width:
    let left;
    if (fromx == tox) {
      left = tox + globals.barwidth/2;
    } else if (fromx < tox) {
      left = fromx + globals.barwidth/2;
    } else {
      left = tox + globals.barwidth/2;
    }
    if (this.type != 'error') {
      let arrow = frame.content[0];
      let text = frame.content[1];

      // compensate for activity bar width:
      if (fromx == tox) {
        arrow.setPoints(20, 0, 0);
      } else if (fromx < tox) {
        arrow.setPoints(0, tox-fromx-globals.barwidth, 0);
      } else {
        arrow.setPoints(fromx-tox-globals.barwidth, 0, 0);
      }
      text.update();
      if (fromx == tox) text.move(0, 0);
      else text.hCenter((fromx+tox)/2-left);
      text.vMove(-(text.bounds.height+2));
      frame.update();
      frame.hMove(left);
      if (fromx != tox) frame.hCenter((fromx+tox)/2);
    } else {
      let img = frame.content[0];
      this.messages.forEach((msg, i) => {
        msg.move(img.bounds.x+img.bounds.width+15, i*(msg.bounds.height+2));
      });
      frame.update();
      frame.hMove(left);
      frame.hCenter((fromx+tox)/2);
    }
    // restore frame vertical position!
    frame.vMove(framey);
    frame.update();
    this.upToDate = true;
  }

  vSet(y) {
    // set the location of the arrow point to 'y'
    let frame = this.viz;
    let arrow = frame.content[0];
    let off = arrow.bounds.y + arrow.bounds.height/2;
    this.vMove(y-off);
  }

  highlight(on) {
    this.viz.content[0].highlight(on);
  }

  get hashKey() {
    let fromll = this.from.lifeline;
    let toll = this.to ? this.to.lifeline : fromll;
    let key = ''+this.index+'.'+fromll.index+'.'+toll.index;
    return key;
  }

  // hashing: avoid re-creating existing Events
  static Table = new Map();

  static newEvent(data, body) {
    let event = new Event(data, body);
    Event.Table.set(event.hashKey, event);
    return event;
  }

  static copyEvent(event, from, to) {
    let fromll = from.lifeline;
    let toll = to ? to.lifeline : fromll;
    let key = ''+event.index+'.'+fromll.index+'.'+toll.index;
    let hashed = Event.Table.get(key);
    if (!hashed) {
      hashed = new Event(event.data, event.body);
      hashed.from = from;
      hashed.to = to;
      hashed.deltaTime = event.deltaTime;
      hashed.initViz();
      Event.Table.set(key, hashed);
    }
    return hashed;
  }
}

class Body extends HasViz {
  constructor(data) {
    super(data); // {lifelines, events, states}
    let datallg = this.partition(this.data.lifelines, '');
    this.lifelineGroup = new LifelineGroup(datallg, this.data.states, null, this);
    this.events = this.data.events
      .map(d => Event.newEvent(d, this)); // need global info!
    // add relative time deltaTime spent in this event
    this.events.forEach((e, i) => {
      e.deltaTime = 1;
      if (i < this.events.length-1) {
        let next = this.events[i+1];
        e.deltaTime = next.from.time - e.from.time;
      }
    });
    this.visibleEvents = []; // calculated in this.filterVisibleEvents()
    this.filterIsUptodate = false; // force recalculation visibleEvents
    this.lifelineGroup.setEvents(this.events);
    this.header = this.lifelineGroup.header;
    this.initViz();
  }

  initViz() {
    this.frame = new Frame([this.lifelineGroup]); // events will be added later
    this.setViz(this.frame);
    this.update();
  }

  update() {
    if (this.upToDate) return;
    this.header.update();
    this.filterVisibleEvents();
    this.viz.update();
    this.upToDate = true;
  }

  partition(lldata, name) {
    let prefix = (ll) => {
      let str = ll.header.instance;
      str = (name == '') ? str : str.replace(name + '.', '');
      return str.replace(/\..*/,'');
    };
    // step1:
    let groups = [];
    let group = null;
    lldata.forEach(ll => {
      if (group) {
        if (prefix(group[0]) == prefix(ll)) {
          group.push(ll);
        } else {
          groups.push(group);
          group = [ll];
        }
      } else {
        group = [ll];
      }
    });
    if (group) {
      groups.push(group);
    }

    let elements = groups.map(group => {
      if (group.length == 1) {
        return group[0];
      } else {
        let p = prefix(group[0]);
        let ename = (name == '') ? p : name + '.' + p;
        return this.partition(group, ename);
      }
    });
    return {name: name, lifelines: elements};
  }

  filterVisibleEvents() {
    if (this.filterIsUptodate) return;
    this.visibleEvents = this.lifelineGroup.filterVisibleEvents(this.events)
      .sort((e1, e2) =>  e1.index - e2.index);
    console.log('#events: ' + this.visibleEvents.length);
    this.frame.content = [this.lifelineGroup].concat(this.visibleEvents);
    this.viz.update();

    this.hMove(this.headerXOffset);
    this.align();

    // events vertical distribution:
    this.lifelineGroup.vMove(0);
    let time = 0;
    this.visibleEvents.forEach(event => {
      event.update();
      event.vSet(globals.vTime(time));
      event.upToDate = true;
      time += event.deltaTime;
    });
    // adapt lifeline length to number of filtered events:
    time += 1;
    // compensate lifeline length for error event:
    let elen = this.visibleEvents.length;
    if ( elen > 0 && this.visibleEvents[elen-1].type == 'error') {
      time += 2;
    }
    this.lifelineGroup.length = time+1;

    // activity bars:
    this.setBars(this.visibleEvents);
    this.filterIsUptodate = true;
  }

  setBars(events) {
    //helper functions:
    let isOpen = bar => {
      return bar.endTime == -1;
    };
    let startBar = (lifeline, time) => {
      if (lifeline.bars.length == 0 || !isOpen(lifeline.bars[lifeline.bars.length-1])) {
        let bar = new ActivityBar({startTime: time, endTime: -1, blocked: []}, lifeline);
        bar.initViz();
        lifeline.bars.push(bar);
      }
    };
    let endBar = (lifeline, time) => {
      if (lifeline.bars.length == 0) return;
      let bar = lifeline.bars[lifeline.bars.length-1];
      if (!bar) return;
      if (bar.blocked.length > 0 && isOpen(bar.blocked[bar.blocked.length-1]))
        endBlocked(lifeline, time);
      bar.endTime = time;
      bar.update();
    };
    let startBlocked = (lifeline, time) => {
      // pre: lifeline.bars.length > 0 && isOpen(lifeline.bars[lifeline.bars.length-1])
      let bar = lifeline.bars[lifeline.bars.length-1];
      if (!bar) return;
      let blocked = new BlockedBar({startTime: time, endTime: -1}, lifeline);
      blocked.initViz();
      bar.blocked.push(blocked);
    };
    let endBlocked = (lifeline, time) => {
      if (lifeline.bars.length == 0) return;
      let bar = lifeline.bars[lifeline.bars.length-1];
      if (bar.blocked.length == 0) return;
      let blocked = bar.blocked[bar.blocked.length-1];
      blocked.endTime = time;
      blocked.update();
    };

    // initial: empty all lifeline bar lists
    this.lifelineGroup.allLifelines.forEach(ll => {
      ll.bars = [];
      ll.upToDate = false;
    });

    let activeLine = null;
    let lastTime = -1;
    let time = 0;
    events.forEach(event => {
      let lifelineFrom = event.from.lifeline;
      // 'to' might be missing!
      let lifelineTo = event.to ? event.to.lifeline : null;
      // close previous bar upon lifeline switch
      if (activeLine && activeLine != lifelineFrom) {
        endBar(activeLine, lastTime);
      }
      if (event.type == 'in') {
        if (lifelineFrom.header.role == 'provides') {
          startBar(lifelineFrom, time);
        }
        startBlocked(lifelineFrom, time);
        if (lifelineTo) {
          startBar(lifelineTo, time);
          activeLine = lifelineTo;
          lastTime = time;
        } else {
          activeLine = null;
        }
      } else if (event.type == 'return') {
        endBar(lifelineFrom, time);
        if (!lifelineTo) {
          activeLine = null;
          lastTime = -1;
        } else if (lifelineTo.header.role == 'provides') {
          endBar(lifelineTo, time);
          activeLine = null;
          lastTime = -1;
        } else {
          endBlocked(lifelineTo, time);
          activeLine = lifelineTo;
          lastTime = time;
        }
      } else if (event.type == 'out') {
        if (!lifelineTo) {
          activeLine = null;
          lastTime = -1;
        } else if (lifelineFrom.index == lifelineTo.index) {
          // qout
          startBar(lifelineTo, time);
          activeLine = lifelineTo;
          lastTime = lifelineTo ? time : -1;
        } else {
          // either qin on lifelineTo or out on provides
          if (lifelineTo.header.role == 'component'
              || lifelineTo.header.role == 'system' // closed
              || lifelineTo.header.role == 'interface') {
            // qin
            startBar(lifelineFrom, time);
            activeLine = lifelineFrom;
            lastTime = time;
          } else {
            // out to provides
            activeLine = lifelineFrom;
            lastTime = time;
          }
        }
      }
      time += event.deltaTime;
    });

    // finit: close all open bars:
    let close = (lifeline) => {
      if (lifeline.bars.length > 0 && isOpen(lifeline.bars[lifeline.bars.length-1])) {
        endBar(lifeline, lifeline.length-2)
      }
    };
    let closeGroup = (group) => {
      if (group.isOpen) {
        group.elements.forEach(elt => {
          if (elt instanceof LifelineGroup) closeGroup(elt);
          else close(elt);
        });
      } else {
        close(group.closedLifeline);
      }
    };
    closeGroup(this.lifelineGroup);
  }

  updateEvents() {
    this.events.forEach(event => {
      // force!
      event.upToDate = false;
      event.update();
    });
  }

  get headerXOffset() {
    return this.lifelineGroup.headerXOffset;
  }

  align() {
    this.lifelineGroup.align();
    //this.events.forEach(event => event.update());
    this.viz.update();
  }

  findActivity(key) {
    return this.lifelineGroup.findActivity(key);
  }

  findLifeline(index) {
    return this.lifelineGroup.findLifeline(index);
  }

  getComponentLifelines() {
    return this.lifelineGroup.getComponentLifelines();
  }

  set state(state) {
    if (state) {
      this.lifelineGroup.state = state;
      this.isModified();
    }
  }

  reorder(header) {
    this.header.reorder(header);
    this.updateEvents();
  }

  restoreLifelineOrder(lifelineOrder) {
    this.header.restoreLifelineOrder(lifelineOrder);
    this.filterIsUptodate = false;
    this.update();
  }
}

class SequenceDiagram extends HasViz {
  constructor(data) {
    super(data); // {lifelines, events, states}
    // restore lifeline index field:
    data.lifelines.forEach( (ll, index) => ll.index = index);
    data.events.forEach( (event, index) => event.index = index);
    // add lifeline index to states:
    if (data.states) {
      data.states.forEach(state => {
        state.forEach(stateEntry => {
          let ll = data.lifelines.find(ll => ll.header.instance == stateEntry.instance);
          stateEntry.index = ll ? ll.index : 0;
        });
      });
    }
    // reset Event hash table:
    Event.Table.clear();
    this.body = new Body(data);
    this.header = this.body.header;
    this.states = data.states;
    this.initViz();
  }

  initViz() {
    this.setViz(new Frame([this.body, this.header]));
    this.move(0, 0);
    this.update();
  }

  update() {
    this.viz.update();
    this.header.vMove(0);
    this.body.vMove(this.header.bounds.height);
    this.viz.updateBounds();
    this.upToDate = true;
  }

  findActivity(key) {
    return this.body.findActivity(key);
  }

  setActive(stateIndex) {
    if (this.states && stateIndex < this.states.length)
      this.body.state = this.states[stateIndex];
  }

  get lifelineOrder() {
    return this.body.header.lifelineOrder;
  }

  restoreLifelineOrder(lifelineOrder) {
    this.body.restoreLifelineOrder(lifelineOrder);
  }
}
