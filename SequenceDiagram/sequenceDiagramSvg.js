/*
 * Copyright (C) 2021, 2022, 2023, 2024 Rob Wieringa <rma.wieringa@gmail.com>
 * Copyright (C) 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
 * Copyright (C) 2022 Paul Hoogendijk <paul@dezyne.org>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

class SequenceDiagramSvg extends DiagramSvg {

  constructor(parent) {
    super(parent);
    /*
     * super defines interface to the outside world, through functions
     *   in.draw(data)
     *   in.dimensions(px, py, width, height)
     *   out.selected(location)
     *
     * extension:
     */
    this.out.event = function (event) {
        console.log('event: %j', event);
    };

    this.data = null;
    this.message = null;

    this.nrFocusFrames = 10;
    this.focusShift = {x:0, y:0};
    this.focusFrame = this.nrFocusFrames;

    this.activeEventIndex = -1;
    this.highlightedEvent = null;
    this.eventFocus = 'center'; // from {'left', 'center', 'right'}
    this.activeLifelineIndex = -1;
    this.highlightedLifeline = null;
    this.lifelineOrder = null;
  }

  /*
   * These abstrace methods have to be defined in this class:
   *
   * initDiagram() { }
   * get svg() { }
   * selection(px, py) { }
   * handleKey(e) { }
   * handleMouseClick(e) { }
   * dragIt(px, py) { }
   * stopDraggingIt() { }
   * get animating() { return false; }
   * animationUpdate() { }
   */

  initDiagram() {
    try {
      this.diagram = new SequenceDiagram(this.data);
      let extension = this.sameHeaders(this.previousData, this.data) && this.lifelineOrder;
      if (extension) {
        this.diagram.restoreLifelineOrder(this.lifelineOrder);
      }
      // reset internal state
      if (extension) {
        if (this.activeLifelineIndex != -1) {
          this.highlightedLifeline = this.diagram.body.findLifeline(this.activeLifelineIndex);
          this.highlightedLifeline.highlight(true);
        }
        this.lifelineOrder = null;
      } else {
        this.activeLifelineIndex = -1;
        this.highlightedLifeline = null;
        this.lifelineOrder = null;
      }
      this.drawing = new Drawing(this.diagram);
      this.drawing.padding.top = 0;
      this.drawing.update();
      this.world.content = this.drawing;
      this.activeEventIndex = this.diagram.body.events.length-1;
      this.selectNextActiveEvent('here');
      let componentLifelines = this.diagram.body.getComponentLifelines()
      if (!extension && componentLifelines.length==1) {
        this.setActiveLifeline(componentLifelines[0].index);
      }
      this.drawing.update();
      this.highlightActiveEvent();
      this.setCursor('default');
    } catch(e) {
      this.diagram = null;
      this.message = new BoundingBox(new Text(''));
      this.message.padding = 30;
      this.message.content.size = 30;
      this.message.color = '#F37361';
      this.setMessage(this.message, 'invalid input: see console [F12] for details');
      console.log('%j: %j', e, this.data);
      this.drawing = new Drawing(this.message);
    }
  }

  sameHeaders(data1, data2) {
    if (!data1 || !data2) return false;
    let headers1 = data1.lifelines.map(ll => ll.header.instance).join(',');
    let headers2 = data2.lifelines.map(ll => ll.header.instance).join(',');
    return headers1 == headers2;
  }

  setMessage(diagram, msg) {
    diagram.content.text = msg;
    diagram.content.update();
    diagram.center(this.width/2, this.height/2);
    diagram.update();
  }

  get svg() {
    this.scrollbars.update();
    this.world.set(this.drawing.bounds);
    if (this.diagram) {
      this.moveHeaderToTop(); // TODO: is this the best place?
    }
    this.svgDrawingPlus('StateDiagramSvg');
    // nothing sequence diagram specific
    return this._svg;
  }

  get animating() {
    return this.diagram && this.focusAnimating;
  }

  animationUpdate() {
//    this.drawing.update();
  }

  moveHeaderToTop() {
    let topLeft = this.world.canvasToWorld(0, 0);
    let bnd = this.diagram.absoluteBounds;
    let header = this.diagram.header;
    header.vMove(Math.max(0, topLeft.y - bnd.y));
  }

  selection(px, py) {
    let sel = this.diagram.objectsAt(px, py);
    return this.selectionOfKlass(sel, Button)
      || this.selectionOfKlass(sel, Header)
      || this.selectionOfKlass(sel, HeaderGroup)
      || this.selectionOfKlass(sel, Event)
      || this.selectionOfKlass(sel, Lifeline);
  }

  lastEvent() {
    let nrEvents = this.diagram.body.events.length;
    return (nrEvents > 0) ? this.diagram.body.events[nrEvents-1] : null;
  }

  hasMatchingLifeline(activity, lifeline) {
    return activity && lifeline && activity.lifeline.index == lifeline.index;
  }

  shiftActiveEvent(right) {
    function shift(pos) {
      if (right) return (pos == 'left') ? 'center' : 'right';
      else return (pos == 'right') ? 'center' : 'left';
    }
    if (this.highlightedEvent) {
      this.eventFocus = shift(this.eventFocus);
      this.focus(this.highlightedEvent, this.eventFocus);
      this.redrawNeeded = true;
    }
  }

  selectNextActiveEvent(direction) {
    let nrEvents = this.diagram.body.events.length;
    let incActiveEventIndex = () => {
      this.activeEventIndex++;
      if (this.activeEventIndex < nrEvents-1) {
        let active = this.diagram.body.events[this.activeEventIndex];
        if (!this.diagram.body.visibleEvents.find(e => e.index == active.index))
          incActiveEventIndex();
      }
    };
    let decActiveEventIndex = () => {
      this.activeEventIndex--;
      if (this.activeEventIndex >= 0) {
        let active = this.diagram.body.events[this.activeEventIndex];
        if (!this.diagram.body.visibleEvents.find(e => e.index == active.index))
          decActiveEventIndex();
      }
    };
    let matches = (eventIndex, lifeline) => {
      if (!lifeline) return true;
      let event = this.diagram.body.events[eventIndex];
      let qin = (event.type == 'out') &&
                (event.from.lifeline.index != event.to.lifeline.index) &&
                (event.to.lifeline.header.role == 'component');
      return this.hasMatchingLifeline(event.from, lifeline) ||
        (this.hasMatchingLifeline(event.to, lifeline) && !qin);
    };
    if (direction == 'down') {
      if (this.activeEventIndex < nrEvents-1)
        incActiveEventIndex();
      while (this.activeEventIndex < nrEvents-1 && !matches(this.activeEventIndex, this.highlightedLifeline))
        incActiveEventIndex();
    } else if (direction == 'up') {
      if (this.activeEventIndex >= 0)
        decActiveEventIndex();
      while (this.activeEventIndex >= 0 && !matches(this.activeEventIndex, this.highlightedLifeline))
        decActiveEventIndex();
    } else if (direction == 'here') {
      // tricky:
      this.activeEventIndex++;
      decActiveEventIndex();
      while (this.activeEventIndex >= 0 && !matches(this.activeEventIndex, this.highlightedLifeline))
        decActiveEventIndex();
    }
    this.highlightActiveEvent();
    this.redrawNeeded = true;
  }

  selectActivity(event, selectedLifeline) {
    let from_selected = this.hasMatchingLifeline(event.from, selectedLifeline);
    let to_selected = this.hasMatchingLifeline(event.to, selectedLifeline);
    let select_from = (from_selected || event.type =='return') && !to_selected;
    let swap = keyboard.hasMod('Control');
    if ((select_from != swap) || !event.to)
      return event.from;
    else
      return event.to;
  }

  highlightActiveEvent() {
    if (this.highlightedEvent) {
      this.highlightedEvent.highlight(false);
    }
    if (this.activeEventIndex >= 0) {
      this.highlightedEvent = this.diagram.body.visibleEvents.find(e => e.index == this.activeEventIndex);
      this.highlightedEvent.highlight(true);
      this.eventFocus = 'center';
      this.focus(this.highlightedEvent, this.eventFocus);
      let activity = this.selectActivity(this.highlightedEvent, this.highlightedLifeline);
      if (activity) {
        let location = activity.location;
        if (location) this.out.selected({...location,
                                         'working-directory': this.data['working-directory']});
      }
    } else {
      this.highlightedEvent = null;
      // do not change focus upon deselect
      // this.focus(null);
    }
    let index = Math.min(this.activeEventIndex+1, this.diagram.states.length-1);
    this.diagram.setActive(index);
    this.diagram.update();
  }

  resetHighlightEvent() {
    this.activeEventIndex = -1;
    this.highlightActiveEvent();
  }

  resetActiveLifeline() {
    if (this.highlightedLifeline) {
      this.highlightedLifeline.highlight(false);
      this.highlightedLifeline.header.highlight(false);
    }
    this.activeLifelineIndex = -1;
  }

  setActiveLifeline(index) {
    // deselect by second activate:
    this.activeLifelineIndex = (this.activeLifelineIndex == index) ? -1 : index;

    if (this.highlightedLifeline) {
      this.highlightedLifeline.highlight(false);
      this.highlightedLifeline.header.highlight(false);
    }
    if (this.activeLifelineIndex != -1) {
      this.highlightedLifeline = this.diagram.body.findLifeline(this.activeLifelineIndex);
      this.highlightedLifeline.highlight(true);
      this.highlightedLifeline.header.highlight(true);
      this.redrawNeeded = true;
    } else {
      this.highlightedLifeline = null;
    }
    this.highlightActiveEvent();
  }

  get bottomRight() {
    let w = this.world.width;
    if (this.scrollbars.vertical.visible) w -= this.scrollbars.width;
    let h = this.world.height;
    if (this.scrollbars.horizontal.visible) h -= this.scrollbars.width;
    return this.world.canvasToWorld(w, h);
  }

  focus(obj, at) {
    // obj instanceof Event or null
    let topLeft = this.world.canvasToWorld(0, 0);
    let bottomRight = this.bottomRight;
    let oldX = 0;
    let oldY = 0;
    let newX = 0;
    let newY = 0;
    let margin = 10/this.world.scale;
    let dbnd = this.diagram.bounds;
    let bodybnd = this.diagram.body.absoluteBounds;
    let headerbnd = this.diagram.header.absoluteBounds;
    if (obj) {
      let objbnd = obj.absoluteBounds;
      // extend object bounds with the width of both headers:
      let extendWidth = (hdr) => {
        let hbnd = hdr.absoluteBounds;
        if (hbnd.x < objbnd.x) {
          let diff = objbnd.x - hbnd.x;
          objbnd.x -= diff;
          objbnd.width += diff;
        }
        if (hbnd.x + hbnd.width > objbnd.x + objbnd.width) {
          let diff = (hbnd.x + hbnd.width) - (objbnd.x + objbnd.width);
          objbnd.width += diff;
        }
      };
      if (obj.from) extendWidth(obj.from.lifeline.header);
      if (obj.to) extendWidth(obj.to.lifeline.header);
      oldX = objbnd.x;
      newX = (objbnd.width > (bottomRight.x - topLeft.x))
        ? ((at == 'left') ? topLeft.x + margin
           : (at == 'center') ? (bottomRight.x + topLeft.x)/2 - objbnd.width/2
           : bottomRight.x - objbnd.width - margin)
        : (oldX < topLeft.x) ? topLeft.x + margin
        : (oldX + objbnd.width > bottomRight.x) ? bottomRight.x - objbnd.width - margin
        : oldX;
      oldY = objbnd.y;
      newY = (oldY < topLeft.y + headerbnd.height) ? topLeft.y + headerbnd.height + margin
        : (oldY + objbnd.height > bottomRight.y) ? bottomRight.y - objbnd.height - margin
        : oldY;
      if (obj.index == this.diagram.body.events.length-1) {
        // scroll down the bottom of body, to show the eligibles
        oldY = dbnd.y + dbnd.height;
        newY = (oldY > bottomRight.y) ? bottomRight.y - margin : oldY;
      }
    } else {
      // assure right side header is left-aligned to view,
      oldX = dbnd.x;
      newX = (oldX + dbnd.width > bottomRight.x) ? bottomRight.x - dbnd.width - margin
        : oldX;
      newX = (newX < topLeft.x) ? topLeft.x + margin
        : newX;
      // scroll up to show body top
      oldY = dbnd.y;
      newY = (oldY < topLeft.y + dbnd.height) ? headerbnd.y + headerbnd.height + margin
        : oldY;
    }
    if (newX != oldX || newY != oldY) {
      this.startFocusAnimation(newX - oldX, newY - oldY);
    }
  }

  startFocusAnimation(dx, dy) {
    this.focusShift = {x: 1/this.nrFocusFrames*dx, y: 1/this.nrFocusFrames*dy};
    this.world.shiftWorld(this.focusShift.x, this.focusShift.y);
    this.focusFrame = 1;
  }

  get focusAnimating() {
    if (this.focusFrame == this.nrFocusFrames) return false;
    this.world.shiftWorld(this.focusShift.x, this.focusShift.y);
    this.focusFrame++;
    return true;
  }

  handleKey(e) {
    if (keyboard.isKeyCode('F1') || keyboard.isKey('?')) {
      window.open('./helpSequence.html');
    } else if (keyboard.isKeyCode('ArrowDown') || keyboard.isKeyCode('ArrowDown', 'Control')) {
      this.selectNextActiveEvent('down');
    } else if (keyboard.isKeyCode('ArrowUp') || keyboard.isKeyCode('ArrowUp', 'Control')) {
      this.selectNextActiveEvent('up');
    } else if (keyboard.isKeyCode('ArrowRight')) {
      this.shiftActiveEvent(true); // right
    } else if (keyboard.isKeyCode('ArrowLeft')) {
      this.shiftActiveEvent(false); // left
    } else if (keyboard.isKey('-', 'Control')) {
      this.world.zoomAround(mouse.x, mouse.y, this.world.zoomOutFactor);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('+', 'Control') || keyboard.isKey('=', 'Control')) {
      this.world.zoomAround(mouse.x, mouse.y, this.world.zoomInFactor);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('0', 'Control')) {
      // horizontal only!
      this.world.fit({width: this.drawing.scaledBounds.width, height: 1});
      this.redrawNeeded = true;
    } else if (keyboard.isKey('1', 'Control')) {
      this.world.zoomAround(mouse.x, mouse.y, 1/this.world.scale);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('s', 'Control')) {
      this.saveAsSvg('sequence.svg');
    } else {
      return super.handleKey(e);
    }
    // suppress default behaviour:
    e.preventDefault();
    return false;
  }
  
  dragIt(px, py) {
    if (this.drag.ctrl) {
      // dragged the canvas
    } else if (this.drag.obj) {
      let obj = this.drag.obj;
      if (obj instanceof Header && (obj.role == 'provides' || obj.role == 'requires')) {
        this.dragHeader(obj, px, py);
      }
    }
  }

  
  dragHeader(header, px, py) {
    header.shift(px-header.bounds.x, 0);
    header.lifeline.align();
    this.diagram.body.reorder(header);
    this.redrawNeeded = true;
  }
  
  stopDraggingIt() {
    let obj = this.drag.obj;
    if (obj && obj instanceof Header && (obj.role == 'provides' || obj.role == 'requires')) {
      // restore obj's position by updating its group
      obj.lifeline.group.header.update();
      obj.lifeline.align();
      this.diagram.body.updateEvents();
      this.redrawNeeded = true;
    }
  }
  
  handleMouseClick(e) {
    let wpt = this.world.mousePoint();
    // take care: handle body and header separately
    this.resetHighlight();
    let obj = this.selection(wpt.x, wpt.y);
    if (obj) {
      if (obj instanceof Button) {
        let manager = obj.manager;
        if (manager instanceof Message) {
          let location = manager.location;
          if (location) this.out.selected({...location,
                                           'working-directory': this.data['working-directory']});
        } else if (manager instanceof Eligible) {
          this.setCursor('wait');
          // save order for diagram extension :
          this.lifelineOrder = this.diagram.lifelineOrder;
          // test: this.in.draw(this.data);
          this.out.event(manager.text);
        } else if (manager instanceof HeaderGroup) {
          obj.callback(e.ctrlKey);
          if (this.highlightedLifeline && manager.lifelineGroup.contains(this.highlightedLifeline)) {
            this.resetActiveLifeline();
          }
          this.selectNextActiveEvent('here');
          this.diagram.update();
          this.redrawNeeded = true;
        }
      } else if (obj instanceof Event) {
        this.activeEventIndex = obj.index;
        this.highlightActiveEvent();
      } else if (obj instanceof Header) {
        this.setActiveLifeline(obj.lifeline.index);
      } else if (obj instanceof HeaderGroup) {
        if (!obj.isOpen)
          this.setActiveLifeline(obj.lifelineGroup.closedLifeline.index);
      } else if (obj instanceof Lifeline) {
        this.setActiveLifeline(obj.index);
      }
    } else {
      this.resetHighlightEvent();
    }
    this.redrawNeeded = true;
  }
}
