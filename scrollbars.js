/*
 * Copyright (C) 2021, 2022, 2023, 2024 Rob Wieringa <rma.wieringa@gmail.com>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

class Scrollbar extends BuildingBlock {
  constructor(xp, yp, tracklen, trackwidth) {
    super();
    this.track = { x: xp, y: yp, length: tracklen, width: trackwidth, color: '#EEEEEE' };
    this.drawing = {pos: 0, length: 0};
    this.thumb = { pos: 0, length: 0, color: '#BBBBBB' };
    this.visible = true;
  }

  /*
   * Thumb length and position are proportional:
   *
   * .-------------------------------------------------.
   * |                       D                         |
   * |       .--------------------.                    |  drawing
   * |       |                    |                    |  after scaling
   * |   A   |         B          |         C          |  and dragging
   * .-------:--------------------:--------------------.
   *         |                    |
   *         |         d          | window
   *         .--------------------.
   *         |    XXXXX           | scrollbar with thumb
   *         .---:-----:----------.
   *           a    b       c
   *
   * A = - drawing.pos (since window starts at 0)
   * B = window width == track.length
   * D = drawing.length
   * A + B + C = D
   *
   * a = thumb.pos
   * b = thumb.length
   * d = track.length
   * a + b + c = d
   *
   * so B == d
   *
   * invariants:
   *
   *   A + B + C == D
   *   a + b + c == d
   *   proportionality, i.e: A/a == B/b == C/c == D/d
   *
   * So, with F = D/d:
   *   thumb.length = b = B/F = track.length/F
   *   thumb.pos = a = A/F = - drawing.pos/F
   */

  update() {
    let F = this.drawing.length / this.track.length;
    if (F <= 1) {
      this.visible = false;
    } else {
      this.visible = true;
      this.updateThumb(this.track.length / F, - this.drawing.pos / F);
    }
  }

  updateTrack(x, y, length) {
    if (this.track.x != x || this.track.y != y || this.track.length != length) {
      this.track.x = x;
      this.track.y = y;
      this.track.length = length;
      this.isModified();
    }
  }

  updateDrawing(length, pos) {
    if (this.drawing.length != length || this.drawing.pos != pos) {
      this.drawing.length = length;
      this.drawing.pos = pos;
    }
  }

  updateThumb(length, pos) {
    if (this.thumb.length != length || this.thumb.pos != pos) {
      this.thumb.length = length;
      this.thumb.pos = pos;
      this.isModified();
    }
  }

  onThumb(px, py) {
    return (this.onTrack(px, py) &&
            this.thumb.pos <= this.position(px, py) &&
            this.position(px, py) <= this.thumb.pos + this.thumb.length)
  }

  stepDirection(toZero, inc) {
    let tpos = this.thumb.pos;
    let incr = this.thumb.length * inc;
    tpos = toZero ? (tpos - incr) : (tpos + incr);
    tpos = Math.min(Math.max(tpos, 0), this.track.length - this.thumb.length);
    this.updateThumb(this.thumb.length, tpos);
  }

  step(px, py) {
    if (this.onTrack(px, py)) {
      if (this.position(px, py) >  this.thumb.pos + this.thumb.length)
        this.stepDirection(false, 3/4);
      else if (this.position(px, py) <  this.thumb.pos)
        this.stepDirection(true, 3/4);
    }
  }

  moveToThumb() {
    let F = this.drawing.length / this.track.length;
    this.updateDrawing(this.drawing.length, - this.thumb.pos * F);
  }

  dragThumb(tpos) {
    let pos = Math.min(Math.max(tpos, 0), this.track.length - this.thumb.length);
    this.updateThumb(this.thumb.length, pos);
  }


  // position(px, py) virtual

  // draw(p) { } virtual
}

class HorizontalScrollbar extends Scrollbar {
  constructor(xp, yp, tracklen, trackwidth) {
    super(xp, yp, tracklen, trackwidth);
  }

  update(parent) {
    this.updateTrack(0, parent.height - this.track.width, parent.width - this.track.width);
    this.updateDrawing(parent.drawing ? (parent.drawing.bounds.width * parent.world.scale) : 1,
                        parent.drawing ? parent.world.worldToCanvasX(parent.drawing.bounds.x) : 0);
    super.update();
  }

  onTrack(px, py) {
    return (this.visible &&
            this.track.x < px && px < this.track.x + this.track.length &&
            this.track.y < py && py < this.track.y + this.track.width);
  }

  moveWorld(parent) {
    if (this.visible) {
      this.moveToThumb();
      parent.world.x0 = parent.drawing ? (this.drawing.pos - parent.drawing.bounds.x * parent.world.scale) : 0;
      parent.world.changed = true;
    }
  }

  position(px, py) {
    return px - this.track.x;
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.element('g', 'HorizontalScrollbar');
    this.SvgVisible = Svg.element('g', 'visible');
    let track = Svg.element('rect', 'track');
    let thumb = Svg.element('rect', 'thumb');
    this.SvgVisible.append(track, thumb);
    if (this.visible) {
      this._svg.replaceChildren(this.SvgVisible);
      Svg.setPos(this.SvgVisible, {x: this.track.x, y: this.track.y});
      // track:
      track.setAttribute('stroke', 'none');
      track.setAttribute('fill', this.track.color);
      Svg.setBounds(track, {x: 0, y: 0, width: this.track.length, height: this.track.width,
                            scaleX: 1, scaleY: 1});
      // thumb:
      thumb.setAttribute('stroke', 'none');
      thumb.setAttribute('fill', this.thumb.color);
      Svg.setBounds(thumb, {x: this.thumb.pos, y: 2, width: this.thumb.length, height: this.track.width-4,
                            scaleX: 1, scaleY: 1});
    } else {
      this._svg.replaceChildren();
    }
    this.changed = false;
    return this._svg;
  }

  dragThumb(px, py) {
    super.dragThumb(px);
  }
}

class VerticalScrollbar extends Scrollbar {
  constructor(xp, yp, tracklen, trackwidth) {
    super(xp, yp, tracklen, trackwidth);
  }

  update(parent) {
    this.updateTrack(parent.width - this.track.width, 0, parent.height - this.track.width);
    this.updateDrawing(parent.drawing ? (parent.drawing.bounds.height * parent.world.scale) : 1,
                        parent.drawing ? parent.world.worldToCanvasY(parent.drawing.bounds.y) : 0);
    super.update();
  }

  onTrack(px, py) {
    return (this.visible &&
            this.track.x < px && px < this.track.x + this.track.width &&
            this.track.y < py && py < this.track.y + this.track.length);
  }

  moveWorld(parent) {
    if (this.visible) {
      this.moveToThumb();
      parent.world.y0 = parent.drawing ? (this.drawing.pos - parent.drawing.bounds.y * parent.world.scale) : 0;
      parent.world.changed = true;
    }
  }

  position(px, py) {
    return py - this.track.y;
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.element('g', 'VerticalScrollbar');
    this.SvgVisible = Svg.element('g', 'visible');
    let track = Svg.element('rect', 'track');
    let thumb = Svg.element('rect', 'thumb');
    this.SvgVisible.append(track, thumb);
    if (this.visible) {
      this._svg.replaceChildren(this.SvgVisible);
      Svg.setPos(this.SvgVisible, {x: this.track.x, y: this.track.y});
      // track:
      track.setAttribute('stroke', 'none');
      track.setAttribute('fill', this.track.color);
      Svg.setBounds(track, {x: 0, y: 0, width: this.track.width, height: this.track.length,
                            scaleX: 1, scaleY: 1});
      // thumb:
      thumb.setAttribute('stroke', 'none');
      thumb.setAttribute('fill', this.thumb.color);
      Svg.setBounds(thumb, {x: 2, y: this.thumb.pos, width: this.track.width-4, height: this.thumb.length,
                            scaleX: 1, scaleY: 1});
    } else {
      this._svg.replaceChildren();
    }
    this.changed = false;
    return this._svg;
  }

  dragThumb(px, py) {
    super.dragThumb(py);
  }
}

class Scrollbars extends BuildingBlock {
  constructor(parent) {
    super();
    this.parent = parent;
    this.width = 15;
    this.horizontal = new HorizontalScrollbar(0, 0, 1, this.width);
    this.vertical = new VerticalScrollbar(0, 0, 1, this.width);
    this.horizontal.parent = this;
    this.vertical.parent = this;
    this.update();
  }

  update() {
    this.horizontal.update(this.parent);
    this.vertical.update(this.parent);
  }

  moveWorld() {
    this.horizontal.moveWorld(this.parent);
    this.vertical.moveWorld(this.parent);
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.element('g', 'Scrollbars');
    let horizontal = this.horizontal.svg;
    let vertical = this.vertical.svg;
    let corner = Svg.element('rect', 'corner');
    this._svg.append(horizontal, vertical, corner);
    // corner:
    if (this.horizontal.visible && this.vertical.visible) {
      corner.setAttribute('stroke', 'none')
      corner.setAttribute('fill', '#BBBBBB');
      let bnd = { x: this.vertical.track.x, y: this.horizontal.track.y,
                  width: this.vertical.track.width, height: this.horizontal.track.width,
                  scaleX: 1, scaleY: 1};
      Svg.setBounds(corner, bnd);
    }
    this.changed = false;
    return this._svg;
  }

  onTracks(px, py) {
    return (this.horizontal.onTrack(px, py) || this.vertical.onTrack(px, py));
  }

  onThumb(px, py) {
    return this.horizontal.onThumb(px, py) || this.vertical.onThumb(px, py);
  }

  step(px, py) {
    this.horizontal.step(px, py);
    this.vertical.step(px, py);
  }
}
