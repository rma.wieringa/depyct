/*
 * Copyright (C) 2021, 2022, 2023, 2024 Rob Wieringa <rma.wieringa@gmail.com>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

class SystemDiagramSvg extends DiagramSvg {

  constructor(parent) {
    super(parent);
    /*
     * super defines interface to the outside world, through functions
     *   in.draw(data)
     *   in.dimensions(px, py, width, height)
     *   out.selected(location)
     *
     * extension: none
     */

    this.tooltipObj = null;
    this.tooltip = null;
    this.highlightedExtras = [];

    this.nrFocusFrames = 10;
    this.focusShift = {x:0, y:0};
    this.focusFrame = this.nrFocusFrames;

    this.diagramStack = []; // [{diagram: Instance, parent: System}]

    this.showInstance = true;
  }

  /*
   * These abstrace methods have to be defined in this class:
   *
   * initDiagram() { }
   * get svg() { }
   * selection(px, py) { }
   * handleKey(e) { }
   * handleMouseClick(e) { }
   * dragIt(px, py) { }
   * stopDraggingIt() { }
   * get animating() { return false; }
   * animationUpdate() { }
   */

  initDiagram() {
    this.showInstance = this.localStorageGetItem('system:showInstance') == 'true';
    if (this.showInstance === null) this.showInstance = true;
    this.diagram = new SUT(this.data).instance;
    Instance.activeInstance =  this.diagram;
    this.diagram.showInstance = this.showInstance;
    this.diagram.move(100, 100);
    this.diagramStack.push({diagram: this.diagram, parent: null});
    this.drawing = new Drawing(this.diagram);
    this.world.content = this.drawing;
    this.setCursor('default');
    this.resetHighlight();
  }

  get svg() {
    this.scrollbars.update();
    this.world.set(this.drawing.bounds);
    this.svgDrawingPlus('SystemDiagramSvg');

    //show summary:
    if (this.highlightedObj && this.world.scale < .75) {
      let summary = this.highlightedModel.summary;
      if (summary) {
        let sum = Svg.group('details');
        Svg.setPos(sum, {x: -summary.bounds.x+10, y: -summary.bounds.y+10});
        summary.update();
        let s = summary.svg;
        sum.append(s);
        this._svg.append(sum);
      }
    }
    //show tooltip:
    if (this.tooltipObj) {
      let tt = Svg.group('tooltip');
      Svg.setPos(tt, {x: mouse.x+20, y: mouse.y});
      let s = this.tooltip.svg;
      tt.append(s);
      this._svg.append(tt);
    }

    // var ser = new XMLSerializer();
    // var str = ser.serializeToString(this._svg);
    // console.log(str);

    return this._svg;
  }

  get animating() {
    return this.diagram && (this.diagram.animatingInstances.length > 0 || this.focusAnimating);
  }

  animationUpdate() {
    if (this.diagram) {
      let instlist = this.diagram.animatingInstances;
      if (instlist.length > 0) {
        instlist.forEach(inst => inst.animationStep());
      }
    }
  }

  mouseMove(e) {
    super.mouseMove(e);
    if (!this.mouseInCanvas) return;
    let obj = this.objAtMouse();
    if (obj && obj instanceof Port)
      this.setTooltip(obj);
    else
      this.setTooltip(null);
  }

  setTooltip(obj) {
    let old = this.tooltipObj;
    this.tooltipObj = obj;
    if (this.tooltipObj && this.tooltipObj != old) {
      if (this.tooltipObj instanceof Port) {
        let text = this.tooltipObj.name;
        this.tooltip = new BoundingBox(new Text(text));
        this.tooltip.padding = 3;
        this.tooltip.color = '#FFFCB0';
        this.tooltip.update();
      }
    }
    if (this.tooltipObj || old)
      this.redrawNeeded = true;
  }

  highlight(obj) {
    super.highlight(obj);
    this.resetHighlightExtras();
  }

  resetHighlight() {
    super.resetHighlight();
    this.resetHighlightExtras();
    this.redrawNeeded = true;
  }

  highlightExtras(lst) {
    this.highlightedExtras = lst;
    this.highlightedExtras.forEach(h => h.highlight(true));
    this.redrawNeeded = true;
  }

  resetHighlightExtras() {
    this.highlightedExtras.forEach(h => h.highlight(false));
    this.highlightedExtras = [];
  }

  get bottomRight() {
    let w = this.world.width;
    if (this.scrollbars.vertical.visible) w -= this.scrollbars.width;
    let h = this.world.height;
    if (this.scrollbars.horizontal.visible) h -= this.scrollbars.width;
    return this.world.canvasToWorld(w, h);
  }

  focus(obj) {
    let sbnd = this.diagram.scaledBounds;
    let obnd = obj.relativeBounds(this.diagram.viz);
    // absolute coordinates:
    let oldX = (obj == this.diagram) ? sbnd.x : sbnd.x+obnd.x;
    let oldY = (obj == this.diagram) ? sbnd.y : sbnd.y+obnd.y;
    let newX = oldX;
    let newY = oldY;
    let topLeft = this.world.canvasToWorld(0, 0);
    let bottomRight = this.bottomRight;
    let newXY = this.world.canvasToWorld(100,100);
    if (oldX < topLeft.x || oldX + obnd.width > bottomRight.x) newX = newXY.x;
    if (oldY < topLeft.y || oldY + obnd.height > bottomRight.y) newY = newXY.y;

    if (newX != oldX || newY != oldY) {
      this.startFocusAnimation(newX - oldX, newY - oldY);
    }
  }

  startFocusAnimation(dx, dy) {
    this.focusShift = {x: 1/this.nrFocusFrames*dx, y: 1/this.nrFocusFrames*dy};
    this.world.shiftWorld(this.focusShift.x, this.focusShift.y);
    this.focusFrame = 1;
    this.redrawNeeded = true;
  }

  get focusAnimating() {
    if (this.focusFrame == this.nrFocusFrames) return false;
    this.world.shiftWorld(this.focusShift.x, this.focusShift.y);
    this.focusFrame++;
    return true;
  }

  highlightAndFocus(obj) {
    this.highlight(obj);
    this.focus(obj);
  }

  handleMouseClick(e) {
    let obj = this.objAtMouse();
    if (obj) {
      if (obj instanceof Button) {
        obj.callback(e.ctrlKey);
        this.redrawNeeded = true;
      } else {
        this.highlight(obj);
        if (obj instanceof Port) {
          this.highlightExtras(this.path(obj));
          this.redrawNeeded = true;
        }
        let location = obj.location;
        if (e.ctrlKey) {
          if (obj instanceof Instance)
            location = obj.model && obj.model.location;
          else if (obj instanceof Port)
            location = obj.dinterface.location;
          else if (obj instanceof Binding)
            location = obj.fromPort.dinterface.location;
        }
        if (location) this.out.selected({...location, 'working-directory': this.data['working-directory']});
      }
    } else {
      this.resetHighlight();
    }
  }

  objAtMouse() {
    if (!this.mouseInCanvas) return null;
    if (!this.diagram) return null;
    let wpt = this.world.mousePoint();
    return this.selection(wpt.x, wpt.y);
  }

  selection(px, py) {
    let sel = this.diagram.objectsAt(px, py);
    return this.selectionOfKlass(sel, Button) ||
      this.selectionOfKlass(sel, Binding) ||
      this.selectionOfKlass(sel, Port) ||
      this.selectionOfKlass(sel, Instance) ||
      this.selectionOfKlass(sel, Component) ||
      this.selectionOfKlass(sel, Foreign) ||
      this.selectionOfKlass(sel, System);
  }

  path(port) {
    function follow(port, bnd) {
      let port2 = (bnd.fromPort == port) ? bnd.toPort : bnd.fromPort;
      let result = [port2];
      if (port2.model instanceof System) {
        let bnd2 = (port2.model == bnd.system) ? port2.externalBinding : port2.internalBinding;
        if (bnd2) {
          result.push(bnd2);
          result = result.concat(follow(port2, bnd2));
        }
      }
      return result;
    }

    let result = [port];
    let ibnd = port.internalBinding;
    let ebnd = port.externalBinding;
    if (ibnd) {
      result.push(ibnd);
      result = result.concat(follow(port, ibnd));
    }
    if (ebnd) {
      result.push(ebnd);
      result = result.concat(follow(port, ebnd));
    }
    return result;
  }

  handleKey(e) {
    if (keyboard.isKeyCode('F1') || keyboard.isKey('?')) {
      window.open('./helpSystem.html');
    } else if (keyboard.isKeyCode('Enter')) {
      // hack: avoid checkRepeatKey loop, since keyUp is not detected
      this.stopKeyTimeout();
      this.enterInstance();
    } else if (keyboard.isKeyCode('Backspace')) {
      this.exitInstance();
    } else if (keyboard.isKeyCode('ArrowDown', 'Control') ||
               keyboard.isKeyCode('ArrowUp', 'Control') ||
               keyboard.isKeyCode('ArrowDown') ||
               keyboard.isKeyCode('ArrowUp') ||
               keyboard.isKeyCode('ArrowRight') ||
               keyboard.isKeyCode('ArrowLeft')) {
      this.navigateInstance(e.key);
    } else if (keyboard.isKey('-')) {
      this.closeSystem();
    } else if (keyboard.isKey('+') || keyboard.isKey('=')) {
      this.openSystem();
    } else if (keyboard.isKey('-', 'Control')) {
      this.world.zoomAround(mouse.x, mouse.y, this.world.zoomOutFactor);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('+', 'Control') || keyboard.isKey('=', 'Control')) {
      this.world.zoomAround(mouse.x, mouse.y, this.world.zoomInFactor);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('0', 'Control')) {
      this.world.fit(this.drawing.scaledBounds);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('1', 'Control')) {
      this.world.zoomAround(mouse.x, mouse.y, 1/this.world.scale);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('s', 'Control')) {
      this.saveAsSvg('system.svg');
    } else if (keyboard.isKey('i')) {
      this.showInstance = !this.showInstance;
      this.localStorageSetItem('system:showInstance', this.showInstance);
      // update all, starting at SUT
      if (this.diagramStack.length > 0) {
        let sut = this.diagramStack[0].diagram;
        sut.showInstance = this.showInstance;
      }
      this.diagram.update();
      this.redrawNeeded = true;
    } else {
      return super.handleKey(e);
    }
    // suppress default behaviour:
    e.preventDefault();
    return false;
  }

  get highlightedInstance() {
    if (this.highlightedObj && this.highlightedObj instanceof Instance) return this.highlightedObj;
    else return null;
  }

  get highlightedModel() {
    if (!this.highlightedObj) return null;
    else if (this.highlightedObj instanceof Instance) return this.highlightedObj.model;
    else if (this.highlightedObj instanceof Component ||
             this.highlightedObj instanceof Foreign ||
             this.highlightedObj instanceof System) return this.highlightedObj;
    else return null;
  }

  get highlightedSystem() {
    let model = this.highlightedModel;
    if (model && model instanceof System) return model;
    else return null;
  }

  openSystem() {
    let system = this.highlightedSystem;
    if (system && !system.isOpen) {
      system.openClose();
      this.diagram.update();
      this.redrawNeeded = true;
    }
  }

  closeSystem() {
    let system = this.highlightedSystem;
    if (system && system.isOpen) {
      system.openClose();
      this.diagram.update();
      this.redrawNeeded = true;
    }
  }

  enterInstance() {
    let inst = this.highlightedInstance;
    if (inst) {
      this.diagram = inst;
      Instance.activeInstance =  this.diagram;
      this.diagramStack.push({diagram: inst, parent: inst.parent});
      this.drawing.updateContent(this.diagram);
      this.highlightAndFocus(this.diagram);
    }
  }

  exitInstance() {
    if (this.diagramStack.length > 1) {
      let current = this.diagramStack.pop();
      let next = this.diagramStack[this.diagramStack.length-1];
      // restore parent relation of current inst!
      current.diagram.parent = current.parent;

      this.diagram = next.diagram;
      Instance.activeInstance =  this.diagram;
      this.drawing.updateContent(this.diagram);
      this.highlightAndFocus(this.diagram);
    }
  }

  navigateInstance(keyCode) {
    let inst = this.highlightedInstance;
    if (inst) {
      this.navigate(inst, keyCode)
    } else if (!this.highlightedObj) {
      this.highlightAndFocus(this.diagram);
    }
  }

  navigate(instance, keyCode) {
    if (keyboard.isKeyCode('ArrowDown', 'Control')) {
      // navigate inward
      if (instance.model instanceof System && instance.model.isOpen) {
        let layer = instance.model.layers[0];
        if (layer) this.highlightAndFocus(layer.instances[0]);
      }
    } else if (keyboard.isKeyCode('ArrowUp', 'Control')) {
      // navigate outward; do not navigate outside diagram:
      if (instance != this.diagram) this.highlightAndFocus(instance.parentSystem.instance);
    } else {
      // navigate in current system
      // do not navigate outside diagram:
      if (instance == this.diagram) return;
      let system = instance.parentSystem;
      if (keyboard.isKeyCode('ArrowDown')) {
        let layer = system.nextLayer(instance);
        if (layer) this.highlightAndFocus(layer.instances[0]);
      } else if (keyboard.isKeyCode('ArrowUp')) {
        let layer = system.previousLayer(instance);
        if (layer) this.highlightAndFocus(layer.instances[0]);
      } else if (keyboard.isKeyCode('ArrowRight')) {
        let sel = system.nextInstance(instance);
        if (sel) this.highlightAndFocus(sel);
      } else if (keyboard.isKeyCode('ArrowLeft')) {
        let sel = system.previousInstance(instance);
        if (sel) this.highlightAndFocus(sel);
      }
    }
  }

  dragIt(px, py) {
    if (this.drag.ctrl) {
      // dragged the canvas
      // nothing diagram specific
    }
  }
}
