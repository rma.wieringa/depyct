let hello_ast = {
  "<class>": "root",
  "elements": [
    {
      "<class>": "bool",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "bool"
        ]
      }
    },
    {
      "<class>": "void",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "void"
        ]
      }
    },
    {
      "<class>": "file-name",
      "name": "examples/hello.dzn"
    },
    {
      "<class>": "interface",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "ihello"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "hello",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          }
        ]
      }
    },
    {
      "<class>": "component",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "hello"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "name": "h",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "ihello"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    }
  ],
  "working-directory": "wip-simulate"
};

let system_hello_ast = {
  "<class>": "root",
  "elements": [
    {
      "<class>": "bool",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "bool"
        ]
      }
    },
    {
      "<class>": "void",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "void"
        ]
      }
    },
    {
      "<class>": "file-name",
      "name": "examples/system-hello.dzn"
    },
    {
      "<class>": "interface",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "ihello"
        ]
      },
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "hello",
            "signature": {
              "<class>": "signature",
              "type_name": {
                "<class>": "scope_name",
                "ids": [
                  "void"
                ]
              },
              "formals": {
                "<class>": "formals",
                "elements": []
              }
            },
            "direction": "in"
          }
        ]
      }
    },
    {
      "<class>": "component",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "hello"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "name": "hh",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "ihello"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      }
    },
    {
      "<class>": "system",
      "name": {
        "<class>": "scope_name",
        "ids": [
          "system_hello"
        ]
      },
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "name": "h",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "ihello"
              ]
            },
            "direction": "provides",
            "formals": {
              "<class>": "formals",
              "elements": []
            }
          }
        ]
      },
      "instances": {
        "<class>": "instances",
        "elements": [
          {
            "<class>": "instance",
            "name": "c",
            "type_name": {
              "<class>": "scope_name",
              "ids": [
                "hello"
              ]
            }
          }
        ]
      },
      "bindings": {
        "<class>": "bindings",
        "elements": [
          {
            "<class>": "binding",
            "left": {
              "<class>": "end-point",
              "port_name": "h"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "c",
              "port_name": "hh"
            }
          }
        ]
      }
    }
  ],
  "working-directory": "wip-simulate"
};

let Camera_ast = {"<class>": "root", "comment": {"<class>": "comment", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 1, "column": 1, "end-line": 39, "end-column": 1, "offset": 0, "length": 1418}, "string": "// Dezyne --- Dezyne command line tools\n//\n// Copyright © 2015, 2022 Rutger van Beusekom <rutger@dezyne.org>\n// Copyright © 2015 Rob Wieringa <rma.wieringa@gmail.com>\n// Copyright © 2016, 2019, 2020, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>\n// Copyright © 2016, 2018 Paul Hoogendijk <paul@dezyne.org>\n//\n// This file is part of Dezyne.\n//\n// Dezyne is free software: you can redistribute it and/or modify it\n// under the terms of the GNU Affero General Public License as\n// published by the Free Software Foundation, either version 3 of the\n// License, or (at your option) any later version.\n//\n// Dezyne is distributed in the hope that it will be useful, but\n// WITHOUT ANY WARRANTY; without even the implied warranty of\n// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\n// Affero General Public License for more details.\n//\n// You should have received a copy of the GNU Affero General Public\n// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.\n//\n// Commentary:\n//\n// Code:\n// camera hardware:\n//   lens: focus => ILens\n//   shutter => IShutter\n//   flash => IFlash\n//   sensor acquire image => ISensor\n//   memory: store/retrieve image => IMemory\n// camera software abstractions:\n//   optics: controls the optical path: lens, focus, shutter\n//   acquisition: controls the imaging path: sensor, memory\n//   driver: relays user control to optics and acquisition\n"}, "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 39, "column": 1, "end-line": 103, "end-column": 1, "offset": 1418, "length": 1347}, "elements": [{"<class>": "file-name", "name": "Optics.dzn"}, {"<class>": "interface", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 25, "column": 1, "end-line": 45, "end-column": 2, "offset": 899, "length": 326}, "name": {"<class>": "scope_name", "ids": ["ILens"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 27, "column": 3, "end-line": 27, "end-column": 21, "offset": 919, "length": 18}, "name": "forward", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 27, "column": 6, "end-line": 27, "end-column": 10, "offset": 922, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 28, "column": 3, "end-line": 28, "end-column": 22, "offset": 940, "length": 19}, "name": "backward", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 28, "column": 6, "end-line": 28, "end-column": 10, "offset": 943, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 29, "column": 3, "end-line": 29, "end-column": 18, "offset": 962, "length": 15}, "name": "stop", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 29, "column": 6, "end-line": 29, "end-column": 10, "offset": 965, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 31, "column": 3, "end-line": 31, "end-column": 22, "offset": 981, "length": 19}, "name": "stopped", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 31, "column": 7, "end-line": 31, "end-column": 11, "offset": 985, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}]}}, {"<class>": "foreign", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 47, "column": 1, "end-line": 50, "end-column": 2, "offset": 1227, "length": 41}, "name": {"<class>": "scope_name", "ids": ["Lens"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 49, "column": 3, "end-line": 49, "end-column": 23, "offset": 1246, "length": 20}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["ILens"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "interface", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 52, "column": 1, "end-line": 65, "end-column": 2, "offset": 1270, "length": 239}, "name": {"<class>": "scope_name", "ids": ["IFocus"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 54, "column": 3, "end-line": 54, "end-column": 21, "offset": 1291, "length": 18}, "name": "measure", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 54, "column": 6, "end-line": 54, "end-column": 10, "offset": 1294, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 55, "column": 3, "end-line": 55, "end-column": 20, "offset": 1312, "length": 17}, "name": "cancel", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 55, "column": 6, "end-line": 55, "end-column": 10, "offset": 1315, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 56, "column": 3, "end-line": 56, "end-column": 22, "offset": 1332, "length": 19}, "name": "maximum", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 56, "column": 7, "end-line": 56, "end-column": 11, "offset": 1336, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}]}}, {"<class>": "interface", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 66, "column": 1, "end-line": 76, "end-column": 2, "offset": 1510, "length": 192}, "name": {"<class>": "scope_name", "ids": ["IContrast"]}, "types": {"<class>": "types", "elements": [{"<class>": "enum", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 68, "column": 3, "end-line": 68, "end-column": 38, "offset": 1534, "length": 35}, "name": {"<class>": "scope_name", "ids": ["EContrast"]}, "fields": {"<class>": "fields", "elements": ["Sharper", "Blurrier"]}}]}, "events": {"<class>": "events", "elements": [{"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 69, "column": 3, "end-line": 69, "end-column": 26, "offset": 1572, "length": 23}, "name": "measure", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 69, "column": 6, "end-line": 69, "end-column": 15, "offset": 1575, "length": 9}, "type_name": {"<class>": "scope_name", "ids": ["EContrast"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}]}}, {"<class>": "foreign", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 77, "column": 1, "end-line": 80, "end-column": 2, "offset": 1703, "length": 49}, "name": {"<class>": "scope_name", "ids": ["Contrast"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 79, "column": 3, "end-line": 79, "end-column": 27, "offset": 1726, "length": 24}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["IContrast"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "system", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 82, "column": 1, "end-line": 94, "end-column": 2, "offset": 1754, "length": 208}, "name": {"<class>": "scope_name", "ids": ["Focus"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 84, "column": 3, "end-line": 84, "end-column": 24, "offset": 1774, "length": 21}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["IFocus"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}, "instances": {"<class>": "instances", "elements": [{"<class>": "instance", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 86, "column": 5, "end-line": 86, "end-column": 24, "offset": 1811, "length": 19}, "name": "focus", "type_name": {"<class>": "scope_name", "ids": ["FocusControl"]}}, {"<class>": "instance", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 87, "column": 5, "end-line": 87, "end-column": 21, "offset": 1835, "length": 16}, "name": "sensor", "type_name": {"<class>": "scope_name", "ids": ["Contrast"]}}, {"<class>": "instance", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 88, "column": 5, "end-line": 88, "end-column": 15, "offset": 1856, "length": 10}, "name": "lens", "type_name": {"<class>": "scope_name", "ids": ["Lens"]}}]}, "bindings": {"<class>": "bindings", "elements": [{"<class>": "binding", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 90, "column": 5, "end-line": 90, "end-column": 25, "offset": 1872, "length": 20}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 90, "column": 5, "end-line": 90, "end-column": 9, "offset": 1872, "length": 4}, "port_name": "port"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 90, "column": 14, "end-line": 90, "end-column": 24, "offset": 1881, "length": 10}, "instance_name": "focus", "port_name": "port"}}, {"<class>": "binding", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 91, "column": 5, "end-line": 91, "end-column": 30, "offset": 1897, "length": 25}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 91, "column": 5, "end-line": 91, "end-column": 15, "offset": 1897, "length": 10}, "instance_name": "focus", "port_name": "lens"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 91, "column": 20, "end-line": 91, "end-column": 29, "offset": 1912, "length": 9}, "instance_name": "lens", "port_name": "port"}}, {"<class>": "binding", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 92, "column": 5, "end-line": 92, "end-column": 34, "offset": 1927, "length": 29}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 92, "column": 5, "end-line": 92, "end-column": 17, "offset": 1927, "length": 12}, "instance_name": "focus", "port_name": "sensor"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 92, "column": 22, "end-line": 92, "end-column": 33, "offset": 1944, "length": 11}, "instance_name": "sensor", "port_name": "port"}}]}}, {"<class>": "component", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 96, "column": 1, "end-line": 117, "end-column": 2, "offset": 1964, "length": 604}, "name": {"<class>": "scope_name", "ids": ["FocusControl"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 98, "column": 3, "end-line": 98, "end-column": 24, "offset": 1991, "length": 21}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["IFocus"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}, {"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 100, "column": 3, "end-line": 100, "end-column": 29, "offset": 2016, "length": 26}, "name": "sensor", "type_name": {"<class>": "scope_name", "ids": ["IContrast"]}, "direction": "requires", "formals": {"<class>": "formals", "elements": []}}, {"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 101, "column": 3, "end-line": 101, "end-column": 23, "offset": 2045, "length": 20}, "name": "lens", "type_name": {"<class>": "scope_name", "ids": ["ILens"]}, "direction": "requires", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "interface", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 119, "column": 1, "end-line": 126, "end-column": 2, "offset": 2570, "length": 79}, "name": {"<class>": "scope_name", "ids": ["IShutter"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 121, "column": 3, "end-line": 121, "end-column": 20, "offset": 2593, "length": 17}, "name": "expose", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 121, "column": 6, "end-line": 121, "end-column": 10, "offset": 2596, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}]}}, {"<class>": "foreign", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 127, "column": 1, "end-line": 130, "end-column": 2, "offset": 2650, "length": 47}, "name": {"<class>": "scope_name", "ids": ["Shutter"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 129, "column": 3, "end-line": 129, "end-column": 26, "offset": 2672, "length": 23}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["IShutter"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "interface", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 132, "column": 1, "end-line": 149, "end-column": 2, "offset": 2699, "length": 398}, "name": {"<class>": "scope_name", "ids": ["IOptics"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 134, "column": 3, "end-line": 134, "end-column": 21, "offset": 2721, "length": 18}, "name": "prepare", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 134, "column": 6, "end-line": 134, "end-column": 10, "offset": 2724, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 135, "column": 3, "end-line": 135, "end-column": 21, "offset": 2742, "length": 18}, "name": "capture", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 135, "column": 6, "end-line": 135, "end-column": 10, "offset": 2745, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 136, "column": 3, "end-line": 136, "end-column": 20, "offset": 2763, "length": 17}, "name": "cancel", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 136, "column": 6, "end-line": 136, "end-column": 10, "offset": 2766, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 137, "column": 3, "end-line": 137, "end-column": 20, "offset": 2783, "length": 17}, "name": "ready", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 137, "column": 7, "end-line": 137, "end-column": 11, "offset": 2787, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}]}}, {"<class>": "component", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 150, "column": 1, "end-line": 164, "end-column": 2, "offset": 3098, "length": 302}, "name": {"<class>": "scope_name", "ids": ["OpticsControl"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 152, "column": 3, "end-line": 152, "end-column": 25, "offset": 3126, "length": 22}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["IOptics"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}, {"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 154, "column": 3, "end-line": 154, "end-column": 29, "offset": 3152, "length": 26}, "name": "shutter", "type_name": {"<class>": "scope_name", "ids": ["IShutter"]}, "direction": "requires", "formals": {"<class>": "formals", "elements": []}}, {"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 155, "column": 3, "end-line": 155, "end-column": 25, "offset": 3181, "length": 22}, "name": "focus", "type_name": {"<class>": "scope_name", "ids": ["IFocus"]}, "direction": "requires", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "system", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 165, "column": 1, "end-line": 182, "end-column": 2, "offset": 3401, "length": 227}, "name": {"<class>": "scope_name", "ids": ["Optics"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 167, "column": 3, "end-line": 167, "end-column": 25, "offset": 3422, "length": 22}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["IOptics"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}, "instances": {"<class>": "instances", "elements": [{"<class>": "instance", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 171, "column": 5, "end-line": 171, "end-column": 26, "offset": 3463, "length": 21}, "name": "optics", "type_name": {"<class>": "scope_name", "ids": ["OpticsControl"]}}, {"<class>": "instance", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 173, "column": 5, "end-line": 173, "end-column": 21, "offset": 3490, "length": 16}, "name": "shutter", "type_name": {"<class>": "scope_name", "ids": ["Shutter"]}}, {"<class>": "instance", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 174, "column": 5, "end-line": 174, "end-column": 17, "offset": 3511, "length": 12}, "name": "focus", "type_name": {"<class>": "scope_name", "ids": ["Focus"]}}]}, "bindings": {"<class>": "bindings", "elements": [{"<class>": "binding", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 176, "column": 5, "end-line": 176, "end-column": 26, "offset": 3529, "length": 21}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 176, "column": 5, "end-line": 176, "end-column": 9, "offset": 3529, "length": 4}, "port_name": "port"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 176, "column": 14, "end-line": 176, "end-column": 25, "offset": 3538, "length": 11}, "instance_name": "optics", "port_name": "port"}}, {"<class>": "binding", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 178, "column": 5, "end-line": 178, "end-column": 37, "offset": 3556, "length": 32}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 178, "column": 5, "end-line": 178, "end-column": 19, "offset": 3556, "length": 14}, "instance_name": "optics", "port_name": "shutter"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 178, "column": 24, "end-line": 178, "end-column": 36, "offset": 3575, "length": 12}, "instance_name": "shutter", "port_name": "port"}}, {"<class>": "binding", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 180, "column": 5, "end-line": 180, "end-column": 33, "offset": 3594, "length": 28}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 180, "column": 5, "end-line": 180, "end-column": 17, "offset": 3594, "length": 12}, "instance_name": "optics", "port_name": "focus"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Optics.dzn", "line": 180, "column": 22, "end-line": 180, "end-column": 32, "offset": 3611, "length": 10}, "instance_name": "focus", "port_name": "port"}}]}}, {"<class>": "file-name", "name": "Acquisition.dzn"}, {"<class>": "interface", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 25, "column": 1, "end-line": 43, "end-column": 2, "offset": 905, "length": 396}, "name": {"<class>": "scope_name", "ids": ["ISensor"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 27, "column": 3, "end-line": 27, "end-column": 21, "offset": 927, "length": 18}, "name": "prepare", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 27, "column": 6, "end-line": 27, "end-column": 10, "offset": 930, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 28, "column": 3, "end-line": 28, "end-column": 21, "offset": 948, "length": 18}, "name": "acquire", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 28, "column": 6, "end-line": 28, "end-column": 10, "offset": 951, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 29, "column": 3, "end-line": 29, "end-column": 20, "offset": 969, "length": 17}, "name": "cancel", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 29, "column": 6, "end-line": 29, "end-column": 10, "offset": 972, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 30, "column": 3, "end-line": 30, "end-column": 20, "offset": 989, "length": 17}, "name": "image", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 30, "column": 7, "end-line": 30, "end-column": 11, "offset": 993, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}]}}, {"<class>": "foreign", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 45, "column": 1, "end-line": 48, "end-column": 2, "offset": 1303, "length": 43}, "name": {"<class>": "scope_name", "ids": ["CMOS"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 47, "column": 3, "end-line": 47, "end-column": 25, "offset": 1322, "length": 22}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["ISensor"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "interface", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 50, "column": 1, "end-line": 57, "end-column": 2, "offset": 1348, "length": 75}, "name": {"<class>": "scope_name", "ids": ["IMemory"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 52, "column": 3, "end-line": 52, "end-column": 19, "offset": 1370, "length": 16}, "name": "store", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 52, "column": 6, "end-line": 52, "end-column": 10, "offset": 1373, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}]}}, {"<class>": "foreign", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 59, "column": 1, "end-line": 62, "end-column": 2, "offset": 1425, "length": 45}, "name": {"<class>": "scope_name", "ids": ["Memory"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 61, "column": 3, "end-line": 61, "end-column": 25, "offset": 1446, "length": 22}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["IMemory"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "interface", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 64, "column": 1, "end-line": 82, "end-column": 2, "offset": 1472, "length": 409}, "name": {"<class>": "scope_name", "ids": ["IAcquisition"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 66, "column": 3, "end-line": 66, "end-column": 21, "offset": 1499, "length": 18}, "name": "prepare", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 66, "column": 6, "end-line": 66, "end-column": 10, "offset": 1502, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 67, "column": 3, "end-line": 67, "end-column": 20, "offset": 1520, "length": 17}, "name": "cancel", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 67, "column": 6, "end-line": 67, "end-column": 10, "offset": 1523, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 68, "column": 3, "end-line": 68, "end-column": 21, "offset": 1540, "length": 18}, "name": "acquire", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 68, "column": 6, "end-line": 68, "end-column": 10, "offset": 1543, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 70, "column": 3, "end-line": 70, "end-column": 20, "offset": 1562, "length": 17}, "name": "image", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 70, "column": 7, "end-line": 70, "end-column": 11, "offset": 1566, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}]}}, {"<class>": "component", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 84, "column": 1, "end-line": 98, "end-column": 2, "offset": 1883, "length": 302}, "name": {"<class>": "scope_name", "ids": ["Acquire"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 86, "column": 3, "end-line": 86, "end-column": 30, "offset": 1905, "length": 27}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["IAcquisition"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}, {"<class>": "port", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 87, "column": 3, "end-line": 87, "end-column": 27, "offset": 1935, "length": 24}, "name": "memory", "type_name": {"<class>": "scope_name", "ids": ["IMemory"]}, "direction": "requires", "formals": {"<class>": "formals", "elements": []}}, {"<class>": "port", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 88, "column": 3, "end-line": 88, "end-column": 27, "offset": 1962, "length": 24}, "name": "sensor", "type_name": {"<class>": "scope_name", "ids": ["ISensor"]}, "direction": "requires", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "system", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 100, "column": 1, "end-line": 115, "end-column": 2, "offset": 2187, "length": 231}, "name": {"<class>": "scope_name", "ids": ["Acquisition"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 102, "column": 3, "end-line": 102, "end-column": 30, "offset": 2213, "length": 27}, "name": "port", "type_name": {"<class>": "scope_name", "ids": ["IAcquisition"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}, "instances": {"<class>": "instances", "elements": [{"<class>": "instance", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 106, "column": 5, "end-line": 106, "end-column": 21, "offset": 2259, "length": 16}, "name": "acquire", "type_name": {"<class>": "scope_name", "ids": ["Acquire"]}}, {"<class>": "instance", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 107, "column": 5, "end-line": 107, "end-column": 19, "offset": 2280, "length": 14}, "name": "memory", "type_name": {"<class>": "scope_name", "ids": ["Memory"]}}, {"<class>": "instance", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 108, "column": 5, "end-line": 108, "end-column": 17, "offset": 2299, "length": 12}, "name": "sensor", "type_name": {"<class>": "scope_name", "ids": ["CMOS"]}}]}, "bindings": {"<class>": "bindings", "elements": [{"<class>": "binding", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 110, "column": 5, "end-line": 110, "end-column": 27, "offset": 2317, "length": 22}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 110, "column": 5, "end-line": 110, "end-column": 9, "offset": 2317, "length": 4}, "port_name": "port"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 110, "column": 14, "end-line": 110, "end-column": 26, "offset": 2326, "length": 12}, "instance_name": "acquire", "port_name": "port"}}, {"<class>": "binding", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 112, "column": 5, "end-line": 112, "end-column": 36, "offset": 2345, "length": 31}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 112, "column": 5, "end-line": 112, "end-column": 19, "offset": 2345, "length": 14}, "instance_name": "acquire", "port_name": "memory"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 112, "column": 24, "end-line": 112, "end-column": 35, "offset": 2364, "length": 11}, "instance_name": "memory", "port_name": "port"}}, {"<class>": "binding", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 113, "column": 5, "end-line": 113, "end-column": 36, "offset": 2381, "length": 31}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 113, "column": 5, "end-line": 113, "end-column": 19, "offset": 2381, "length": 14}, "instance_name": "acquire", "port_name": "sensor"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Acquisition.dzn", "line": 113, "column": 24, "end-line": 113, "end-column": 35, "offset": 2400, "length": 11}, "instance_name": "sensor", "port_name": "port"}}]}}, {"<class>": "file-name", "name": "Camera.dzn"}, {"<class>": "import", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 39, "column": 1, "end-line": 39, "end-column": 19, "offset": 1418, "length": 18}, "name": "Optics.dzn"}, {"<class>": "import", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 40, "column": 1, "end-line": 40, "end-column": 24, "offset": 1437, "length": 23}, "name": "Acquisition.dzn"}, {"<class>": "interface", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 42, "column": 1, "end-line": 70, "end-column": 2, "offset": 1462, "length": 627}, "name": {"<class>": "scope_name", "ids": ["IControl"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 44, "column": 3, "end-line": 44, "end-column": 19, "offset": 1485, "length": 16}, "name": "setup", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 44, "column": 6, "end-line": 44, "end-column": 10, "offset": 1488, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 45, "column": 3, "end-line": 45, "end-column": 19, "offset": 1504, "length": 16}, "name": "shoot", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 45, "column": 6, "end-line": 45, "end-column": 10, "offset": 1507, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 46, "column": 3, "end-line": 46, "end-column": 21, "offset": 1523, "length": 18}, "name": "release", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 46, "column": 6, "end-line": 46, "end-column": 10, "offset": 1526, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 48, "column": 3, "end-line": 48, "end-column": 25, "offset": 1545, "length": 22}, "name": "focus_lock", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 48, "column": 7, "end-line": 48, "end-column": 11, "offset": 1549, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}, {"<class>": "event", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 49, "column": 3, "end-line": 49, "end-column": 20, "offset": 1570, "length": 17}, "name": "image", "signature": {"<class>": "signature", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 49, "column": 7, "end-line": 49, "end-column": 11, "offset": 1574, "length": 4}, "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}]}}, {"<class>": "component", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 72, "column": 1, "end-line": 86, "end-column": 2, "offset": 2091, "length": 424}, "name": {"<class>": "scope_name", "ids": ["Driver"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 74, "column": 3, "end-line": 74, "end-column": 29, "offset": 2112, "length": 26}, "name": "control", "type_name": {"<class>": "scope_name", "ids": ["IControl"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}, {"<class>": "port", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 75, "column": 3, "end-line": 75, "end-column": 37, "offset": 2141, "length": 34}, "name": "acquisition", "type_name": {"<class>": "scope_name", "ids": ["IAcquisition"]}, "direction": "requires", "formals": {"<class>": "formals", "elements": []}}, {"<class>": "port", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 76, "column": 3, "end-line": 76, "end-column": 27, "offset": 2178, "length": 24}, "name": "optics", "type_name": {"<class>": "scope_name", "ids": ["IOptics"]}, "direction": "requires", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "system", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 88, "column": 1, "end-line": 102, "end-column": 2, "offset": 2517, "length": 247}, "name": {"<class>": "scope_name", "ids": ["Camera"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 90, "column": 3, "end-line": 90, "end-column": 29, "offset": 2538, "length": 26}, "name": "control", "type_name": {"<class>": "scope_name", "ids": ["IControl"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}, "instances": {"<class>": "instances", "elements": [{"<class>": "instance", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 94, "column": 5, "end-line": 94, "end-column": 19, "offset": 2583, "length": 14}, "name": "driver", "type_name": {"<class>": "scope_name", "ids": ["Driver"]}}, {"<class>": "instance", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 95, "column": 5, "end-line": 95, "end-column": 29, "offset": 2602, "length": 24}, "name": "acquisition", "type_name": {"<class>": "scope_name", "ids": ["Acquisition"]}}, {"<class>": "instance", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 96, "column": 5, "end-line": 96, "end-column": 19, "offset": 2631, "length": 14}, "name": "optics", "type_name": {"<class>": "scope_name", "ids": ["Optics"]}}]}, "bindings": {"<class>": "bindings", "elements": [{"<class>": "binding", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 98, "column": 5, "end-line": 98, "end-column": 32, "offset": 2651, "length": 27}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 98, "column": 5, "end-line": 98, "end-column": 12, "offset": 2651, "length": 7}, "port_name": "control"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 98, "column": 17, "end-line": 98, "end-column": 31, "offset": 2663, "length": 14}, "instance_name": "driver", "port_name": "control"}}, {"<class>": "binding", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 99, "column": 5, "end-line": 99, "end-column": 45, "offset": 2683, "length": 40}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 99, "column": 5, "end-line": 99, "end-column": 23, "offset": 2683, "length": 18}, "instance_name": "driver", "port_name": "acquisition"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 99, "column": 28, "end-line": 99, "end-column": 44, "offset": 2706, "length": 16}, "instance_name": "acquisition", "port_name": "port"}}, {"<class>": "binding", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 100, "column": 5, "end-line": 100, "end-column": 35, "offset": 2728, "length": 30}, "left": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 100, "column": 5, "end-line": 100, "end-column": 18, "offset": 2728, "length": 13}, "instance_name": "driver", "port_name": "optics"}, "right": {"<class>": "end-point", "location": {"<class>": "location", "file-name": "Camera.dzn", "line": 100, "column": 23, "end-line": 100, "end-column": 34, "offset": 2746, "length": 11}, "instance_name": "optics", "port_name": "port"}}]}}], "working-directory": "dezyne-2.18.1"};

let erroneous_ast = {"<class>": "root", "elements": [{"<class>": "bool", "name": {"<class>": "scope_name", "ids": ["bool"]}}, {"<class>": "void", "name": {"<class>": "scope_name", "ids": ["void"]}}, {"<class>": "file-name", "name": "foo.dzn"}, {"<class>": "interface", "name": {"<class>": "scope_name", "ids": ["ihello"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "name": "hello", "signature": {"<class>": "signature", "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "name": "world", "signature": {"<class>": "signature", "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}]}}, {"<class>": "interface", "name": {"<class>": "scope_name", "ids": ["itimer"]}, "types": {"<class>": "types", "elements": []}, "events": {"<class>": "events", "elements": [{"<class>": "event", "name": "hello", "signature": {"<class>": "signature", "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "in"}, {"<class>": "event", "name": "world", "signature": {"<class>": "signature", "type_name": {"<class>": "scope_name", "ids": ["void"]}, "formals": {"<class>": "formals", "elements": []}}, "direction": "out"}]}}, {"<class>": "component", "name": {"<class>": "scope_name", "ids": ["hello"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "name": "hh", "type_name": {"<class>": "scope_name", "ids": ["ihello"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}, {"<class>": "port", "name": "tt", "type_name": {"<class>": "scope_name", "ids": ["itimer"]}, "direction": "requires", "formals": {"<class>": "formals", "elements": []}}]}}, {"<class>": "system", "name": {"<class>": "scope_name", "ids": ["hello_system"]}, "ports": {"<class>": "ports", "elements": [{"<class>": "port", "name": "h", "type_name": {"<class>": "scope_name", "ids": ["ihello"]}, "direction": "provides", "formals": {"<class>": "formals", "elements": []}}]}, "instances": {"<class>": "instances", "elements": [{"<class>": "instance", "name": "c", "type_name": {"<class>": "scope_name", "ids": ["hello"]}}, {"<class>": "instance", "name": "t", "type_name": {"<class>": "scope_name", "ids": ["timer"]}}]}, "bindings": {"<class>": "bindings", "elements": [{"<class>": "binding", "left": {"<class>": "end-point", "port_name": "h"}, "right": {"<class>": "end-point", "instance_name": "c", "port_name": "hh"}}, {"<class>": "binding", "left": {"<class>": "end-point", "instance_name": "c", "port_name": "tt"}, "right": {"<class>": "end-point", "instance_name": "t", "port_name": "t"}}]}}], "working-directory": "wip"};

//let AST = hello_ast;
//let AST = system_hello_ast;
//let AST = erroneous_ast;
let AST = Camera_ast;
