/*
 * Copyright (C) 2020,2021,2024 Rob Wieringa <rma.wieringa@gmail.com>
 * Copyright (C) 2022 Paul Hoogendijk <paul@dezyne.org>
 * Copyright (C) 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */



let Ast2Data = {

  is_a: function(o, klass) {
    return o && o['<class>'] == klass;
  },

  name2string: function(name) { // is_a 'scope_name'
    // TODO: fix namespaces
    return name.ids[name.ids.length-1];
  },

  models: function(rns) { // root or namespace
    let result = [];
    rns.elements
      .filter(o => Ast2Data.is_a(o, 'namespace'))
      .forEach(o => result = result.concat(Ast2Data.models(o)));
    rns.elements
      .filter(o => Ast2Data.is_a(o, 'interface') ||
              Ast2Data.is_a(o, 'component') ||
              Ast2Data.is_a(o, 'system') ||
              Ast2Data.is_a(o,'foreign'))
      .forEach(o => result.push(o));
    return result;
  },

  sut: function(root) {
    let components = Ast2Data.models(root);

    function findByName(name) {
      // TODO: fix namespaces
      return components.find(c => Ast2Data.name2string(c.name) == Ast2Data.name2string(name));
    }

    function size(comp) {
      if (!comp) return 0;
      if (Ast2Data.is_a(comp, 'interface') ) return 0;
      if (Ast2Data.is_a(comp, 'component') || Ast2Data.is_a(comp, 'foreign')) return 1;
      let sz = 1;
      comp.instances.elements.forEach(i => {
        let sub = findByName(i.type_name);
        sz += size(sub);
      });
      return sz;
    }

    let sut = null;
    let sutSize = 0;
    components.forEach(o => {
      let sz = size(o);
      if (sz > sutSize) {
        sut = o;
        sutSize = sz;
      }
    });
    return Ast2Data.name2string(sut.name);
  },

  model2data: function(o) {
    if (Ast2Data.is_a(o, 'interface')) {
      return Ast2Data.interface2data(o);
    } else if (Ast2Data.is_a(o, 'component')) {
      return Ast2Data.component2data(o);
    } else if (Ast2Data.is_a(o, 'system')) {
      return Ast2Data.system2data(o);
    } else if (Ast2Data.is_a(o, 'foreign')) {
      return Ast2Data.foreign2data(o);
    }
  },

  ports2data: function(ports, direction) {
    return ports.elements.filter(p => p.direction == direction && !p['injected?']).map(Ast2Data.port2data);
  },

  interface2data: function(intf) {
    return { kind: 'interface',
             name: Ast2Data.name2string(intf.name),
             location: intf.location
           };
  },

  component2data: function(comp) {
    return { kind: 'component',
             name: Ast2Data.name2string(comp.name),
             location: comp.location,
             provides: Ast2Data.ports2data(comp.ports, 'provides'),
             requires: Ast2Data.ports2data(comp.ports, 'requires'),
           };
  },

  bindInjected: function(binding) {
    return binding.left.port_name == '*' || binding.right.port_name == '*';
  },

  system2data: function(sys) {
    return { kind: 'system',
             name: Ast2Data.name2string(sys.name),
             location: sys.location,
             provides: Ast2Data.ports2data(sys.ports, 'provides'),
             requires: Ast2Data.ports2data(sys.ports, 'requires'),
             instances: sys.instances.elements.map(Ast2Data.instance2data),
             bindings: sys.bindings.elements.filter(bind => !Ast2Data.bindInjected(bind)).map(Ast2Data.binding2data),
           };
  },

  foreign2data: function(comp) {
    return { kind: 'foreign',
             name: Ast2Data.name2string(comp.name),
             location: comp.location,
             provides: Ast2Data.ports2data(comp.ports, 'provides'),
             requires: Ast2Data.ports2data(comp.ports, 'requires'),
           };
  },

  blocking: function(port) {
    return port['blocking?'] == 'blocking';
  },

  port2data: function(port) {
    return {name: port.name, location: port.location,
            interface: Ast2Data.name2string(port.type_name), blocking: Ast2Data.blocking(port)};
  },

  instance2data: function(inst) {
    return {name: inst.name,
            location: inst.location,
            model: Ast2Data.name2string(inst.type_name)};
  },

  binding2data: function(binding) {
    let left = Ast2Data.endpoint2data(binding.left);
    let right = Ast2Data.endpoint2data(binding.right);
    // mixup of from and to will be corrected in systemDiagram.js
    return {from: left, to: right, location: binding.location};
  },

  endpoint2data: function(ep) {
    return {inst: ep.instance_name, port: ep.port_name, location: ep.location};
  },

  ast2data: function(root) {
    return {sut: Ast2Data.sut(root),
            models: Ast2Data.models(root).map(o => Ast2Data.model2data(o)),
            'working-directory': root['working-directory']
           };
  },
};
