let AST = {
  "<class>": "root",
  "elements": [
    {
      "<class>": "interface",
      "name": {"<class>": "scope_name","ids": ["I"]},
      "types": {
        "<class>": "types",
        "elements": []
      },
      "events": {
        "<class>": "events",
        "elements": [
          {
            "<class>": "event",
            "name": "e",
            "signature": {
              "<class>": "signature",
              "type_name": {"<class>": "scope_name","ids": ["void"]},
              "formals": {"<class>": "formals","elements": []}
            },
            "direction": "in"
          }
        ]
      }
    },
    {
      "<class>": "component",
      "name": {"<class>": "scope_name","ids": ["Dezyne"]},
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "name": "r",
            "type_name": {"<class>": "scope_name","ids": ["I"]},
            "direction": "requires",
            "formals": {"<class>": "formals","elements": []}
          }
        ]
      }
    },
    {
      "<class>": "foreign",
      "name": {"<class>": "scope_name","ids": ["SVG"]},
      "ports": {
        "<class>": "ports",
        "elements": [
          {
            "<class>": "port",
            "name": "p",
            "type_name": {"<class>": "scope_name","ids": ["I"]},
            "direction": "provides",
            "formals": {"<class>": "formals","elements": []}
          }
        ]
      }
    },
    {
      "<class>": "system",
      "name": {"<class>": "scope_name","ids": ["Depyct"]},
      "ports": {
        "<class>": "ports",
        "elements": []
      },
      "instances": {
        "<class>": "instances",
        "elements": [
          {
            "<class>": "instance",
            "name": "dzn",
            "type_name": {"<class>": "scope_name","ids": ["Dezyne"]}
          },
          {
            "<class>": "instance",
            "name": "svg",
            "type_name": {"<class>": "scope_name","ids": ["SVG"]}
          }
        ]
      },
      "bindings": {
        "<class>": "bindings",
        "elements": [
          {
            "<class>": "binding",
            "left": {
              "<class>": "end-point",
              "instance_name": "dzn",
              "port_name": "r"
            },
            "right": {
              "<class>": "end-point",
              "instance_name": "svg",
              "port_name": "p"
            }
          }
        ]
      }
    }
  ],
  "working-directory": "dzn"
}
