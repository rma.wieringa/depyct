/*
 * Copyright (C) 2020, 2021, 2024 Rob Wieringa <rma.wieringa@gmail.com>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

/* Example */
let Data = Ast2Data.ast2data(AST);
let S = new SystemDiagramSvg();
S.in.draw(Data);
