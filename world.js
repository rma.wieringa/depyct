/*
 * Copyright (C) 2020, 2021, 2022, 2024 Rob Wieringa <rma.wieringa@gmail.com>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

class World extends BuildingBlock {
  constructor(width, height) {
    super();
    // 'canvas' dimensions:
    this.width = width;
    this.height = height;
    this.x0 = this.y0 = 0; // (0, 0) in canvas dimensions:
    this.scale = 1;
    // content:
    this._content = null;
    this.zoomInFactor = 1.05;
    this.zoomOutFactor = 0.95;
  }

  set content(content) {
    if (this._content != content) {
      content.parent = this;
      this._content = content;
      this.isModified();
      this.update();
    }
  }

  get content() {
    return this._content;
  }

  set(bounds) {
    let left = this.worldToCanvasX(bounds.x);
    let top = this.worldToCanvasY(bounds.y);
    let right = this.worldToCanvasX(bounds.x+bounds.width);
    let bottom = this.worldToCanvasY(bounds.y+bounds.height);
    // when needed, center for small dimensions:
    if (right - left < this.width) {
      left = (this.width - (right - left))/2;
    } else {
      // do not allow scrolling too far in either direction
      left = Math.max(this.width - (right - left), Math.min(left, 0));
    }
    if (bottom - top < this.height) {
      top = (this.height - (bottom - top))/2;
    } else {
      // do not allow scrolling too far in either direction
      top = Math.max(this.height - (bottom-top), Math.min(top, 0));
    }

    let x0 = left - bounds.x*this.scale;
    let y0 = top - bounds.y*this.scale;
    if (this.x0 != x0 || this.y0 != y0) {
      this.x0 = left - bounds.x*this.scale;
      this.y0 = top - bounds.y*this.scale;
      this.isModified();
    }
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('world');
    this._svg.setAttribute('width', this.width);
    this._svg.setAttribute('height', this.height);
    this._svg.setAttribute('transform',
                          `translate(${this.x0} ${this.y0})  scale(${this.scale} ${this.scale})`);
    this._svg.append(this.content.svg);
    this.changed = false;
    return this._svg;
  }

  get state() {
    return {
      width: this.width,
      height: this.height,
      x0: this.x0,
      y0: this.y0,
      scale: this.scale
    };
  }

  set state(st) {
    this.resizeCanvas(st.width, st.height);
    this.x0 = st.x0;
    this.y0 = st.y0;
    this.scale = st.scale;
  }

  resizeCanvas(width, height) {
    this.width = width;
    this.height = height;
    this.isModified();
  }

  mouseWheel(e) {
    if (e.ctrlKey) {
      let zoom = (e.deltaY < 0) ? this.zoomInFactor : this.zoomOutFactor;
      this.zoomAround(mouse.x, mouse.y, zoom);
    } else if (e.shiftKey) {
      this.x0 -= e.deltaY;
      this.isModified();
    } else {
      this.y0 -= e.deltaY;
      this.isModified();
    }
  }

  zoomAround(mx, my, zoom) {
    this.scale *= zoom;
    this.x0 = this.x0 + (this.x0 - mx)*(zoom - 1);
    this.y0 = this.y0 + (this.y0 - my)*(zoom - 1);
    this.isModified();
  }

  drag(wfromx, wfromy, wtox, wtoy) {
    console.log('drag: %s, %s, %s, %s', wfromx, wfromy, wtox, wtoy);
    let cfrom = this.worldToCanvas(wfromx, wfromy);
    let cto = this.worldToCanvas(wtox, wtoy);
    this.x0 += cto.x - cfrom.x;
    this.y0 += cto.y - cfrom.y;
    this.isModified();
  }

  fit(bounds) { // bounds in World dimensions
    let sc = Math.min(this.width/bounds.width, this.height/bounds.height)*.9;
    this.scale = sc;
    this.isModified();
  }

  canvasToWorld(cx, cy) {
    return {x: (cx - this.x0)/this.scale, y: (cy - this.y0)/this.scale};
  }

  worldToCanvasX(wx) {
    return wx * this.scale + this.x0;
  }

  worldToCanvasY(wy) {
    return wy * this.scale + this.y0;
  }

  worldToCanvas(wx, wy) {
    return {x: this.worldToCanvasX(wx), y: this.worldToCanvasY(wy)};
  }

  mousePoint() {
    return this.canvasToWorld(mouse.x, mouse.y);
  }

  shiftWorld(dx, dy) {
    this.x0 += dx * this.scale;
    this.y0 += dy * this.scale;
    this.isModified();
  }
};
