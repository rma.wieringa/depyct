/*
 * Copyright (C) 2021, 2022, 2023, 2024 Rob Wieringa <rma.wieringa@gmail.com>
 * Copyright (C) 2022 Paul Hoogendijk <paul@dezyne.org>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

class StateDiagramSvg extends DiagramSvg {

  constructor(parent) {
    super(parent);

    /*
     * super defines interface to the outside world, through functions
     *   in.draw(data)
     *   in.dimensions(px, py, width, height)
     *   out.selected(location)
     *
     * extension:
     */
    this.in.reset = () => {
      this.data = null;
      if (this.set_up) {
        this.data = null;
        this.initDiagram();
        this.redrawNeeded = true;
      }
    };

    this.phase = {status: 'none', // {'none', 'simple', 'noself', 'full', 'stable', 'final'}
                  stability: 0};

    this.simpleDiagram = null;
    this.noselfDiagram = null;
    this.fullDiagram = null;

    this.dataLimit = 4000;
    this.nrNodes = 0;
    this.detail = null;

    this.layoutAborted = false;

    this.timeOut = 0;
  }

  /*
   * These abstract methods have to be defined in this class:
   *
   * initDiagram() { }
   * get svg() { }
   * selection(px, py) { }
   * handleKey(e) { }
   * handleMouseClick(e) { }
   * dragIt(px, py) { }
   * stopDraggingIt() { }
   * get animating() { return false; }
   * animationUpdate() { }
   */

  initDiagram() {
    this.resetEnvironment();
    if (this.data == null) {
      this.diagram = null;
      this.drawing.updateContent(this.diagram);
      this.setCursor('default');
    } else {
      this.nrNodes = this.data.states.length + this.data.transitions.length;
      if (this.nrNodes > this.dataLimit) {
        let msg =
            'more than '+ this.dataLimit + ' nodes\n\n' +
            'cowardly refusing to\n' +
            'draw state diagram';
        this.data = {states:[{id:'0', state:[{instance:'', state:[{name:'WARNING', value:msg}]}]},],
                     transitions:[], clusters: []};
      } else {
        this.data.clusters = StateDiagramBase.clusterTransitions(this.data.transitions);
      }
      this.diagram = this.ProgressMessage();
      this.progressMessage = `calculating layout ${this.nrNodes} nodes\n`;
      this.drawing = new Drawing(this.diagram);

      this.showSelf = true;

      // start timing...
      this.timeOut = Date.now();
      this.incTimeOut();
      this.simpleDiagram = new StateDiagramSimple(this.data);
      console.log('----------- PHASE I -----------');
      console.log('  nr nodes: ' + this.simpleDiagram.graph.nodes.length);
      this.phase.status = 'simple';
      this.phase.stability = this.stabilize(this.simpleDiagram, 1, this.timeOut);
    }
  }

  resetEnvironment() {
    //pre: this.set_up
    // this.world.scale = 1;
    // this.world.x0 = 0;
    // this.world.y0 = 0;
    this.diagram = null;
    this.simpleDiagram = null;
    this.noselfDiagram = null;
    this.fullDiagram = null;
    this.phase.status = 'none';
    this.drag.dragging = false;
    this.highlightedObj = null;
    this.detail = null;
    this.showSelf = true;
  }

  ProgressMessage() {
    let message = new RoundedBoundingBox(new Text(''));
    message.color = '#FFFFFF';
    message.padding = 20;
    message.center(this.width/2, this.height/2);
    message.update();
    return message;
  }

  incTimeOut() {
    let time = Date.now();
    while (this.timeOut < time) this.timeOut += 1000;
  }

  set progressMessage(msg) {
    this.diagram.content.text = msg;
    this.diagram.content.update();
    this.diagram.center(this.width/2, this.height/2);
    this.drawing.update();
  }

  stabilize(diagram, accuracy, timeOut) {
    if (diagram.maxEnergyPerNode == null) {
      // first call
      diagram.updateGraph();
      diagram.maxEnergyPerNode = diagram.energyPerNode;
    }
    let result = 0;
    let time = Date.now();
    let iter = 0;
    do {
      let logEN = Math.log(accuracy/diagram.energyPerNode);
      let minEN = Math.min(Math.log(accuracy/diagram.maxEnergyPerNode), logEN);
      result = Math.floor((1 - logEN/minEN) * 100);
      diagram.updateGraph();
      iter++;
      diagram.maxEnergyPerNode = Math.max(diagram.maxEnergyPerNode, diagram.energyPerNode);
      time = Date.now();
    } while (time < timeOut && diagram.energyPerNode > accuracy);
    if (diagram.energyPerNode <= accuracy) result = 100;
    console.log('at ' + Math.floor(time/1000) + ' sec: iterations: ' + iter + '; stability: ' + result);
    return result;
  }

  get svg() {
    this.scrollbars.update();
    this.world.set(this.drawing.bounds);
    this.svgDrawingPlus('StateDiagramSvg');

    if (this.highlightedObj) {
      let grayout = Svg.element('rect', 'grayout');
      grayout.setAttribute('stroke', 'none');
      grayout.setAttribute('fill', '#ffffffAA');
      Svg.setBounds(grayout, this.diagram.scaledBounds);
      let ssg = this.highlightedObj.svgSubGraph(2);
      this.svgWorld().append(grayout, ssg);
    }

    if (this.detail && this.world.scale < .75 && this.mouseInCanvas) {
      let detail = Svg.group('detail');
      Svg.setPos(detail, {x: 0, y: 0});
      let pad = 3;
      let back = Svg.element('rect', 'back');
      back.setAttribute('fill', this.background);
      back.setAttribute('rx', 10);
      back.setAttribute('ry', 10);
      Svg.setBounds(back, {x: 0, y: 0,
                           width: this.detail.bounds.width+2*pad, height: this.detail.bounds.height+2*pad,
                           scaleX: 1, scaleY: 1});
      detail.append(back);
      let s = this.detail.svg.cloneNode(true);
      Svg.setPos(s, {x: pad, y: pad});
      detail.append(s);
      this._svg.append(detail);
    }
    // var ser = new XMLSerializer();
    // var str = ser.serializeToString(this._svg);
    // console.log(str);

    return this._svg;
  }

  handleMouseClick(e) {
    let wpt = this.world.mousePoint();
    let node = this.selection(wpt.x, wpt.y);
    if (node) {
      this.highlight(node);
      if (node.data.location) {
        this.out.selected({...node.data.location, 'working-directory': this.data['working-directory']});
      }
    } else {
      this.resetHighlight();
    }
    this.redrawNeeded = true;
  }

  // override super:
  mouseMove(e) {
    super.mouseMove(e);
    if (!this.mouseInCanvas) return;
    let node = this.nodeAtMouse();
    if (node != this.detail) {
      if (this.detail) {
        if (this.detail instanceof Transition) this.detail.hover(false);
        this.detail = null;
      }
      if (node) {
        this.detail = node;
        if (this.detail instanceof Transition) this.detail.hover(true);
      }
      this.redrawNeeded = true;
    }
  }

  nodeAtMouse() {
    if (!this.mouseInCanvas) return null;
    if (!this.diagram) return null;
    let wpt = this.world.mousePoint();
    return this.selection(wpt.x, wpt.y);
  }

  selection(px, py) {
    let sel = this.diagram.objectsAt(px, py);
    return this.selectionOfKlass(sel, State)
      || this.selectionOfKlass(sel, Transition);
  }
  
  handleKey(e) {
    if (keyboard.isKeyCode('F1') || keyboard.isKey('?')) {
      window.open('./helpState.html');
    } else if (keyboard.isKeyCode('Escape')) {
      this.layoutAborted = !this.layoutAborted;
      if (!this.layoutAborted) {
        this.diagram.updateContent();
        this.redrawNeeded = true;
      }
    } else if (keyboard.isKey('.')) {
      if (this.diagram instanceof StateDiagramFull) {
        let ini = this.diagram.states.find(state => state instanceof InitialState);
        if (ini) {
          this.highlight(ini);
          this.redrawNeeded = true;
        }
      }
    } else if (keyboard.isKey('-', 'Control')) {
      this.world.zoomAround(mouse.x, mouse.y, this.world.zoomOutFactor);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('+', 'Control') || keyboard.isKey('=', 'Control')) {
      this.world.zoomAround(mouse.x, mouse.y, this.world.zoomInFactor);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('0', 'Control')) {
      this.world.fit(this.drawing.scaledBounds);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('1', 'Control')) {
      this.world.zoomAround(mouse.x, mouse.y, 1/this.world.scale);
      this.redrawNeeded = true;
    } else if (keyboard.isKey('s', 'Control')) {
      this.saveAsSvg('state.svg');
    } else if (keyboard.isKey('s')) {
      this.toggleSelf();
    } else {
      return super.handleKey(e);
    }
    // suppress default behaviour:
    e.preventDefault();
    return false;
  }


  dragIt(px, py) {
    if (this.drag.ctrl) {
      // dragged the canvas
      // nothing diagram specific
    } else if (this.drag.obj) {
      if (this.drag.obj instanceof State) {
        this.drag.obj.dragselfs(px, py);
      } else {
        this.drag.obj.drag(px, py);
      }
      this.diagram.update();
    }
  }

  stopDraggingIt() {
    if (this.drag.ctrl) {
    } else if (this.drag.obj) {
      if (this.drag.obj instanceof State) {
        this.drag.obj.stopdragselfs();
      } else {
        this.drag.obj.stopdrag();
      }
    }
    }

  isDraggingObject() {
    return this.drag.dragging && this.drag.obj && !(this.drag.obj instanceof Scrollbar) && !this.drag.ctrl;
  }

  get animating() {
    if (this.diagram == null ||
        this.phase.status == 'none') {
      return false;
    } else if (this.phase.status == 'simple' ||
               this.phase.status == 'noself' ||
               this.phase.status == 'full' ||
               this.phase.status == 'stable') {
      return true;
    } else if (this.phase.status == 'final') {
      if (this.isDraggingObject()) {
        return true;
      } else if (this.layoutAborted) {
        return false;
      } else {
        return (this.diagram.energyPerNode > this.accuracy);
      }
    } else {
      return false;
    }
  }

  animationUpdate() {
    if (this.diagram == null) {
    } else if (this.phase.status == 'none') {
    } else if (this.phase.status == 'simple') {
      this.progressMessage = `calculating layout ${this.nrNodes} nodes\n\n` +
        `PHASE I accuracy ${this.phase.stability}%`;
      if (this.phase.stability < 100) {
        this.incTimeOut();
        this.phase.stability = this.stabilize(this.simpleDiagram, 1, this.timeOut);
      } else {
        this.noselfDiagram = new StateDiagramNoSelf(this.data);
        this.noselfDiagram.copyLayout(this.simpleDiagram);
        console.log('----------- PHASE II -----------');
        console.log('  nr nodes: ' + this.noselfDiagram.graph.nodes.length);
        this.phase.status = 'noself';
        // accuracy = .01 * Math.sqrt(this.nrNodes);
        this.accuracy = .001 * this.noselfDiagram.SDNodes.length;
        this.phase.stability = this.stabilize(this.noselfDiagram, this.accuracy, this.timeOut);
      }
    } else if (this.phase.status == 'noself') {
      this.progressMessage = `calculating layout ${this.nrNodes} nodes\n\n` +
        `PHASE I accuracy 100%\n\n` +
        `PHASE II accuracy ${this.phase.stability}%`;
      if (this.phase.stability < 100) {
        this.incTimeOut();
        this.phase.stability = this.stabilize(this.noselfDiagram, this.accuracy, this.timeOut);
      } else {
        this.fullDiagram = new StateDiagramFull(this.data);
        this.fullDiagram.copyLayout(this.noselfDiagram);
        this.fullDiagram.update();
        console.log('----------- PHASE III -----------');
        console.log('  nr nodes: ' + this.fullDiagram.graph.nodes.length);
        this.phase.status = 'full';
        this.accuracy = .001 * this.fullDiagram.SDNodes.length;
        this.phase.stability = this.stabilize(this.fullDiagram, this.accuracy, this.timeOut);
      }
    } else if (this.phase.status == 'full') {
      this.progressMessage = `calculating layout ${this.nrNodes} nodes\n\n` +
        `PHASE I accuracy 100%\n\n` +
        `PHASE II accuracy 100%\n\n` +
        `PHASE III accuracy ${this.phase.stability}%`;
      if (this.phase.stability < 100 && !this.layoutAborted) {
        this.incTimeOut();
        this.phase.stability = this.stabilize(this.fullDiagram, this.accuracy, this.timeOut);
      } else {
        this.phase.status = 'stable';
      }
    } else if (this.phase.status == 'stable') {
      this.diagram = this.fullDiagram;
      this.drawing.updateContent(this.diagram);
      // ?? this.world.set(this.drawing.bounds);
      this.world.fit(this.drawing.scaledBounds);
      this.setCursor('default');
      this.phase.status = 'final';
    } else if (this.phase.status == 'final') {
      if (this.isDraggingObject()) {
        // fixate the node to the mousepoint
        let wpt = this.world.mousePoint();
        this.dragIt(wpt.x-this.drag.offset.x, wpt.y-this.drag.offset.y);
      }
      if (!this.layoutAborted) this.diagram.updateContent();
    }
  }

  toggleSelf() {
    if (this.diagram instanceof StateDiagramFull) {
      this.showSelf = !this.showSelf;
      this.diagram.drawSelf(this.showSelf);
      this.redrawNeeded = true;
    }
  }
}

