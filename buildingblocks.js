/*
 * Copyright (C) 2020, 2021, 2022, 2023, 2024 Rob Wieringa <rma.wieringa@gmail.com>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

/***********************************************************
 * Building blocks are base for all visualisation.
 ***********************************************************/

/*
 * When DEBUG is on, additional visualisation is presented.
 * currently: each Frame will show a red border
 */
let DEBUG = false;

/*
  Svg: a collection of static svg methods
 */

class Svg {

  static id = 0;

  static element(etype, cls) {
    let s = document.createElementNS('http://www.w3.org/2000/svg', etype);
    if (cls) s.setAttribute('class', cls);
    s.setAttribute('id', Svg.id++);
    return s;
  }

  static group(cls) {
    return Svg.element('g', cls);
  }

  static setVisible(obj, viz) {
    let val = viz ? 'visible' : 'hidden';
    obj.childNodes.forEach(n => n.setAttribute('visibility', val));
  }

  static setBounds(obj, bounds) {
    obj.setAttribute('x', bounds.x);
    obj.setAttribute('y', bounds.y);
    obj.setAttribute('width', bounds.width);
    obj.setAttribute('height', bounds.height);
    if ((bounds.scaleX || bounds.scaleY) && (bounds.scaleX != 1 || bounds.scaleY != 1)) {
      obj.setAttribute('transform', `scale(${bounds.scaleX} ${bounds.scaleY})`);
    }
  }

  static setPos(obj, bounds) {
    obj.setAttribute('transform', `translate(${bounds.x} ${bounds.y})`);
  }

  static setScale(obj, bounds) {
    if (bounds.scaleX != 1 || bounds.scaleY != 1) {
      obj.setAttribute('transform', `scale(${bounds.scaleX} ${bounds.scaleY})`);
    }
  }
}

/*
 * BuildingBlock: the abstract base class for all building blocks.
 * elements:
 *   parent: the containing BuildingBLock (if any)
 *   _bounds: { x: float, y: float, width: float, height: float, scaleX: float, scaleY: float };
 *     location and dimensions  (default { 0, 0, 0, 0, 1, 1 }
 *   changed: boolean: the block is changed; no new svg is generated (!yet)
 *   upToDate: boolean: no 'severe' change has occurred, so update() is not necessary
 * functions:
 *   bounds: nil --> {float, float, float, float, float, float}
 *     returns current {x, y, with, height, scaleX, scaleY}
 *   relativeBounds: BuildingBlock --> {float, float, float, float, float, float}
 *     returns current {x, y, with, height, scaleX, scaleY} relative to a given parent
 *   scaledBounds: nil -> bounds
 *     returns bounds respecting potential scaling
 *   update: nil -> nil
 *     if !this.upToDate: update its sub blocks (if any)
 *        and recalculates dimensions (if relevant)
 *   move: (float, float) --> nil
 *     sets the location, and moves its sub blocks (if any)
 *   hCenter: float --> nil
 *     horizontally center around given x
 *   center: (float, float) --> nil
 *     horizontally and vertically center around given (x, y)
 *   shift: (float, float) --> nil
 *     relative move over (dz, dy)
 *   svg: nil --> SVG
 *     generate DOM svg element
 *   transform: nil -> nil
 *     translate and scale appropriately before drawing the shape
 *   atPos: (float, float) --> bool
 *     returns true if the given (x, y) is within bounds
 *   objectsAt: (float, float) --> [BuildingBlock]
 *     if atPos(x, y) returns itself and all sub blocks that are atPos(x, y)
 *   highlight: bool --> nil
 *     set or reset highlighted
 */
class BuildingBlock {
  static bid = 0;

  constructor() {
    this.bid = BuildingBlock.bid++;
    this._parent = null;
    this._bounds = { x: 0, y: 0, width: 0, height: 0, scaleX: 1, scaleY: 1 };
    this._highlighted = false;
    this.color = '#000000';
    this.highlightColor = '#55AAFF';
    this.shadowColor = '#64646464';
    this.alpha = '#FF';
    this._visible = true;
    this._svg = null;
    this._changed = true;
    this._upToDate = false;
  }

  isModified() {
    this.changed = true;
    this.upToDate = false;
  }

  set parent(parent) {
    this._parent = parent;
    this.upToDate = false;
  }

  get parent() {
    return this._parent;
  }

  get bounds() {
    return this._bounds;
  }

  set bounds(val) {
    if (this._bounds.x != val.x || this._bounds.y != val.y ||
        this._bounds.width != val.width || this._bounds.height != val.height ||
        this._bounds.scaleX != val.scaleX || this._bounds.scaleY != val.scaleY) {
      this._bounds.x = val.x;
      this._bounds.y = val.y;
      this._bounds.width = val.width;
      this._bounds.height = val.height;
      this._bounds.scaleX = val.scaleX;
      this._bounds.scaleY = val.scaleY;
      this.isModified();
    }
  }

  set boundsXYWH(val) {
    val.scaleX = this._bounds.scaleX;
    val.scaleY = this._bounds.scaleY;
    this.bounds = val;
  }

  set boundsWH(val) {
    val.x = this._bounds.x;
    val.y = this._bounds.y;
    val.scaleX = this._bounds.scaleX;
    val.scaleY = this._bounds.scaleY;
    this.bounds = val;
  }

  set boundsWidth(val) {
    if (this._bounds.width != val) {
      this._bounds.width = val;
      this.isModified();
    }
  }

  set boundsHeight(val) {
    if (this._bounds.height != val) {
      this._bounds.height = val;
      this.isModified();
    }
  }

  get highlighted() {
    return this._highlighted;
  }

  set highlighted(val) {
    if (this._highlighted != val) {
      this._highlighted = val;
      this.isModified();
    }
  }

  get visible() {
    return this._visible;
  }

  set visible(val) {
    if (this._visible != val) {
      this._visible = val;
      this.isModified();
    }
  }

  get changed() {
    return this._changed;
  }

  set changed(val) {
    this._changed = val;
    if (val && this._parent)
      this._parent.changed = true;
  }

  get upToDate() {
    return this._upToDate;
  }

  forceUpdate() {
    this.upToDate = false;
    this.update();
  }

  update() {
    this._upToDate = true;
  }

  set upToDate(val) {
    this._upToDate = val;
    if (!val && this._parent)
      this._parent.upToDate = false;
  }

  get scaledBounds() {
    return {x: this._bounds.x, y: this._bounds.y,
            width: this._bounds.width*this._bounds.scaleX,
            height: this._bounds.height*this._bounds.scaleY,
            scaleX: this._bounds.scaleX, scaleY: this._bounds.scaleY};
  }

  relativeBounds(parent) {
    let bnd = this.scaledBounds;
    if (this.parent && this.parent != parent) {
      let rbnd = this.parent.parentalRelativeBounds(parent);
      return {x: rbnd.x + bnd.x*rbnd.scaleX, y: rbnd.y + bnd.y*rbnd.scaleY,
              width: bnd.width*bnd.scaleX*rbnd.scaleX, height: bnd.height*bnd.scaleY*rbnd.scaleY,
              scaleX: bnd.scaleX*rbnd.scaleX, scaleY: bnd.scaleY*rbnd.scaleY};
    } else {
      return bnd;
    }
  }

  // default immplementation. only exceptions are in HasViz and Group
  parentalRelativeBounds(parent) {
    return this.relativeBounds(parent);
  }

  get absoluteBounds() {
    return this.relativeBounds(null);
  }

  hMove(x) {
    if (this._bounds.x != x) {
      this._bounds.x = x;
      this.isModified();
    }
  }

  vMove(y) {
    if (this.bounds.y != y) {
      this._bounds.y = y;
      this.isModified();
    }
  }

  move(x, y) {
    this.hMove(x);
    this.vMove(y);
  }

  moveAbsolute(ax, ay) {
    let abs = this.absoluteBounds;
    this.shift(ax-abs.x, ay-abs.y);
  }

  hCenter(x) {
    this.hMove(x-this.bounds.width/2);
  }

  vCenter(y) {
    this.vMove(y-this.bounds.height/2);
  }

  center(x, y) {
    this.move(x-this.bounds.width/2, y-this.bounds.height/2);
  }

  shift(dx, dy) {
    let oldx = this.bounds.x;
    let oldy = this.bounds.y;
    this.move(oldx+dx, oldy+dy);
  }

  scale(scaleX, scaleY) {
    if (this._bounds.scaleX != scaleX || this._bounds.scaleY != scaleY) {
      this._bounds.scaleX = scaleX;
      this._bounds.scaleY = scaleY;
      this.isModified();
    }
  }

  atPos(x, y) {
    // check bounding box, allow some margin
    let eps = 2;
    return this.bounds.x - eps <= x &&
      x <= this.bounds.x + this.bounds.width*this.bounds.scaleX + eps &&
      this.bounds.y - eps <= y
      && y <= this.bounds.y + this.bounds.height*this.bounds.scaleY + eps;
  }

  objectsAt(x, y) {
    return this.atPos(x, y) ? [this] : [];
  }

  highlight(on) {
    this.highlighted = on;
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('BuildingBlock');
    this.changed = false;
    return this._svg;
  }

  svgAddDecoration(etype, settings) {
    // shadow:
    if (this.shadowed) {
      let shadow = Svg.element(etype, 'shadow');
      let eps = 3;
      settings(shadow, eps, eps);
      shadow.setAttribute('fill', this.shadowColor);
      shadow.setAttribute('stroke', 'none');
      this._svg.append(shadow);
    }

    // fill:
    let fill = Svg.element(etype, 'fill');
    settings(fill, 0, 0);
    fill.setAttribute('fill', this.color);
    fill.setAttribute('stroke', 'none');
    this._svg.append(fill);

    // highlight:
    if (this.highlighted) {
      let highlight = Svg.element(etype, 'highlight');
      settings(highlight, 0, 0);
      highlight.setAttribute('fill', 'none');
      highlight.setAttribute('stroke', this.highlightColor);
      highlight.setAttribute('stroke-width', this.strokeWeight+2);
      this._svg.append(highlight);
    }

    // border:
    let border = Svg.element(etype, 'border');
    settings(border, 0, 0);
    border.setAttribute('fill', 'none');
    border.setAttribute('stroke', this.strokeColor);
    border.setAttribute('stroke-width', this.strokeWeight);
    this._svg.append(border);
  }

  svgAddLineDecoration(etype, settings) {
    // highlight:
    if (this.highlighted) {
      let highlight = Svg.element(etype, 'highlight');
      settings(highlight, 0, 0);
      highlight.setAttribute('fill', 'none');
      highlight.setAttribute('stroke', this.highlightColor);
      highlight.setAttribute('stroke-width', this.weight+2);
      this._svg.append(highlight);
    }

    // line:
    let line = Svg.element(etype, 'line');
    settings(line, 0, 0);
    line.setAttribute('fill', 'none');
    line.setAttribute('stroke', this.color);
    line.setAttribute('stroke-width', this.weight);
    this._svg.append(line);
  }
}

/*
 * Bordered()
 *   a BuildingBlock with a border.
 *   by default the internals are white, the border is black
 */
class Bordered extends BuildingBlock {
  constructor() {
    super();
    this.color = '#FFFFFF';
    this.strokeColor = '#000000';
    this.strokeWeight = .5;
  }
}

/*
 * Stroke()
 *   a line-like BuildingBlock.
 *   by default the stroke is black
 */
class Stroke extends BuildingBlock {
  constructor() {
    super();
    this.strokeColor = '#000000';
    this.weight = 1;
  }
}

/*
 * Box()
 *   a rectangle, default 1x1, colored black.
 *   currently its border look is fixed
 */
class Box extends Bordered {
  constructor() {
    super();
    this.boundsWH = {width: 1, height: 1};
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('Box');
    let etype = 'rect';
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      Svg.setBounds(obj, { x: x, y: y, width: this.bounds.width, height: this.bounds.height, scaleX: this.bounds.scaleX, scaleY: this.bounds.scaleY });
    };
    this.svgAddDecoration(etype, settings);
    this.changed = false;
    return this._svg;
  }
}

/*
 * DTriangle()
 *   a downward triangle
 *   currently its border look is fixed
 */
class DTriangle extends Bordered {
  constructor() {
    super();
    this.boundsWH = {width: 1, height: 1};
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('DTriangle');
    let etype = 'polygon';
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      obj.setAttribute('points',
                       `${x}, ${y}  ` +
                       `${x + this.bounds.width}, ${y} ` +
                       `${x + this.bounds.width/2}, ${y + this.bounds.height}`);
    };
    this.svgAddDecoration(etype, settings);
    this.changed = false;
    return this._svg;
  }
}

/*
 * Text(text)
 *   a (left-aligned) representation of given text (string)
 *   currently its looks (font, size, and color) are fixed
 *   its width is calculated
 */
class Text extends BuildingBlock {
  static canvas = document.createElement("canvas");
  static context = Text.canvas.getContext("2d");
  constructor(text) {
    super();
    this._text = text;
    this.font = 'arial';
    this.size = 15;
    this._bold = false;
    this.fixedWidth = false;
    this.centered = false;
    this.boundsWH = {width: this.vWidth(), height: this.vHeight()};
  }

  get text() {
    return this._text;
  }

  set text(val) {
    if (this._text != val) {
      this._text = val;
      this.isModified();
    }
  }

  get bold() {
    return this._bold;
  }

  set bold(val) {
    if (this._bold != val) {
      this._bold = val;
      this.isModified();
    }
  }

  setWidth(w) {
    if (!this.fixedWidth || this.bounds.width != w) {
      this.fixedWidth = true;
      this.boundsWidth = w;
      this.isModified();
    }
  }

  vWidth() {
    let width = 0;
    // (c) https://www.w3docs.com/snippets/javascript/how-to-calculate-text-width-with-javascript.html
    Text.context.font = (this.bold ? 'bold ' : '') + this.size + 'px ' + this.font;
    this.text.split(/\n/).forEach(line => {
      let metrics = Text.context.measureText(line);
      width = Math.max(width, metrics.width);
    });
    return width;
  }

  vHeight() {
    let lines = this.text.split(/\n/);
    return this.size * lines.length;
  }

  update() {
    if (this.upToDate) return;
    let vwidth = this.vWidth();
    let vheight = this.vHeight();
    if ((!this.fixedWidth && this.bounds.width != vwidth) ||
        this.bounds.height != vheight) {
      if (!this.fixedWidth) this.boundsWidth = this.vWidth();
      this.boundsHeight = this.vHeight();
      this.changed = true;
    }
    this.upToDate = true;
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.element('text', 'Text');
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    this._svg.setAttribute('x', this.centered ? this.bounds.width/2 : 0);
    this._svg.setAttribute('y', -this.size/2);
    this.text.split(/\n/).forEach(line => {
      let t = Svg.element('tspan', 'text');
      t.setAttribute('x', this.centered ? this.bounds.width/2 : 0);
      t.setAttribute('dy', this.size);
      let strong = line.length > 0 && line[0] == '$';
      let text = strong ? line.substring(1) : line;
      // non-default style:
      if (this.bold || this.centered || this.size != 15 || strong) {
        let weight = this.bold ? 'font-weight: bold; ' : '';
        let anchor = this.centered ? 'text-anchor: middle; ' : '';
        let size = this.size != 15 ? `font-size: ${this.size}px; ` : '';
        let fill = strong ? 'fill: #CC0000; ' : '';
        t.setAttribute('style', weight + anchor + size + fill);
      }
      let n = document.createTextNode(text);
      // node.nodeValue = line;
      t.append(n);
      this._svg.append(t);
    });
    this.changed = false;
    return this._svg;
  }

  // default style:
  static style() {
    let style = Svg.element('style');
    let textstyle = document.createTextNode('.Text {fill: #000000; stroke: none; font-family: arial; font-size: 15px; font-weight: normal; text-anchor: start; dominant-baseline: central;}');
    style.append(textstyle);
    return style;
  }
}

/*
 * VLine(len)
 *   a thin vertical line of given length (float)
 *   currently its looks are fixed
 */
class VLine extends Stroke {
  constructor(len) {
    super();
    this.boundsWH = {width: 0, height: len};
    this.weight = .5;
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('VLine');
    let etype = 'line';
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      obj.setAttribute('x1', x);
      obj.setAttribute('y1', y);
      obj.setAttribute('x2', x);
      obj.setAttribute('y2', y + this.bounds.height);
    };
    this.svgAddLineDecoration(etype, settings);
    this.changed = false;
    return this._svg;
  }
}

/*
 * Dot()
 *   an (almost invisible) dot
 *   can be used as anchor point
 */
class Dot extends BuildingBlock {
  constructor() {
    super();
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('Dot');
    Svg.setPos(this._svg, this.bounds);
    this.changed = false;
    return this._svg;
  }
}

/*
 * RectLine(fx, fy, tx, ty)
 *  a line from (fx, fy) to (tx, ty).
 */
class RectLine extends Stroke {
  constructor(fx, fy, tx, ty) {
    super();
    this.setPoints(fx, fy, tx, ty);
  }

  setPoints(fx, fy, tx, ty) {
    if (this.fx != fx || this.fy != fy || this.tx != tx || this.ty != ty) {
      this.fx = fx;
      this.fy = fy;
      this.tx = tx;
      this.ty = ty;
      this.boundsXYWH = {x: Math.min(fx,tx), y: Math.min(fy,ty),
                         width: Math.abs(fx-tx), height: Math.abs(fy-ty)};
    }
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('RectLine');
    let etype = 'line';
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      obj.setAttribute('x1', x + this.fx-this.bounds.x);
      obj.setAttribute('y1', y + this.fy-this.bounds.y);
      obj.setAttribute('x2', x + this.tx-this.bounds.x);
      obj.setAttribute('y2', y + this.ty-this.bounds.y);
      obj.setAttribute('stroke-linecap', 'square');
    };
    this.svgAddLineDecoration(etype, settings);
    this.changed = false;
    return this._svg;
  }
}

/*
 * DirectedRectLine(fx, fy, tx, ty)
 *  a 'triangular' line starting at (fx, fy), ending at (tx, ty).
 *  the thickness of the line diminishes from start towards end
 */
class DirectedRectLine extends BuildingBlock {
  constructor(fx, fy, tx, ty) {
    super();
    this.thicknessStart = 3;
    this.thicknessEnd = 0;
    this.setPoints(fx, fy, tx, ty);
  }

  setPoints(fx, fy, tx, ty) {
    if (this.fx != fx || this.fy != fy || this.tx != tx || this.ty != ty) {
      this.fx = fx;
      this.fy = fy;
      this.tx = tx;
      this.ty = ty;
      this.boundsXYWH = {x: Math.min(fx,tx), y: Math.min(fy,ty),
                         width: Math.abs(fx-tx), height: Math.abs(fy-ty)};
    }
  }

  move(x, y) {
    let dx = x - this.bounds.x;
    let dy = y - this.bounds.y;
    this.setPoints(this.fx+dx, this.fy+dy, this.tx+dx, this.ty+dy);
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('DirectedRectLine');
    let etype = 'polygon';
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      let dx = this.tx-this.fx;
      let dy = this.ty-this.fy;
      let len = Math.sqrt(dx*dx + dy*dy);
      let ex = dy / len;
      let ey = dx / len;
      let start = this.thicknessStart;
      let end = this.thicknessEnd;
      let points = [[x + this.fx - this.bounds.x + ex*start, y + this.fy - this.bounds.y - ey*start],
                    [x + this.tx - this.bounds.x + ex*end, y + this.ty - this.bounds.y - ey*end],
                    [x + this.tx - this.bounds.x - ex*end, y + this.ty - this.bounds.y + ey*end],
                    [x + this.fx - this.bounds.x - ex*start, y + this.fy -this.bounds.y + ey*start]]
      obj.setAttribute('points', points.map(pt => `${pt[0]}, ${pt[1]}`).join(' '));
    };
    this.svgAddDecoration(etype, settings);
    this.changed = false;
    return this._svg;
  }
}

/*
 * HRectLine(fx, fy, tx, ty)
 *  a segmented line from (fx, fy) to (tx, ty).
 *  Currently 3 segments are drawn, the middle one being horizontal.
 *  Corners are 'rounded'
 */
class HRectLine extends Stroke {
  constructor(fx, fy, tx, ty) {
    super();
    this._yMiddle = 10; // relative to fy
    this.curve = 2; // roundness of corners
    this.setPoints(fx, fy, tx, ty);
  }

  set yMiddle(val) {
    if (this._yMiddle != val) {
      this._yMiddle = val;
      this.isModified();
    }
  }

  get yMiddle() {
    return this._yMiddle;
  }

  setPoints(fx, fy, tx, ty) {
    if (this.fx != fx || this.fy != fy || this.tx != tx || this.ty != ty) {
      this.fx = fx;
      this.fy = fy;
      this.tx = tx;
      this.ty = ty;
      this.boundsXYWH = {x: Math.min(fx,tx), y: Math.min(fy,ty),
                         width: Math.abs(fx-tx), height: Math.abs(fy-ty)};
    }
  }

  move(x, y) {
    let dx = x - this.bounds.x;
    let dy = y - this.bounds.y;
    this.setPoints(this.fx+dx, this.fy+dy, this.tx+dx, this.ty+dy);
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('HRectLine');
    let etype = 'polyline';
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      let points = [];
      points.push([x + this.fx - this.bounds.x, y + this.fy - this.bounds.y]);
      let sgn = (this.fx < this.tx) ? 1 : -1;
      if (Math.abs(this.fx - this.tx) < this.curve*2) {
        points.push([x + this.fx - this.bounds.x, y + this.fy+this.yMiddle - this.bounds.y]);
      } else {
        points.push([x + this.fx - this.bounds.x, y + this.fy+this.yMiddle-this.curve - this.bounds.y]);
        points.push([x + this.fx+this.curve*sgn - this.bounds.x, y + this.fy+this.yMiddle - this.bounds.y]);
      }
      if (Math.abs(this.fx - this.tx) < this.curve*2) {
        points.push([x + this.tx - this.bounds.x, y + this.fy+this.yMiddle - this.bounds.y]);
      } else {
        points.push([x + this.tx-this.curve*sgn - this.bounds.x, y + this.fy+this.yMiddle - this.bounds.y]);
        points.push([x + this.tx - this.bounds.x, y + this.fy+this.yMiddle+this.curve - this.bounds.y]);
      }
      points.push([x + this.tx - this.bounds.x, y + this.ty - this.bounds.y]);
      obj.setAttribute('points', points.map(pt => `${pt[0]}, ${pt[1]}`).join(' '));
      obj.setAttribute('stroke-linecap', 'square');
    };
    this.svgAddLineDecoration(etype, settings);
    this.changed = false;
    return this._svg;
  }

  atPos(x, y) {
    // bounding box isn't good enough
    if (!super.atPos(x, y)) return false;

    let eps = 4;
    let midy = this.bounds.y + this.yMiddle*this.bounds.scaleY;
    let rightx = this.bounds.x + this.bounds.width*this.bounds.scaleX;

    if (this.bounds.x == this.fx) {
      return (y < midy && this.bounds.x - eps <= x && x <= this.bounds.x + eps ||
              y > midy && rightx - eps <= x && x <= rightx + eps ||
              midy - eps <= y && y <= midy + eps);
    } else {
      return (y < midy && rightx - eps <= x && x <= rightx + eps ||
              y > midy && this.bounds.x - eps <= x && x <= this.bounds.x + eps ||
              midy - eps <= y && y <= midy + eps);
    }
  }
}

/*
 * HArrow(fx, tx, y)
 *  a horizontal arrow from (fx, y) to (tx, y)
 *  currently its point dimensions are fixed
 */
class HArrow extends Stroke {
  constructor(fx, tx, y) {
    super();
    this.pointHeight = 6;
    this.pointWidth = 10;
    this.dotsize = 3;
    this.setPoints(fx, tx, y);
    this.fx;
    this.tx;
  }

  setPoints(fx, tx, y) {
    if (this.fx != fx || this.tx != tx || this.y != y) {
      this.fx = fx;
      this.tx = tx;
      this.boundsXYWH = {x: Math.min(fx,tx), y: y - this.pointHeight/2,
                         width: Math.abs(fx-tx), height: this.pointHeight};
    }
  }

  move(x, y) {
    if (this.fx < this.tx) {
      this.setPoints(x, x+this.tx-this.fx, y+this.pointHeight/2);
    } else {
      this.setPoints(x+this.fx-this.tx, x, y+this.pointHeight/2);
    }
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('HArrow');
    let etype = 'polyline';
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      let dir = (this.fx < this.tx) ? 1 : -1;
      let tox = x + this.tx - this.bounds.x - dir*this.pointWidth;
      let points = [[x + this.fx - this.bounds.x, y + this.pointHeight/2],
                    [tox, y + this.pointHeight/2],
                    [tox, y],
                    [tox + dir*this.pointWidth, y + this.pointHeight/2],
                    [tox, y + this.pointHeight],
                    [tox, y + this.pointHeight/2],
                   ];
      obj.setAttribute('points', points.map(pt => `${pt[0]}, ${pt[1]}`).join(' '));
    };
    this.svgAddLineDecoration(etype, settings);
    this.changed = false;
    return this._svg;
  }
}

/*
 * Group(content)
 *  a grouping of its content, a list of BuildingBlock-s
 *  elements do not have relative position
 *  the group has an origin, relative to which all elements are drawn
 */
class Group extends BuildingBlock {
  constructor(content) {
    super();
    this.origin = {x: 0, y: 0};
    this.content = content; // [buildingblock]
  }

  set content(content) {
    content.forEach(bb => bb.parent = this);
    this._content = content;
    this.isModified();
  }

  get content() {
    return this._content;
  }

  parentalRelativeBounds(parent) {
    let bnd = {x: this.origin.x, y: this.origin.y,
               width: this.bounds.width, height: this.bounds.height,
               scaleX: this.bounds.scaleX, scaleY: this.bounds.scaleY};
    if (this.parent && this.parent != parent) {
      let rbnd = this.parent.parentalRelativeBounds(parent);
      return {x: rbnd.x + bnd.x*rbnd.scaleX, y: rbnd.y + bnd.y*rbnd.scaleY,
              width: bnd.width*bnd.scaleX*rbnd.scaleX, height: bnd.height*bnd.scaleY*rbnd.scaleY,
              scaleX: bnd.scaleX*rbnd.scaleX, scaleY: bnd.scaleY*rbnd.scaleY};
    } else {
      return bnd;
    }
  }

  update() {
    if (this.upToDate) return;
    this._content.forEach(c => c.update());
    if (this._content.length > 0) {
      let minx = 9999999;
      let maxx = -9999999;
      let miny = 9999999;
      let maxy = -9999999;
      this._content.forEach(c => {
        let bnd = c.scaledBounds;
        minx = Math.min(minx, bnd.x);
        maxx = Math.max(maxx, bnd.x + bnd.width);
        miny = Math.min(miny, bnd.y);
        maxy = Math.max(maxy, bnd.y + bnd.height);
      });
      this.bounds = {x: this.origin.x + minx, y: this.origin.y + miny,
                     width: maxx - minx, height: maxy - miny,
                     scaleX: this._bounds.scaleX, scaleY: this._bounds.scaleY};
    }
    this.upToDate = true;
  }

  move(x, y) {
    this.origin.x += x - this.bounds.x;
    this.origin.y += y - this.bounds.y;
    this.bounds.x = x;
    this.bounds.y = y;
    this.isModified();
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('Group');
    if (!this.visible) return this._svg;
    Svg.setPos(this._svg, {x: this.origin.x, y: this.origin.y});
    this._content.forEach(c => this._svg.append(c.svg));
    return this._svg;
  }

  atPos(x, y) {
    // Group is dimensionless, so no restrictions
    return true;
  }

  objectsAt(x, y) {
    let result = [];
    this._content.forEach(c => {
      result = result.concat(c.objectsAt(x - this.origin.x, y - this.origin.y))
    });
    return result;
  }
}

/*
 * Frame(content)
 *  an invisible frame around its content, a list of BuildingBlock-s
 *  Located at (0, 0) upon construction
 *  Function update() updates the Frame's dimensions.
 */
class Frame extends BuildingBlock {
  constructor(content) {
    super();
    this._content = [];
    this.content = content; // [buildingblock]
  }

  set content(content) {
    let changed = this._content.length != content.length;
    if (!changed) {
      content.forEach((bb, i) => changed = changed || bb != this._content[i]);
    }
    if (changed) {
      this._content = content;
      content.forEach(bb => bb.parent = this);
      this.isModified();
    }
  }

  get content() {
    return this._content;
  }

  update() {
    if (this.upToDate) return;
    this._content.forEach(c => c.update());
    this.updateBounds();
    this.upToDate = true;
  }

  updateBounds() {
    if (this._content.length > 0) {
      let minx = 9999999;
      let maxx = -9999999;
      let miny = 9999999;
      let maxy = -9999999;
      this._content.forEach(c => {
        let bnd = c.scaledBounds;
        minx = Math.min(minx, bnd.x);
        maxx = Math.max(maxx, bnd.x + bnd.width);
        miny = Math.min(miny, bnd.y);
        maxy = Math.max(maxy, bnd.y + bnd.height);
      });
      this.boundsWH = {width: maxx - minx, height: maxy - miny};
      this._content.forEach(c => {
        c.shift(-minx, -miny);
      });
      this.shift(minx, miny);
    }
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('Frame');
    if (!this.visible) return this._svg;
    if (DEBUG) {
      let debug = Svg.element('rect', 'debug');
      let bnd = this.scaledBounds;
      Svg.setBounds(debug, {x: 0, y: 0, width: this.bounds.width, height: this.bounds.height});
      debug.setAttribute('fill', 'none');
      debug.setAttribute('stroke', '#FF0000');
      debug.setAttribute('stroke-width', 2.5);
      this._svg.append(debug);
    }
    this._content.forEach(c => this._svg.append(c.svg));
    Svg.setPos(this._svg, this.bounds);
    Svg.setScale(this._svg, this.bounds);
    this.changed = false;
    return this._svg;
  }

  objectsAt(x, y) {
    if (this.atPos(x, y)) {
      let result = [];
      this._content.forEach(c => result = result.concat(c.objectsAt(x-this.bounds.x, y-this.bounds.y)));
      return result.concat([this]);
    } else {
      return [];
    }
  }

  hCenterElement(element) {
    element.hCenter(this.bounds.width/2);
  }
}

/*
 * Circle(radius, color)
 *  a circle with default radius 1 (float)
 */
class Circle extends Bordered {
  constructor() {
    super();
    this._radius = 1; // float
    this.shadowed = false;
    this.update();
  }

  set radius(val) {
    if (this._radius != val) {
      this._radius = val;
      this.isModified();
    }
  }

  get radius() {
    return this._radius;
  }

  update() {
    if (this.upToDate) return;
    this.boundsWH = {width: 2 * this.radius, height: 2 * this.radius};
    this.upToDate = true;
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('Circle');
    let etype = 'circle';
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      obj.setAttribute('cx', x + this.radius);
      obj.setAttribute('cy', y + this.radius);
      obj.setAttribute('r', this.radius);
    };
    this.svgAddDecoration(etype, settings);
    this.changed = false;
    return this._svg;
  }
}

/*
  a container with padding around its content
 */

class Padded extends Bordered {
  constructor(content) {
    super();
    this._content = content; // buildingblock
    this._content.parent = this;
    this._padding = {left: 1, top: 1, right: 1, bottom: 1};
  }

  set padding(pad) {
    let left, right, top, bottom;
    if (typeof pad === 'object') {
      left = pad.left;
      right = pad.right;
      top = pad.top;
      bottom = pad.bottom;
    } else {
      left = right = top = bottom = pad;
    }
    if (this._padding.left != left || this._padding.top != top ||
        this._padding.right != right || this._padding.bottom != bottom) {
      if (typeof pad === 'object') {
        this._padding = pad;
      } else {
        this._padding.left = left;
        this._padding.top = top;
        this._padding.right = right;
        this._padding.bottom = bottom;
      }
      this.isModified();
    }
  }

  get padding() {
    return this._padding;
  }

  set content(content) {
    let changed = content != this._content;
    if (changed) {
      this._content = content;
      content.parent = this;
      this.isModified();
    }
  }

  get content() {
    return this._content;
  }

  update() {
    if (this.upToDate) return;
    this._content.update();
    this.updateBounds();
    this.upToDate = true;
  }

  updateBounds() {
    this._content.move(this._padding.left, this._padding.top);
    let cbnd = this._content.scaledBounds;
    let w = Math.max(this.minWidth, cbnd.width) + (this._padding.left+this._padding.right);;
    let h = cbnd.height + (this._padding.top+this._padding.bottom);
    this.boundsWH = {width: w, height: h};
    this.upToDate = true;
  }

  // get svg() { } // virtual

}

/*
 * RoundedBoundingBox(content)
 *  a rectangle with rounded corners around its content (BuildingBlock).
 *  Located at (0, 0) upon construction
 *  If visible is false, only the content is drawn.
 */
class RoundedBoundingBox extends Padded {
  constructor(content) {
    super(content);
    this.visible = true; // boolean
    this.color = '#FFFFFF';
    this.round = 1; // in pixels
    this.minWidth = 0;
    this.shadowed = true;
    this.update();
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('RoundedBoundingBox');
    let etype = 'rect';
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      Svg.setBounds(obj, { x: x, y: y,
                           width: this.bounds.width, height: this.bounds.height,
                           scaleX: this.bounds.scaleX, scaleY: this.bounds.scaleY });
      obj.setAttribute('rx', this.round);
      obj.setAttribute('ry', this.round);
    };
    this.svgAddDecoration(etype, settings);
    let content = this._content.svg;
    this._svg.append(content);
    this.changed = false;
    return this._svg;
  }

  objectsAt(x, y) {
    if (this.atPos(x, y)) {
      return  this._content.objectsAt(x-this.bounds.x, y-this.bounds.y).concat([this]);
    } else {
      return [];
    }
  }
}

/*
 * BoundingBox(content)
 *  a RoundedBoundingBox without rounded corners
 */
class BoundingBox extends RoundedBoundingBox {
  constructor(content) {
    super(content);
    this.round = 0;
  }
}

/*
 * TextBox(text)
 *  a RoundedBoundingBox with as content text (Text)
 */
class TextBox extends RoundedBoundingBox {
  constructor(text) {
    super(text);
    this.round = 7;
  }
}

/*
 * Button(text, callback, parent)
 *  a button, represented as a RoundedBoundingBox
 *  with given text, function callback: nil --> nil,
 * and parent: BuildingBlock
 */
class Button extends RoundedBoundingBox {
  constructor(text, callback, manager) {
    super(new Text(text));
    this.padding = {left: 5, top: 0, right: 5, bottom: 0};
    this.color = '#EEEEEE';
    this.round = 1;
    this.shadowed = false;
    this.text = text;
    this.callback = callback;
    this.manager = manager;
  }

  setText(text) {
    if (this._content.text != text) {
      this._content.text = text;
      this.isModified();
    }
  }

  get svg() {
    return super.svg;
  }
}

/* Table(content)
 *   a two dimensional table of Text elements.
 */

class Table extends BuildingBlock {
  constructor(content) {
    super();
    this.content = content;
    this.hpad = 8;
    this.vpad = 8;
    this.hsep = 8;
    this.vsep = 8;
    this.centered = false;

    this.colWidth = [];
    this.rowHeight = [];
    this.update();
  }

  set content(content) {
    this._content = content;
    content.forEach(row => {
      row.forEach(elt => elt.parent = this);
    });
    this.isModified();
  }

  get content() {
    return this._content;
  }

  update() {
    if (this.upToDate) return;
    this._content.forEach(row => {
      row.forEach(elt => elt.update());
    });
    if (this._content.length == 0) {
      this.colWidth = [];
      this.rowHeight = [];
    } else {
      this.colWidth = this._content[0].map(elt => 0);
      this.rowHeight = this._content.map(row => 0);
    }
    this._content.forEach(row => {
      row.forEach((elt, ci) => {
        this.colWidth[ci] = Math.max(this.colWidth[ci], elt.bounds.width);
      });
    });
    this._content.forEach((row, ri) => {
      let rheight = 0;
      row.forEach(elt => {
        rheight = Math.max(rheight, elt.bounds.height);
      });
      this.rowHeight[ri] = rheight;
    });

    let twidth = this.hpad;
    this.colWidth.forEach(cw => { twidth += cw + this.hsep; });
    let theight = this.vpad;
    this.rowHeight.forEach(rh => { theight += rh + this.vsep; });
    let w = twidth - this.hsep + this.hpad;
    let h = theight - this.vsep + this.vpad;
    this.boundsWH = {width: w, height: h};
    this.upToDate = true;
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('Table');
    Svg.setPos(this._svg, this.bounds);
    let lines = [];
    let py = 0;
    this._content.forEach((row, ri) => {
      if (ri > 0) {
        py += this.vsep/2;
        let ln = Svg.element('line', 'line');
        ln.setAttribute('stroke', '#646464');
        ln.setAttribute('stroke-width', .5);
        ln.setAttribute('x1', this.hpad);
        ln.setAttribute('y1', py);
        ln.setAttribute('x2', this.bounds.width-this.hpad);
        ln.setAttribute('y2', py);
        py += this.vsep/2;
        this._svg.append(ln);
      } else {
        py += this.vpad;
      }
      let px = this.hpad;
      row.forEach((elt, ci) => {
        elt.move(px, py);
        if (this.centered) {
          elt.hCenter(px+this.colWidth[ci]/2);
        }
        let e = elt.svg;
        this._svg.append(e);
        px += this.colWidth[ci] + this.hsep;
      });
      py += this.rowHeight[ri];
    });
    this.changed = false;
    return this._svg;
  }
}

/* Alert
 *   an alert icon (red with white exclamation mark).
 */

class Alert extends BuildingBlock {
  constructor() {
    super();
    this.boundsWH = {width: 35, height: 35};
    this.color = '#E74745';
    this.shadowed = true;
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('Alert');
    let etype = 'polygon';
    let ln1 = Svg.element('line', 'line');
    let ln2 = Svg.element('line', 'line');
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      let points = [[x + this.bounds.width/2, y],
                    [x + this.bounds.width, y + this.bounds.height],
                    [x, y + this.bounds.height]];
      obj.setAttribute('points', points.map(pt => `${pt[0]}, ${pt[1]}`).join(' '));
    };
    this.svgAddDecoration(etype, settings);

    // exclamation mark:
    ln1.setAttribute('x1', this.bounds.width/2);
    ln1.setAttribute('y1', 12);
    ln1.setAttribute('x2', this.bounds.width/2);
    ln1.setAttribute('y2', this.bounds.height-11);
    ln1.setAttribute('stroke', '#FFFFFF');
    ln1.setAttribute('stroke-linecap', 'round');
    ln1.setAttribute('stroke-width', 4);
    this._svg.append(ln1);

    ln2.setAttribute('x1', this.bounds.width/2);
    ln2.setAttribute('y1', this.bounds.height-6);
    ln2.setAttribute('x2', this.bounds.width/2);
    ln2.setAttribute('y2', this.bounds.height-5);
    ln2.setAttribute('stroke', '#FFFFFF');
    ln2.setAttribute('stroke-linecap', 'round');
    ln2.setAttribute('stroke-width', 4);
    this._svg.append(ln2);
    this.changed = false;
    return this._svg;
  }
}


/*
 * Diamond()
 *  a diamond shape
 *  Located at (0, 0) upon construction
 */
class Diamond extends Bordered {
  constructor() {
    super();
    this.boundsWH = {width: 1, height: 1};
    this.shadowed = false;
    this.update();
  }

  get svg() {
    if (!this.changed) return this._svg;
    this._svg = Svg.group('Diamond');
    let etype = 'polygon';
    Svg.setVisible(this._svg, this.visible);
    Svg.setPos(this._svg, this.bounds);
    let settings = (obj, x, y) => {
      let points = [[x + this.bounds.width/2, y],
                    [x + this.bounds.width, y + this.bounds.height/2],
                    [x + this.bounds.width/2, y + this.bounds.height],
                    [x, y + this.bounds.height/2]];
      obj.setAttribute('points', points.map(pt => `${pt[0]}, ${pt[1]}`).join(' '));
      obj.setAttribute('stroke-linecap', 'square');
    };
    this.svgAddDecoration(etype, settings);
    this.changed = false;
    return this._svg;
  }
}
