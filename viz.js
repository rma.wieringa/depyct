/*
 * Copyright (C) 2020, 2021, 2022, 2023, 2024 Rob Wieringa <rma.wieringa@gmail.com>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

/***********************************************************
 * HasViz couples data to visualisation
 ***********************************************************/

/*
 * HasViz: the abstract base class for all visualisation classes.
 * inherits from BuildingBlock
 * elements:
 *   data: Data (which can be anything)
 *     the data to be visualised
 *   viz: BuildingBlock
 *     the object's visualisation
 * functions:
 *   new: Data --> HasViz
 *     create HasViz objects for all relevant sub data here.
 *   initViz: nil --> nil
 *     call initViz on all relevant sub viz object here
 *     create the object's own viz (BuildingBlock)
 *   setViz: BuildingBlock --> nil
 *     set the object's viz, and its dimensions
 *   update, move, svg, bounds, shift, objectsAt
 *     apply these functions on the object's viz
 */
class HasViz extends BuildingBlock {
  constructor(data) {
    super();
    this.data = data;
    this.viz = null;
  }

  // virtual:
  initViz() { }

  setViz(viz) {
    this.viz = viz;
    this._bounds = viz._bounds;
    this.viz.parent = this;
    this.isModified();
  }

  get bounds() {
    return this.viz.bounds;
  }

  set bounds(val) {
    this.viz.bounds = val;
  }

  // HasViz exception: relation with child is not relative to (0, 0)
  // so bounds are corrected for that
  parentalRelativeBounds(parent) {
    if (this.parent && this.parent != parent) {
      return this.parent.parentalRelativeBounds(parent);
    } else {
      // shound not occur!
      return {x: 0, y: 0, width: 0, height: 0, scaleX: 1, scaleY: 1};
    }
  }

  update() {
    if (this.upToDate) return;
    this.viz.update();
    this.upToDate = true;
  }

  hMove(x) {
    this.viz.hMove(x);
  }

  vMove(y) {
    this.viz.vMove(y);
  }

  move(x, y) {
    this.viz.move(x, y);
  }

  hCenter(x) {
    this.viz.hCenter(x);
  }

  vCenter(y) {
    this.viz.vCenter(y);
  }

  center(x, y) {
    this.viz.center(x, y);
  }

  shift(dx, dy) {
    this.viz.shift(dx, dy);
  }

  scale(sw, sh) {
    this.viz.scale(sw, sh);
  }

  get scaledBounds() {
    return this.viz.scaledBounds;
  }

  get svg() {
    if (this.changed) {
      this._svg = this.viz.svg;
      this.changed = false;
    }
    return this._svg;
  }

  objectsAt(x, y) {
    if (this.viz.atPos(x, y)) {
      return this.viz.objectsAt(x, y).concat([this]);
    } else {
      return [];
    }
  }

  highlight(on) {
    this.viz.highlight(on);
  }

  setAlpha(a) {
    this.viz.setAlpha(a);
  }
}
