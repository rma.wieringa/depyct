/*
 * Copyright (C) 2021, 2022, 2024, 2025 Rob Wieringa <rma.wieringa@gmail.com>
 *
 * This file is part of Depyct.
 * Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
 *
 * Depyct is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

const keyboard = {
  e: null,

  hasMod: (m) => {
    return ((m == 'Alt') && keyboard.e && keyboard.e.altKey) ||
      ((m == 'Control') && keyboard.e && keyboard.e.ctrlKey) ||
      ((m == 'Shift') && keyboard.e && keyboard.e.shiftKey);
  },

  hasKeyMod: (mod1, mod2) => {
    // mods: CTRL, ALT
    function checkdown(m) {
      return (mod1 == m || mod2 == m) ? keyboard.hasMod(m) : !keyboard.hasMod(m);
    }
    return checkdown('Control') && checkdown('Alt');
  },

  hasKeyCodeMod: (mod1, mod2, mod3) => {
    // mods: SHIFT, CTRL, ALT
    function checkdown(m) {
      return (mod1 == m || mod2 == m || mod3 == m) ? keyboard.hasMod(m) : !keyboard.hasMod(m);
    }
    return checkdown('Shift') && checkdown('Control') && checkdown('Alt');
  },

  isKey: (k, mod1, mod2) => {
    return keyboard.e.key == k && keyboard.hasKeyMod(mod1, mod2);
  },

  isKeyCode: (k, mod1, mod2, mod3) => {
    return keyboard.e.code == k && keyboard.hasKeyCodeMod(mod1, mod2, mod3);
  }
};

const mouse = {
  x: 0,
  y: 0,
}

/*
 * Abstract class for drawing a diagram
 *
 * Visualisation is done using buildingblocks
 * Zooming and dragging are facilitated. The World object handles the
 * appropriate transformations.
 */

class DiagramSvg extends BuildingBlock {

  /*
   * These abstract methods have to be defined by any subclass
   */
  initDiagram() { }
  get svg() { }
  selection(px, py) { }
  handleMouseClick(e) { }
  dragIt(px, py) { }
  stopDraggingIt() { }
  get animating() { return false; }
  animationUpdate() { }

  constructor(parent) {
    super();
    /*
     * interface to the outside world, through functions
     *   in.draw(data)
     *     show the data as a diagram
     *   in.dimensions(px, py, width, height)
     *     set the location (px, py) and size (with, height) of the
     *     diagram
     *   out.selected(location)
     *     process the selected node, using its location
     *
     *   out functions can be (re)defined by the outside world.
     *   sub classes can extend this interface
     */
    this.in = {
      draw: (data) => {
        this.previousData = this.data;
        this.data = data;
        if (this.set_up) {
          this.initDiagram();
          this.redraw();
        }
      },
      dimensions: (px, py, width, height) => {
        this.dimensions(px, py, width, height);
      },
    };
    this.out = {
      selected: (location) => {
        console.log('selected location: %j', location);
      }
    };

    // default canvas dimensions: whole window
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.fixedSize = false;

    this.set_up = false;
    this.diagram = null;
    this.world = null;
    this.loop = false;

    this.background = '#E8E8E8';
    this.drawing = null;

    // scrollbars
    this.scrollbars = null;

    // dragging:
    this.drag = {mousedown: false,
                 dragging: false,
                 ctrl: false,
                 start: {x: 0, y: 0}, // dragging startpoint
                 obj: null,
                 offset: {x: 0, y: 0} // dragging object offset
                };

    // highlighting:
    this.highlightedObj = null;

    // timeout for handling repeating key:
    this.keyTimeout = {
      delay: 50, // const
      handler: null,
      start: 0,
      wait: 0,
      running: false
    }

    // visualisation as svg:
    this._svg = null;

    // is diagram visually changed?
    this.redrawNeeded = false;

    this.setup();

    document.body.appendChild(this.svg);

    this.cursor = 'default';
    this.overButton = false;
  }

  dimensions(px, py, width, height) {
    this.width = width;
    this.height = height;
    this.fixedSize = true;
    if (this.set_up) {
      this.world.resizeCanvas(width, height);
      this.redraw();
    }
  }

  setup() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.world = new World(this.width, this.height);
    this.world.parent = this;
    this.drawing = new Dot();
    this.world.content = this.drawing;
    this.addEventListeners();
    this.set_up = true;
    this.setCursor('wait');
    this.scrollbars = new Scrollbars(this);
  }

  // drawing, scrollbars plus world
  svgDrawingPlus(title) {
    let drawing = this.drawing.svg;
    let scrollbars = this.scrollbars.svg;
    let world = this.world.svg;
    world.replaceChildren(drawing);
    let style = Text.style();
    if (!this._svg) {
      this._svg = Svg.element('svg', title);
      this._svg.setAttribute('text-rendering', 'optimizeSpeed');
      this._svg.append(style, world, scrollbars);
    } else {
      this._svg.replaceChildren(style, world, scrollbars);
    }
    this._svg.style.backgroundColor = this.background;
    this._svg.setAttribute('width', this.world.width);
    this._svg.setAttribute('height', this.world.height);
  }

  svgWorld() {
    let nodes = this._svg.childNodes;
    let world = nodes.values().find(child => child.getAttribute('class') == 'world');
    return world;
  }

  addEventListeners() {
    let checkRedraw = (f) => {
      return (e) => {
        // executing function 'f' might result in a need for redraw
        this.redrawNeeded = false;
        let thisf = (e) => f(e);
        let result = thisf(e);
        if (this.redrawNeeded)
          this.redraw();
      };
    };
    let add = (evt, fn) => {
      window.addEventListener(evt, checkRedraw(fn), {passive: false});
    }
    add('resize', this.windowResize.bind(this));
    add('keydown', this.keyDown.bind(this));
    add('keyup', this.keyUp.bind(this));
    add('mousemove', this.mouseMove.bind(this));
    add('mousedown', this.mouseDown.bind(this));
    add('mouseup', this.mouseUp.bind(this));
    add('wheel', this.mouseWheel.bind(this));

    let leaveWindow = (e) => {
      this.stopKeyTimeout();
      keyboard.e = null;
    };
    window.addEventListener('blur', leaveWindow);
  }

  redraw(timeStamp) {
    if (this.diagram) {
      // diagram should have been updated now!
      this.drawing.updateBounds();
      this._svg = this.svg;
      if (this.animating) {
        this.animationUpdate();
        window.requestAnimationFrame(t => this.redraw(t));
      } else {
        this.checkRepeatKey();
      }
    }
  }

  setCursor(type) {
    this.cursor = type;
    document.body.style.cursor = type;
  }

  get mouseInCanvas() {
    let w = this.width;
    let h = this.height;
    if (this.scrollbars.vertical.visible)
      w -= this.scrollbars.width;
    if (this.scrollbars.horizontal.visible)
      h -= this.scrollbars.width;
    if (!(0 <= mouse.x && mouse.x <= w)) return false;
    if (!(0 <= mouse.y && mouse.y <= h)) return false;
    return true;
  }

  get mouseInCanvasOrScrollbars() {
    let w = this.width;
    let h = this.height;
    if (!(0 <= mouse.x && mouse.x <= w)) return false;
    if (!(0 <= mouse.y && mouse.y <= h)) return false;
    return true;
  }

  windowResize(e) {
    if (!this.fixedSize) {
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      this.world.resizeCanvas(this.width, this.height);
    }
    e.preventDefault();
    this.redrawNeeded = true;
  }

  selectionOfKlass(sel, klass) {
    let obj = sel.find(s => s instanceof klass);
    return obj;
  }

  highlight(obj) {
    if (this.highlightedObj) {
      this.highlightedObj.highlight(false);
    }
    this.highlightedObj = obj;
    this.highlightedObj.highlight(true);
    this.redrawNeeded = true;
  }

  resetHighlight() {
    if (this.highlightedObj) {
      this.highlightedObj.highlight(false);
      this.highlightedObj = null;
    }
  }

  mouseMove(e) {
    mouse.x = e.clientX;
    mouse.y = e.clientY;
    if (!this.mouseInCanvasOrScrollbars) return;
    e.preventDefault();
    if (!this.diagram) return;

    // dragging?
    if (this.drag.mousedown) {
      let wpt = this.world.mousePoint();
      // ignore micro dragging:
      let draggingDist = Math.abs(wpt.x-this.drag.start.x) + Math.abs(wpt.y-this.drag.start.y);
      let epsilon = 5;
      if (this.drag.dragging || draggingDist * this.world.scale > epsilon) {
        this.drag.dragging = true;
        if (this.drag.obj == this.scrollbars.horizontal || this.drag.obj == this.scrollbars.vertical) {
          this.drag.obj.dragThumb(mouse.x - this.drag.offset.x, mouse.y - this.drag.offset.y);
          this.scrollbars.moveWorld();
          this.redrawNeeded = true;
        } else {
          if (this.drag.ctrl) {
            // drag the canvas
            this.world.x0 = mouse.x - this.drag.offset.x;
            this.world.y0 = mouse.y - this.drag.offset.y;
            this.world.changed = true;
            // also perform potential aditional diagram specific actions
          }
          this.dragIt(wpt.x-this.drag.offset.x, wpt.y-this.drag.offset.y);
          this.redrawNeeded = true;
        }
      }
    }
    if (!this.drag.dragging && this.mouseInCanvas) {
      let wpt = this.world.mousePoint();
      let obj = this.selection(wpt.x, wpt.y);
      if (obj && obj instanceof Button) {
        if (this.cursor != 'wait' && !this.overButton) {
          document.body.style.cursor = 'pointer';
          this.overButton = true;
        }
      } else if (this.overButton) {
        document.body.style.cursor = this.cursor;
        this.overButton = false;
      }
    }
  }

  mouseDown(e) {
    if (this.scrollbars.onTracks(mouse.x, mouse.y)) {
      e.preventDefault();
      if (this.scrollbars.onThumb(mouse.x, mouse.y)) {
        // drag?
        let bar = this.scrollbars.horizontal.onTrack(mouse.x, mouse.y)
            ? this.scrollbars.horizontal
            : this.scrollbars.vertical;
        let wpt = this.world.mousePoint();
        this.drag.mousedown = true;
        this.drag.start.x = wpt.x;
        this.drag.start.y = wpt.y;
        this.drag.obj = bar;
        // only one coordinate is relevant....
        this.drag.offset.x = mouse.x - bar.thumb.pos;
        this.drag.offset.y = mouse.y - bar.thumb.pos;
      } else {
        this.scrollbars.step(mouse.x, mouse.y);
        this.scrollbars.moveWorld();
        this.redrawNeeded = true;
      }
      return;
    }
    if (!this.mouseInCanvas) {
      return;
    }
    if (!this.diagram) return;
    let wpt = this.world.mousePoint();
    this.drag.mousedown = true;
    this.drag.ctrl = e.ctrlKey;
    this.drag.start.x = wpt.x;
    this.drag.start.y = wpt.y;
    this.drag.obj = null;
    if (e.ctrlKey) {
      this.drag.offset.x = mouse.x - this.world.x0;
      this.drag.offset.y = mouse.y - this.world.y0;
    } else {
      let obj = this.selection(wpt.x, wpt.y);
      if (obj) {
        // do not drag buttons
        if (obj instanceof Button) {
          this.buttonSelected = obj;
          obj.highlight(true);
          this.redrawNeeded = true;
        } else {
          this.drag.obj = obj;
          let bnd = obj.bounds;
          this.drag.offset.x = wpt.x - bnd.x;
          this.drag.offset.y = wpt.y - bnd.y;
        }
      }
    }
  }

  mouseUp(e) {
    let handleButton = () => {
      if (this.buttonSelected) {
        this.buttonSelected.highlight(false);
        this.redrawNeeded = true;
      }
      this.buttonSelected = null;
    };
    e.preventDefault();
    this.drag.mousedown = false;
    if (this.drag.dragging) {
      this.drag.dragging = false;
      handleButton();
      this.stopDraggingIt();
    } else {
      if (!this.mouseInCanvas) return;
      if (!this.diagram) return;
      handleButton();
      this.handleMouseClick(e);
    }
  }

  handleKey(e) {
    let down = keyboard.isKeyCode('ArrowDown', 'Shift');
    let up = keyboard.isKeyCode('ArrowUp', 'Shift');
    let right = keyboard.isKeyCode('ArrowRight', 'Shift');
    let left = keyboard.isKeyCode('ArrowLeft', 'Shift');
    if (down || up || right || left) {
      let inc = 1/4;
      if (down) this.scrollbars.vertical.stepDirection(false, inc);
      else if (up) this.scrollbars.vertical.stepDirection(true, inc);
      else if (right) this.scrollbars.horizontal.stepDirection(false, inc);
      else if (left) this.scrollbars.horizontal.stepDirection(true, inc);
      this.scrollbars.moveWorld();
      this.redrawNeeded = true;
    } else {
      // default behaviour for unbound keys:
      return;
    }
  }

  keyDown(e) {
    if (!this.mouseInCanvasOrScrollbars) return;
//    e.preventDefault();
    if (!this.diagram) return;
    keyboard.e = e;
    if (!e.repeat) {
      this.startKeyTimeout();
      return this.handleKey(e);
    }
  }

  keyUp(e) {
    this.stopKeyTimeout();
    if (!this.mouseInCanvasOrScrollbars) return;
//    e.preventDefault();
    keyboard.e = null;
    return false;
  }

  startKeyTimeout() {
    this.keyTimeout.running = true;
    this.keyTimeout.wait = this.keyTimeout.delay * 15; // initial delay is larger
    this.keyTimeout.start = Date.now();
  }

  stopKeyTimeout() {
    this.keyTimeout.running = false;
    if (this.keyTimeout.handler != null) {
      clearTimeout(this.keyTimeout.handler);
      this.keyTimeout.handler = null;
    }
  }

  checkRepeatKey() {
    if (!this.mouseInCanvasOrScrollbars) {
      // reset
      this.stopKeyTimeout();
    }
    if (this.keyTimeout.running && this.keyTimeout.handler == null) {
      let now = Date.now();
      let delta = now - this.keyTimeout.start;
      let rerun = () => {
        // restart timer:
        this.keyTimeout.start = Date.now();
        this.keyTimeout.wait = this.keyTimeout.delay;
        this.keyTimeout.handler = null;
        this.handleKey(keyboard.e);
        // can't delay redrawing!
        if (this.redrawNeeded) {
          this.redraw();
        }
      };
      this.keyTimeout.handler = setTimeout(rerun, this.keyTimeout.wait - delta);
    }
  }

  mouseWheel(e) {
    if (!this.mouseInCanvas) return;
    e.preventDefault();
    if (!this.diagram) return;
    this.world.mouseWheel(e);
    this.redrawNeeded = true;
    return false;
  }

  localStorageSetItem(key, value) {
    if (this.localStorageAvailable()) {
      window.localStorage.setItem(key, value);
    }
  }

  localStorageGetItem(key) {
    let result = null;
    if (this.localStorageAvailable()) {
      result = window.localStorage.getItem(key);
    }
    return result;
  }

  localStorageAvailable() {
    let supported = window && window.localStorage;
    if (supported) {
      try {
        // testing: exception when not available
        var key = 'testDiagram';
        window.localStorage.setItem(key, 'testDiagram');
        window.localStorage.removeItem(key);
      } catch (e) {
        supported = false;
      }
    }
    return supported;
  }

  // Function to download data to a file
  //(c) https://stackoverflow.com/questions/13405129/create-and-save-a-file-with-javascript
  download(data, filename, type) {
    var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
      window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
      var a = document.createElement("a"),
          url = URL.createObjectURL(file);
      a.href = url;
      a.download = filename;
      document.body.appendChild(a);
      a.click();
      setTimeout(function() {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
      }, 0);
    }
  }

  saveAsSvg(title) {
    if (!this.diagram) return;
    let bnd = this.drawing.bounds;
    // store state:
    let save = {width: this.width, height: this.height, world: this.world.state};
    this.width = bnd.width + this.scrollbars.width;
    this.height = bnd.height + this.scrollbars.width;
    this.world.state = {
      width: this.width,
      height: this.height,
      scale: 1,
      x0:  -bnd.x,
      y0:  -bnd.y};
    let s = this.svg;
    let str = new XMLSerializer().serializeToString(s);
    this.download(str, title, 'svg');
    // restore state:
    this.width = save.width;
    this.height = save.height;
    this.world.state = save.world;
    this.redrawNeeded = true;
  }
}

class Drawing extends HasViz {
  constructor(content) {
    super();
    this.content = content;
    this.initViz();
  }

  initViz() {
    let box = new BoundingBox(this.content);
    box.padding = {left: 50, top: 50, right: 50, bottom: 50};
    box.color = '#FFFFFF';
    box.strokeColor = '#FFFFFF';
    box.strokeWeight = 0;
    box.shadowed = false;
    box.update();
    this.setViz(box);
  }

  get padding() {
    return this.viz.padding;
  }

  set padding(pad) {
    this.viz.padding = pad;
  }
  
  updateContent(content) {
    this.content = content;
    this.isModified();
    if (content) {
      this.viz.content = content;
      this.update();
    } else {
      this.viz = null;
      this.upToDate = true;
    }
  }

  updateBounds() {
    if (this.viz) this.viz.updateBounds();
  }
}
