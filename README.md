<!---
#
#  Copyright (C) 2020, 2021, 2022, 2023, 2024 Rob Wieringa <rma.wieringa@gmail.com>
#
#  This file is part of Depyct.
#  Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)
#
#  Depyct is free software, it is distributed under the terms of
#  the GNU General Public Licence version 3 or later.
#  See <http://www.gnu.org/licenses/>.
-->

# Depyct

![logo](./logo.png)

Depyct offers Dezyne web views based on Scalable Vector Graphics (SVG)

Depyct is free software, it is distributed under the terms of the GNU
General Public Licence version 3 or later. See the file [COPYING](./COPYING).

# Requirements

Depyct depends on the following packages:

  - https://github.com/JWally/jsLPSolver

# Views

## System diagram

A system diagram can be generated from a Dezyne file as shown in the
following example:

    echo "let AST = " $(dzn graph -b system -f json -L hello.dzn) > SystemDiagram/data.js


It is available for viewing in [systemDiagram.html](./SystemDiagram/systemDiagram.html).

For online help press `F1` or `?`.

## Sequence diagram

A sequence diagram of a trace can be generated from a Dezyne file as
shown in the following example:

    echo "let Data = " $(cat trace | dzn simulate --state -l hello.dzn | dzn trace -f json) > SequenceDiagram/data.js

It is available for viewing in [sequenceDiagram.html](./SequenceDiagram/sequenceDiagram.html)

For online help press `F1` or `?`.

## State diagram

A state diagram can be generated from a Dezyne file as shown in the
following example:


    echo "let Data = " $(dzn graph -b state -f json -L hello.dzn) > StateDiagram/data.js

It is available for viewing in [stateDiagram.html](./StateDiagram/stateDiagram.html)

For online help press `F1` or `?`.

# JavaScript Code Documentation

All Dezyne views -- currently three -- follow the same top-level
structure. Taking the system view as an example:

  - The content is covered in the file `systemDiagram.js`. Here, each
    of the various elements of a system diagram is defined in its own
    class: `Port`, `Binding`, `Instance`, etc. Each element has its
    own logical structure and visualization.

  - The part covering the web presentation and user interaction is
    covered in the file `systemDiagramSvg.js`, in class
    `SystemDiagramSvg`. The functionality handled here includes:

    - page initialization
    - scrollbar handling
    - data communication with the outside world (data loading and event data sending)
    - the basic user event handling, such as reacting on mouse and keyboard clicks
    - refreshing the visualization after changes
    - the 'stop-motion' animation occurring when an instance is opened or closed

  - All three Dezyne views have similar web presentation and user
    interaction. For that reason, the common part is covered in the
    generic `diagramSvg.js` file, in class `DiagramSvg`. Class
    `SystemDiagramSvg` e.g. inherits from `DiagramSvg`.

  - All views are recursively built from building blocks, starting from
    a limited set of primitives, like boxes and lines.

## SVG

The Dezyne views offered by Depyct are in SVG (Scalable Vector
Graphics) format. The SVG JavaScript API (see e.g.
[Mozilla](https://developer.mozilla.org/en-US/docs/Web/API/SVG_API) or
[w3.org](https://www.w3.org/TR/SVG11/svgdom.html))
is used to generate these views.

The SVG structure for a view follows the view's building block
structure one-to-one.

The API is part of the DOM (Document Object Model), and changes in the
SVG trigger an automatic redraw of the web page, provided the changes
occur at the root of the SVG object tree. For that reason, each
BuildinBlock propagates its `modified` status all the way up to its
root parent.

DOM operations are notoriously expensive. To prevent unnecessary
recalculations, each building block stores its most recent SVG and
maintains a `changed` boolean indicating the need to recalculate the
SVG.

## BuildingBlocks

All drawing elements inherit from the `BuildingBlock` base class. The
base class defines (among others) an object's position and dimensions,
and the object's visual representation in the form of an SVG.

Additionally, the following support classes are defined:

### HasViz

Each diagram element that has a visual representation (and most
elements have), inherits from this class. The visual is stored in the
`viz` member.

### World

The `World` class keeps track of changes due to zooming and panning.

### Scrollbars

The Depyct diagrams are provided with a horizontal and vertical
scrollbar whenever the diagram size is larger than the HTML page size
(e.g. due to zooming). When not needed, one or both of the scrollbars
are hidden.


### Diagram

Each of the Depyct diagrams is presented on its own HTML page, and all
code that is common to these pages is part of the `DiagramSvg` class.

It provides, among other things,

  - a programming interface to the outside world
  - scrollbars and their handling
  - event listeners to support user interaction, such as mouse and
    keyboard actions
  - an SVG preamble, common for all diagrams
  - saving of the SVG to a file

All resulting diagram-specific method calls are defined as virtual.

## System Diagram

The System Diagram implementation has a number of challenges:

  - the system instances layout;
  - bindings crossing instances;
  - the feature that an instance can be closed and opened.

### System Layout

The instances in a system are arranged such that all bindings point
downwards. This is possible due to the absence of binding cycles.
Instances are ordered top-to-bottom in so-called Layers, and layouting
ensures that all instances in one layer have the same `y` ordinate.

A binding between two ports is visualized as a three-segment line, the
middle segment being horizontal. The system layout ensures that the
total length of all horizontal binding segments is minimal. To achieve
that, the instances within a layer can be reordered.

The minimization of the total binding length boils down to a so-called
linear programming problem. See section 
[Linear Programming](#linear-programming) for a more detailed description.

Part of the layouting is the ordering of bindings such that
unnecessary crossings in the 'between layers' areas are avoided.

### Bindings Crossing Instances

In some Dezyne models, the layouting process will result in a diagram
where a binding crosses an instance. To avoid confusion, the
overlapping part of the binding is rendered in a light gray color, as
if it lies behind the instance. This is achieved as follows:

  - Render the complete system.
  - Redraw all instances on top; this hides the overlapping part of
    the binding.
  - Redraw all bindings on top of that, with an opacity factor.

### Closing and Opening

In a system diagram, subsystems can be closed (i.e. not show their
internal structure) and reopened. As part of that action, the system
layout is recalculated. A 'stop-motion' animation assists the user to
follow the change in layout:

  - in the first animation step, the subsystem changes its open-closed
    state, but keeps its original dimensions;
  - in the following steps, it gradually morphs to the final size;
  - in each step, the layout is recalculated;
  - reordering of instances is avoided.

So reordering is only done in the initial layout, where all instances
are in their `open` state.

### Linear Programming

As stated above, the instances within each layer are to be positioned
such that the total length of all bindings is minimal.

The LP (linear programming) `javascript-lp-solver` package is used to
compute the solution for this problem. As input it takes a set of
variables and constraints and a function to be optimized; it delivers
optimal values for each variable.

The optimization problem is solved in two steps.

The first step is done only once, at the initial layouting. In this
step, no constraints are set on the relative position of instances in
each layer. As a result of this step, the order of instances can
change.

The second step is done as part of each re-layout. In this step, the
ordering of instances is maintained.

The optimization process is explained with the following example:

    component Driver
    {
      provides IControl control;
      requires IAcquisition acquisition;
      requires IOptics optics;
    }

    component Optics
    {
      provides IOptics port;
    }

    component Acquisition
    {
      provides IAcquisition port;
    }

    component Camera
    {
      provides IControl control;

      system
      {
        Driver driver;
        Optics optics;
        Acquisition acquisition;

        control <=> driver.control;
        driver.acquisition <=> acquisition.port;
        driver.optics <=> optics.port;
      }
    }


#### Step one: establish instance ordering

##### Variables

In step one, the following variables are defined:

  - for each instance `I`: a variable called _**I:x**_ modeling the x
    ordinate of the instance position;
  - for each binding `b`: a variable called _**b:d**_ modeling the
    difference between de x ordinates of the binding's `from` port and
    `to` port.

In the optimal layout, the total length of all horizontal binding
segments will be minimal. In other words, the absolute values of the
binding variables are needed in the optimization. The following
standard LP method is available:

When _b_ is a variable for which its absolute value _|b|_ is needed:

  - introduce variables _**b:pos**_ and _**b:neg**_;
  - replace _b_ by _b:pos - b:neg_;
  - replace _|b|_ by _b:pos + b:neg_;

For the `Camera` example, the variable definition boils down to the following list:

  - for instance `driver`: variable _**driver:x**_;
  - for instance `optics`: variable _**optics:x**_;
  - for instance `acquisition`: variable _**acquisition:x**_;
  - for binding `control <=> driver.control`: variables _**b0:pos**_
    and _**b0:neg**_;
  - for binding `driver.acquisition <=> acquisition.port`: variables
    _**b1:pos**_ and _**b1:neg**_;
  - for binding `driver.optics <=> optics.port`: variables
    _**b2:pos**_ and _**b2:neg**_;

##### Constraints

In the package used, all variables have an implied non-negativity
constraint, unless explicitly stated otherwise.

All instances are constrained by the system bounding box:  

  - _driver:x $\le$ W - driver.width_
  - _optics:x $\le$ W - optics.width_
  - _acquisition:x $\le$ W - acquisition.width_

where _W_ is the constant denoting the width of the bounding box.

Note: In this phase of layouting, no additional constraints are put on
instances, such as ordering and overlap. See 
[Step two](#step-two-calculate-instance-positions) below for the
second phase.

Each binding variable can be defined in terms of the corresponding
port x ordinates; note that a port position is relative to its
instance position, and that each _b:d_ has to be replaced by _b:pos - b:neg_
  
  - _b0:pos - b0:neg $=$ (driver:x + driver.control.x) - (0 + control.x)_
  - _b1:pos - b1:neg $=$ (acquisition:x + acquisition.port.x) - (driver:x + driver.acquisition.x)_
  - _b2:pos - b2:neg $=$ (optics:x + optics.port.x) - (driver:x + driver.optics.x)_

##### Objective

The objective of the optimization is to minimize the linear objective
function _sum(|b:d|)_, or -- replacing each _|b|_ by _b:pos + b:neg_ --
to minimize
  
  - _b0:pos + b0:neg + b1:pos + b1:neg + b2:pos + b2:neg_

##### Result

The result of the LP solve contains values for all variables. In step
one, the _I:x_ values are used to sort the instances within each layer
based on their midpoints, so `I1` comes before `I2` when
_I1:x + I1.width/2 $\le$ I2:x + I2.width/2_.

In the `Camera` example the sorting process will result in the following
reordering: instance `acquisition` will be positioned left of `optics`.

#### Step two: calculate instance positions

In the second step, the model is extended and the final position of
each instance is determined. This step is executed as part of each
relayout.

##### Variables

The variables defined in step one are reused in step two.

##### Constraints

The constraints from step one are still valid but are extended with
constraints that -- per layer -- express the non-overlapping order of
instances. For the `Camera` example, the `optics` instance must be
positioned to the right of the `acquisition` instance with some
separation between the two, which comes down to:

  - _optics:x $\ge$ acquisition:x + acquisition.width + hsep_

where _hsep_ is the constant denoting the required horizontal
separation between instances in the same layer.

##### Objective

The objective function defined in step one is also used in this step.

##### Result

The result of the LP solve contains values for all instance _I:x_
variables. These values are used to position the instances, and
the binding layouts are adapted accordingly.

## Sequence Diagram

The JSON data input from which a diagram is derived needs some processing:

  - The JSON data is organized by lifeline; each lifeline contains a
    header with a 'dotted' name. The header of the diagram shows a
    recursive structure that follows the system composition of the
    underlying model. The recursive structure is derived from the JSON
    data by factoring out common name preparts.
  - The lifelines in the diagram show so-called 'activity bars'
    denoting the start and end of an activity. The JSON data contains
    a list of events that refer to activities on lifelines. The
    activity bars are derived from this data by careful case analysis.

Independent of scrolling, the header is always rendered at the top of
the window. This is achieved by moving it to its appropriate position
just before rendering.

System headers can be closed and reopened, similar to the system
diagram. When a system is closed:

  - the system header internals are hidden;
  - all lifelines internal to the system are hidden, and replaced by
    one new system lifeline;
  - each trace event internal to the system is hidden;
  - each event going into the system, or out of it, is replaced by an
    event to the new system lifeline;
  - all eligibles internal to the system are hidden;
  - all activity bars are recalculated;
  - the whole diagram gets a re-layout, resulting in a compacter
    diagram.

When the system is reopened, all previous closing steps are undone.

## State Diagram

The State Diagram layout is a directed graph where the nodes are
the state diagram's states and transitions.

### Force Directed Layout (FDL)

A so-called force-directed layouting method, or FDL for short, is used
(see
e.g. [Wikipedia](https://en.wikipedia.org/wiki/Force-directed_graph_drawing))
to come to an aesthetically-pleasing distribution of nodes. In this
method, spring-like attractive forces and repulsive forces like those
of electrically charged particles are used to separate
nodes. Iteratively, the forces are recalculated until a minimal energy
level is reached. Damping is added to speed up the process. The used
'physical' scalars like 'mass' and 'charge' of nodes, or 'length' and
'stiffness' of edges result from experiments with a limited set of
examples.

The main disadvantages of this method are high processing time and the
risk of suboptimal solutions.

The main performance bottleneck lies in calculating the repulsive
force, which is -- for each iteration -- quadratic in the number of
nodes. Repulsive forces are inverse quadratic in the distance between
two nodes, and the calculation of the total repulsive force on one node
can be approximated by only considering nodes that are relatively
close by. Efficiency in identifying such a set is achieved
by storing nodes in a so-called spatial grid (see e.g.
[Wikipedia](https://en.wikipedia.org/wiki/Grid_(spatial_index))).
However, larger graphs will still lead to excessive processing time.

To mitigate the risk of suboptimal solutions, the graph is built up in
three phases. In each phase, the FDL method is applied. The resulting
positioning of nodes is used as a starting point for the next phase.

  - In phase one, a relatively small graph is constructed from state
    nodes, without transitions or self-transitions.
  - In phase two, non-self-transition nodes are added; each is
    positioned between its pair of associated state nodes.
  - In phase three, self-transitions are added; each set associated
    with a state node is positioned in a close circle around the state
    node.

### Choice Nodes

The following graph transformation feature is implemented in Depyct
but could be made available in Dezyne instead.

When a state node leads to a set of transitions that have a common
starting text sequence, the graph is transformed: a so-called 'choice'
node is inserted. The example below explains the procedure.

Suppose 

  - _T1_ is a transition node with text _c1; c2; t1_ 
  - _T2_ is a transition node with text _c1; c2; t21; t22_ 
  - _S_, _S1_, and _S2_ are state nodes

so _T1_ and _T2_ share a text prepart, then 

  - _S &rarr; T1 &rarr; S1; S &rarr; T2 &rarr; S2_ 

is transformed into 

  - _S &rarr; Tc &rarr; C; C &rarr; T1a &rarr; S1; C &rarr; T2a &rarr; S2_ 

where

  - _Tc_ is a new transition node with the common text _c1; c2_ 
  - _C_ is a diamond-shaped choice node without text
  - _T1a_ is a new transition node with text _t1_ 
  - _T2a_ is a new transition node with text _t21; t22_ 

### Edge Visualization

The visualization of edges in the directed graph has two features
worth mentioning:

  - Traditionally, edges are visualized as arrow lines where the
    (triangular) point of the arrow touches the targeted node. Since
    in the state diagram nodes are rendered as rounded rectangles, it
    is not trivial to determine the touchpoint. This problem is
    avoided by rendering edges as tapered straight lines, narrowing
    towards their target node.
  - In a self-transition like _S &rarr; T &rarr; S_, the two edges would
    overlap when rendered as straight lines. This is avoided by
    inserting waypoint nodes _W1_ and _W2_ halfway along the two
    edges, transforming the self-transition into

    - _S &rarr; W1 &rarr; T &rarr; W2 &rarr; S_ 
  
    The repulsive forces between nodes visually split the edges. Each
    waypoint is rendered as a small dot in the same color as the edge
    and with a diameter equal to the width of the edge to make it
    unobtrusive, so the edge looks like a kinked line.
  
### Progress

In 'interactive' mode, when the user drags one of the nodes, the
display is updated after each iteration step until the required
stability is reached.

In the initial phase, to avoid additional performance penalties, no
diagram is displayed. Instead, progress messages are presented during
lengthy FDL calculations to avoid a 'frozen' page. This is implemented
in the `animation` method, in `stateDiagramSvg.js`. It uses a timeout
mechanism with an interval of one second to throttle the refresh
frequency. In each interval, the force calculation is repeated until
the allotted time is up or the required stability is found, in which
case the next phase is started.


# Legalese

Copyright © 2020, 2021, 2022, 2023, 2024 Rob Wieringa <rma.wieringa@gmail.com>

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.
